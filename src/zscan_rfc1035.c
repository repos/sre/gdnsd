
#line 1 "./src/zscan_rfc1035.rl"
/* Copyright © 2012 Brandon L Black <blblack@gmail.com> and Jay Reitz <jreitz@gmail.com>
 *
 * This file is part of gdnsd.
 *
 * gdnsd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gdnsd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gdnsd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>
#include "zscan_rfc1035.h"

#include "conf.h"
#include "ltree.h"
#include "chal.h"
#include "dnssec.h"

#include <gdnsd/alloc.h>
#include <gdnsd/log.h>
#include <gdnsd/misc.h>
#include <gdnsd/file.h>

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <setjmp.h>
#include <errno.h>

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif

#define parse_error(_fmt, ...) \
    do {\
        log_err("rfc1035: Zone %s: parse error at file %s line %u: " _fmt, logf_dname(z->zroot->c.dname), z->curfn, z->lcount, __VA_ARGS__);\
        siglongjmp(z->jbuf, 1);\
    } while (0)

#define parse_error_noargs(_fmt) \
    do {\
        log_err("rfc1035: Zone %s: parse error at file %s line %u: " _fmt, logf_dname(z->zroot->c.dname), z->curfn, z->lcount);\
        siglongjmp(z->jbuf, 1);\
    } while (0)

#define parse_warn(_fmt, ...) \
    do {\
        log_warn("rfc1035: Zone %s: parse warning at file %s line %u: " _fmt, logf_dname(z->zroot->c.dname), z->curfn, z->lcount, __VA_ARGS__);\
        if (gcfg->zones_strict_data)\
            siglongjmp(z->jbuf, 1);\
    } while (0)

struct zscan {
    uint8_t  ipv6[16];
    uint32_t ipv4;
    bool     zn_err_detect;
    unsigned lcount;
    unsigned text_len;
    unsigned def_ttl;
    unsigned uval;
    unsigned ttl;
    unsigned ttl_min;
    unsigned uv_1;
    unsigned uv_2;
    unsigned uv_3;
    unsigned uv_4;
    unsigned uv_5;
    unsigned rfc3597_data_len;
    unsigned rfc3597_data_written;
    uint8_t* rfc3597_data;
    struct ltree_node_zroot* zroot;
    const char* tstart;
    const char* curfn;
    char* include_filename;
    uint8_t  origin[256];
    uint8_t  file_origin[256];
    uint8_t  lhs_dname[256];
    uint8_t  rhs_dname[256];
    union {
        uint8_t eml_dname[256];
        char    rhs_dyn[256];
        char    caa_prop[256];
    };
    uint8_t* text;
    sigjmp_buf jbuf;
};

F_NONNULL
static void scanner(struct zscan* z, char* buf, const size_t bufsize);

/******** IP Addresses ********/

F_NONNULL
static void set_ipv4(struct zscan* z, const char* end)
{
    char txt[16];
    unsigned len = end - z->tstart;
    if (len > 15)
        parse_error_noargs("IPv4 address unparseable (too long)");
    memcpy(txt, z->tstart, len);
    txt[len] = 0;
    z->tstart = NULL;
    struct in_addr addr;
    int status = inet_pton(AF_INET, txt, &addr);
    if (status > 0)
        z->ipv4 = addr.s_addr;
    else
        parse_error("IPv4 address '%s' invalid", txt);
}

F_NONNULL
static void set_ipv6(struct zscan* z, const char* end)
{
    char txt[INET6_ADDRSTRLEN + 1];
    unsigned len = end - z->tstart;
    if (len > INET6_ADDRSTRLEN)
        parse_error_noargs("IPv6 address unparseable (too long)");
    memcpy(txt, z->tstart, len);
    txt[len] = 0;
    z->tstart = NULL;
    struct in6_addr v6a;
    int status = inet_pton(AF_INET6, txt, &v6a);
    if (status > 0)
        memcpy(z->ipv6, v6a.s6_addr, 16);
    else
        parse_error("IPv6 address '%s' invalid", txt);
}

F_NONNULL
static void set_uval(struct zscan* z)
{
    errno = 0;
    z->uval = strtoul(z->tstart, NULL, 10);
    z->tstart = NULL;
    if (errno)
        parse_error("Integer conversion error: %s", logf_errno());
}

F_NONNULL
static void set_uval8(struct zscan* z)
{
    set_uval(z);
    if (z->uval > 255)
        parse_error("Value %u exceeds 8-bit limit", z->uval);
}

F_NONNULL
static void set_uval16(struct zscan* z)
{
    set_uval(z);
    if (z->uval > 65535)
        parse_error("Value %u exceeds 16-bit limit", z->uval);
}

F_NONNULL
static void validate_origin_in_zone(struct zscan* z, const uint8_t* origin)
{
    gdnsd_assume(z->zroot->c.dname);
    if (!dname_isinzone(z->zroot->c.dname, origin))
        parse_error("Origin '%s' is not within this zonefile's zone (%s)", logf_dname(origin), logf_dname(z->zroot->c.dname));
}

F_NONNULL F_PURE
static unsigned dn_find_final_label_offset(const uint8_t* dname)
{
    gdnsd_assert(dname_get_status(dname) == DNAME_PARTIAL);

    // Since we assert DNAME_PARTIAL, we just have to search forward until the
    // next potential label len is the partial terminator 0xff.
    const uint8_t* dnptr = dname + 1;
    unsigned next_llen_pos = *dnptr + 1U;
    while (dnptr[next_llen_pos] != 0xff) {
        dnptr += next_llen_pos;
        next_llen_pos = *dnptr + 1U;
    }

    return (unsigned)(dnptr - dname);
}

// This converts an unqualified name to a qualified one.  Normal behavior is to
// append the current $ORIGIN, but we also plug in support for '@' here (as a
// lone character meaning $ORIGIN) and also these extensions:
//--
// * If the final label is "@Z", we replace that with the original zone-level
// origin (the name of the actual zone) rather than the current $ORIGIN
// * If the final label is "@F", we replace that with the original file-level
// origin (the origin when a zonefile or includefile was first loaded, before
// any $ORIGIN statement within it) rather than the current $ORIGIN
//--
// @Z and @F are equivalent when not processing an included file.
// @Z and @F can also, like @, be the first (only) label; there doesn't have to
// be any prefix label before them.

F_NONNULL
static enum dname_status dn_qualify(uint8_t* dname, const uint8_t* origin, uint8_t* const file_origin, const uint8_t* zone_origin)
{
    gdnsd_assert(dname_get_status(dname) == DNAME_PARTIAL);

    // Lone "@" case:
    if (dname[0] == 3U && dname[2] == '@') {
        gdnsd_assume(dname[1] == 1U && dname[3] == 0xff);
        dname_copy(dname, origin);
        return DNAME_VALID;
    }

    // @Z/@F handling (note @X for any other char is illegal for now):
    const unsigned final_label_offset = dn_find_final_label_offset(dname);
    const unsigned final_label_len = dname[final_label_offset];
    gdnsd_assume(final_label_len != 0);

    if (final_label_len == 2U && dname[final_label_offset + 1] == '@') {
        const uint8_t which = dname[final_label_offset + 2];
        // adjust dname to strip the final @X label off
        dname[final_label_offset] = 0xff;
        *dname -= 3U;
        // note lowercase z/f here, because earlier dname processing
        // normalizes all alpha chars to lowercase
        if (which == 'z')
            return dname_cat(dname, zone_origin);
        else if (which == 'f')
            return dname_cat(dname, file_origin);
        else
            return DNAME_INVALID;
    }

    // default qualification with no @ involvement
    return dname_cat(dname, origin);
}

F_NONNULL
static void dname_set(struct zscan* z, uint8_t* dname, unsigned len, bool lhs)
{
    gdnsd_assume(z->zroot->c.dname);
    enum dname_status catstat;
    enum dname_status status;

    if (len) {
        status = dname_from_string(dname, z->tstart, len);
    } else {
        gdnsd_assume(lhs);
        dname_copy(dname, z->origin);
        status = DNAME_VALID;
    }

    switch (status) {
    case DNAME_INVALID:
        parse_error_noargs("unparseable domainname");
        break;
    case DNAME_VALID:
        if (lhs && !dname_isinzone(z->zroot->c.dname, dname))
            parse_error("Domainname '%s' is not within this zonefile's zone (%s)", logf_dname(dname), logf_dname(z->zroot->c.dname));
        break;
    case DNAME_PARTIAL:
        // even though in the lhs case we commonly trim
        //   back most or all of z->origin from dname, we
        //   still have to construct it just for validity checks
        catstat = dn_qualify(dname, z->origin, z->file_origin, z->zroot->c.dname);
        if (catstat == DNAME_INVALID)
            parse_error_noargs("illegal domainname");
        gdnsd_assert(catstat == DNAME_VALID);
        break;
    default:
        gdnsd_assume(0);
    }
}

// This is broken out into a separate function (called via
//   function pointer to eliminate the possibility of
//   inlining on non-gcc compilers, I hope) to avoid issues with
//   setjmp and all of the local auto variables in zscan_rfc1035() below.
typedef bool (*sij_func_t)(struct zscan*, char*, const size_t);
F_NONNULL F_NOINLINE
static bool _scan_isolate_jmp(struct zscan* z, char* buf, const size_t bufsize)
{
    if (!sigsetjmp(z->jbuf, 0)) {
        scanner(z, buf, bufsize);
        return false;
    }
    return true;
}

F_NONNULL
static bool zscan_do(struct ltree_node_zroot* zroot, const uint8_t* origin, const char* fn, const unsigned def_ttl_arg)
{
    log_debug("rfc1035: Scanning file '%s' for zone '%s'", fn, logf_dname(zroot->c.dname));

    bool failed = false;

    struct fmap* fmap = gdnsd_fmap_new(fn, true, true);
    if (!fmap) {
        failed = true;
        return failed;
    }

    const size_t bufsize = gdnsd_fmap_get_len(fmap);
    char* buf = gdnsd_fmap_get_buf(fmap);

    struct zscan* z = xcalloc(sizeof(*z));
    z->lcount = 1;
    z->def_ttl = def_ttl_arg;
    z->zroot = zroot;
    z->curfn = fn;
    dname_copy(z->origin, origin);
    dname_copy(z->file_origin, origin);
    dname_copy(z->lhs_dname, origin); // set lhs to relative origin initially

    sij_func_t sij = &_scan_isolate_jmp;
    if (sij(z, buf, bufsize))
        failed = true;

    if (gdnsd_fmap_delete(fmap))
        failed = true;

    if (z->text)
        free(z->text);
    if (z->rfc3597_data)
        free(z->rfc3597_data);
    if (z->include_filename)
        free(z->include_filename);
    free(z);

    return failed;
}

/********** TXT ******************/

F_NONNULL
static void text_start(struct zscan* z V_UNUSED)
{
    gdnsd_assert(z->text == NULL);
    gdnsd_assert(z->text_len == 0);
}

F_NONNULL
static void text_add_tok(struct zscan* z, const unsigned len, const bool big_ok)
{
    char* text_temp = xmalloc(len ? len : 1);
    unsigned newlen = len;
    if (len) {
        newlen = dns_unescape(text_temp, z->tstart, len);
        if (!newlen) {
            free(text_temp);
            parse_error_noargs("Text chunk has bad escape sequence");
        }
        gdnsd_assume(newlen <= len);
    }

    if (newlen > 255U) {
        if (!big_ok || gcfg->disable_text_autosplit) {
            free(text_temp);
            parse_error_noargs("Text chunk too long (>255 unescaped)");
        }
        if (newlen > 16000U) {
            free(text_temp);
            parse_error_noargs("Text chunk too long (>16000 unescaped)");
        }
        const unsigned remainder = newlen % 255;
        const unsigned num_whole_chunks = (newlen - remainder) / 255;
        unsigned new_alloc = newlen + num_whole_chunks + (remainder ? 1 : 0);
        if (new_alloc + z->text_len > 16000U) {
            free(text_temp);
            parse_error_noargs("Text record too long (>16000 in rdata form)");
        }

        z->text = xrealloc(z->text, z->text_len + new_alloc);
        unsigned write_offset = z->text_len;
        z->text_len += new_alloc;
        const char* readptr = text_temp;
        for (unsigned i = 0; i < num_whole_chunks; i++) {
            z->text[write_offset++] = 255;
            memcpy(&z->text[write_offset], readptr, 255);
            write_offset += 255;
            readptr += 255;
        }
        if (remainder) {
            z->text[write_offset++] = remainder;
            memcpy(&z->text[write_offset], readptr, remainder);
        }
        gdnsd_assume(write_offset + remainder == z->text_len);
    } else { // 0-255 bytes, one chunk
        const unsigned new_alloc = newlen + 1;
        if (new_alloc + z->text_len > 16000U) {
            free(text_temp);
            parse_error_noargs("Text record too long (>16000 in rdata form)");
        }
        z->text = xrealloc(z->text, z->text_len + new_alloc);
        unsigned write_offset = z->text_len;
        z->text_len += new_alloc;
        z->text[write_offset++] = newlen;
        memcpy(&z->text[write_offset], text_temp, newlen);
    }

    free(text_temp);
    z->tstart = NULL;
}

F_NONNULL
static void text_add_tok_huge(struct zscan* z, const unsigned len)
{
    char* storage = xmalloc(len ? len : 1);
    unsigned newlen = len;
    if (len) {
        newlen = dns_unescape(storage, z->tstart, len);
        if (!newlen) {
            free(storage);
            parse_error_noargs("Text chunk has bad escape sequence");
        }
        gdnsd_assume(newlen <= len);
    }

    if (newlen > 16000U) {
        free(storage);
        parse_error_noargs("Text chunk too long (>16000 unescaped)");
    }

    // _huge is only used alone, not in a set
    gdnsd_assume(!z->text_len);
    gdnsd_assume(!z->text);

    z->text = (uint8_t*)storage;
    z->text_len = newlen;
    z->tstart = NULL;
}

F_NONNULL
static void set_filename(struct zscan* z, const unsigned len)
{
    char* fn = xmalloc(len + 1);
    const unsigned newlen = dns_unescape(fn, z->tstart, len);
    if (!newlen) {
        free(fn);
        parse_error_noargs("Filename has bad escape sequence");
    }
    gdnsd_assume(newlen <= len);
    z->include_filename = fn = xrealloc(fn, newlen + 1);
    fn[newlen] = 0;
    z->tstart = NULL;
}

F_NONNULL
static char* _make_zfn(const char* curfn, const char* include_fn)
{
    if (include_fn[0] == '/')
        return xstrdup(include_fn);

    const char* slashpos = strrchr(curfn, '/');
    const unsigned cur_copy = (slashpos - curfn) + 1;
    const unsigned include_len = strlen(include_fn);
    char* rv = xmalloc(cur_copy + include_len + 1);
    memcpy(rv, curfn, cur_copy);
    memcpy(rv + cur_copy, include_fn, include_len);
    rv[cur_copy + include_len] = 0;

    return rv;
}

F_NONNULL
static void process_include(struct zscan* z)
{
    gdnsd_assume(z->include_filename);

    validate_origin_in_zone(z, z->rhs_dname);
    char* zfn = _make_zfn(z->curfn, z->include_filename);
    free(z->include_filename);
    z->include_filename = NULL;
    bool subfailed = zscan_do(z->zroot, z->rhs_dname, zfn, z->def_ttl);
    free(zfn);
    if (subfailed)
        siglongjmp(z->jbuf, 1);
}

// Input must have two bytes of text constrained to [0-9A-Fa-f]
F_NONNULL
static unsigned hexbyte(const char* intxt)
{
    gdnsd_assume(
        (intxt[0] >= '0' && intxt[0] <= '9')
        || (intxt[0] >= 'A' && intxt[0] <= 'F')
        || (intxt[0] >= 'a' && intxt[0] <= 'f')
    );
    gdnsd_assume(
        (intxt[1] >= '0' && intxt[1] <= '9')
        || (intxt[1] >= 'A' && intxt[1] <= 'F')
        || (intxt[1] >= 'a' && intxt[1] <= 'f')
    );

    int out;

    if (intxt[0] <= '9')
        out = (intxt[0] - '0') << 4;
    else
        out = ((intxt[0] | 0x20) - ('a' - 10)) << 4;

    if (intxt[1] <= '9')
        out |= (intxt[1] - '0');
    else
        out |= ((intxt[1] | 0x20) - ('a' - 10));

    gdnsd_assume(out >= 0 && out < 256);
    return (unsigned)out;
}

F_NONNULL
static void mult_uval(struct zscan* z, int fc)
{
    fc |= 0x20;
    switch (fc) {
    case 'm':
        z->uval *= 60;
        break;
    case 'h':
        z->uval *= 3600;
        break;
    case 'd':
        z->uval *= 86400;
        break;
    case 'w':
        z->uval *= 604800;
        break;
    default:
        gdnsd_assume(0);
    }
}

F_NONNULL
static void set_dyna(struct zscan* z, const char* fpc)
{
    unsigned dlen = fpc - z->tstart;
    if (dlen > 255)
        parse_error_noargs("DYNA/DYNC plugin!resource string cannot exceed 255 chars");
    memcpy(z->rhs_dyn, z->tstart, dlen);
    z->rhs_dyn[dlen] = 0;
    z->tstart = NULL;
}

F_NONNULL
static void set_caa_prop(struct zscan* z, const char* fpc)
{
    unsigned dlen = fpc - z->tstart;
    if (dlen > 255)
        parse_error_noargs("CAA property string cannot exceed 255 chars");
    memcpy(z->caa_prop, z->tstart, dlen);
    z->caa_prop[dlen] = 0;
    z->tstart = NULL;
}

static unsigned clamp_ttl(struct zscan* z, const uint8_t* dname, const unsigned rrtype, const unsigned ttl)
{
    if (ttl > gcfg->max_ttl) {
        parse_warn("Name '%s': TTL %u for type %s too large, clamped to max_ttl setting of %u",
                   logf_dname(dname), ttl, logf_rrtype(rrtype), gcfg->max_ttl);
        return gcfg->max_ttl;
    } else if (ttl < gcfg->min_ttl) {
        parse_warn("Name '%s': TTL %u for type %s too small, clamped to min_ttl setting of %u",
                   logf_dname(dname), ttl, logf_rrtype(rrtype), gcfg->min_ttl);
        return gcfg->min_ttl;
    }
    return ttl;
}

F_NONNULL
static void rec_nxd(struct zscan* z)
{
    if (ltree_add_rec_enxd(z->zroot, z->lhs_dname))
        siglongjmp(z->jbuf, 1);
}

F_NONNULL
static void rec_soa(struct zscan* z)
{
    if (dname_cmp(z->lhs_dname, z->zroot->c.dname))
        parse_error_noargs("SOA record can only be defined for the root of the zone");

    unsigned ncache = z->uv_5;
    // Here we clamp the negative TTL using min_ttl and max_ncache_ttl
    if (ncache > gcfg->max_ncache_ttl) {
        parse_warn("SOA negative-cache field %u too large, clamped to max_ncache_ttl setting of %u", ncache, gcfg->max_ncache_ttl);
        ncache = gcfg->max_ncache_ttl;
    } else if (ncache < gcfg->min_ttl) {
        parse_warn("SOA negative-cache field %u too small, clamped to min_ttl setting of %u", ncache, gcfg->min_ttl);
        ncache = gcfg->min_ttl;
    }

    // And here, we clamp the real RR TTL using min_ttl and the ncache value derived above
    if (z->ttl > ncache) {
        parse_warn("SOA TTL %u > ncache field %u, clamped to ncache value", z->ttl, ncache);
        z->ttl = ncache;
    } else if (z->ttl < gcfg->min_ttl) {
        parse_warn("SOA TTL %u too small, clamped to min_ttl setting of %u", z->ttl, gcfg->min_ttl);
        z->ttl = gcfg->min_ttl;
    }

    const unsigned rrtype = DNS_TYPE_SOA;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned ma_len = z->rhs_dname[0];
    const unsigned em_len = z->eml_dname[0];
    const unsigned rdlen = ma_len + em_len + 20U;
    uint8_t* rdata = xmalloc(2U + rdlen);
    uint8_t* rdwrite = rdata;
    gdnsd_put_una16(htons(rdlen), rdwrite);
    rdwrite += 2U;
    memcpy(rdwrite, &z->rhs_dname[1], ma_len);
    rdwrite += ma_len;
    memcpy(rdwrite, &z->eml_dname[1], em_len);
    rdwrite += em_len;
    gdnsd_put_una32(htonl(z->uv_1), rdwrite);
    rdwrite += 4U;
    gdnsd_put_una32(htonl(z->uv_2), rdwrite);
    rdwrite += 4U;
    gdnsd_put_una32(htonl(z->uv_3), rdwrite);
    rdwrite += 4U;
    gdnsd_put_una32(htonl(z->uv_4), rdwrite);
    rdwrite += 4U;
    gdnsd_put_una32(htonl(ncache), rdwrite);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void rec_a(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_A;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    uint8_t* rdata = xmalloc(6U);
    rdata[0] = 0;
    rdata[1] = 4U;
    memcpy(&rdata[2], &z->ipv4, 4U);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void rec_aaaa(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_AAAA;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    uint8_t* rdata = xmalloc(18U);
    rdata[0] = 0;
    rdata[1] = 16U;
    memcpy(&rdata[2], z->ipv6, 16U);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void rec_1name(struct zscan* z, unsigned rrtype)
{
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned rdlen = z->rhs_dname[0];
    uint8_t* rdata = xmalloc(2U + rdlen);
    gdnsd_put_una16(htons(rdlen), rdata);
    memcpy(&rdata[2], &z->rhs_dname[1], rdlen);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void rec_mx(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_MX;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned dnlen = z->rhs_dname[0];
    const unsigned rdlen = 2U + dnlen;
    uint8_t* rdata = xmalloc(2U + rdlen);
    gdnsd_put_una16(htons(rdlen), rdata);
    gdnsd_put_una16(htons(z->uval), &rdata[2]);
    memcpy(&rdata[4], &z->rhs_dname[1], dnlen);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void rec_srv(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_SRV;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned dnlen = z->rhs_dname[0];
    const unsigned rdlen = 6U + dnlen;
    uint8_t* rdata = xmalloc(2U + rdlen);
    gdnsd_put_una16(htons(rdlen), rdata);
    gdnsd_put_una16(htons(z->uv_1), &rdata[2]);
    gdnsd_put_una16(htons(z->uv_2), &rdata[4]);
    gdnsd_put_una16(htons(z->uv_3), &rdata[6]);
    memcpy(&rdata[8], &z->rhs_dname[1], dnlen);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
}

F_NONNULL
static void text_cleanup(struct zscan* z)
{
    if (z->text)
        free(z->text);
    z->text = NULL;
    z->text_len = 0;
}

F_NONNULL
static void rec_naptr(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_NAPTR;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned dnlen = z->rhs_dname[0];
    const unsigned rdlen = 4U + dnlen + z->text_len;
    uint8_t* rdata = xmalloc(2U + rdlen);
    gdnsd_put_una16(htons(rdlen), rdata);
    gdnsd_put_una16(htons(z->uv_1), &rdata[2]);
    gdnsd_put_una16(htons(z->uv_2), &rdata[4]);
    memcpy(&rdata[6], z->text, z->text_len);
    memcpy(&rdata[6U + z->text_len], &z->rhs_dname[1], dnlen);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
    text_cleanup(z);
}

F_NONNULL
static void rec_txt(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_TXT;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    const unsigned rdlen = z->text_len;
    uint8_t* rdata = xmalloc(2U + z->text_len);
    gdnsd_put_una16(htons(rdlen), rdata);
    memcpy(&rdata[2], z->text, z->text_len);
    if (ltree_add_rec(z->zroot, z->lhs_dname, rdata, rrtype, z->ttl)) {
        free(rdata);
        siglongjmp(z->jbuf, 1);
    }
    text_cleanup(z);
}

F_NONNULL
static void rec_dyna(struct zscan* z)
{
    z->ttl = clamp_ttl(z, z->lhs_dname, DNS_TYPE_A, z->ttl);
    if (ltree_add_rec_dynaddr(z->zroot, z->lhs_dname, z->rhs_dyn, z->ttl, z->ttl_min))
        siglongjmp(z->jbuf, 1);
}

F_NONNULL
static void rec_dync(struct zscan* z)
{
    z->ttl = clamp_ttl(z, z->lhs_dname, DNS_TYPE_CNAME, z->ttl);
    if (ltree_add_rec_dync(z->zroot, z->lhs_dname, z->rhs_dyn, z->ttl, z->ttl_min))
        siglongjmp(z->jbuf, 1);
}

F_NONNULL
static void rec_rfc3597(struct zscan* z)
{
    if (z->rfc3597_data_written < z->rfc3597_data_len)
        parse_error("RFC3597 %s claimed rdata length of %u, but only %u bytes of data present", logf_rrtype(z->uv_1), z->rfc3597_data_len - 2U, z->rfc3597_data_written - 2U);

    // There are good reasons to not allow RFC3597 definitions for these types,
    // because the rest of the code critically relies on correct rdata lengths
    // and the ability to process legit right-hand-side names, and the names
    // must be downcased for DNSSEC to work, etc.
    const unsigned rrtype = z->uv_1;
    if (rrtype == DNS_TYPE_A
            || rrtype == DNS_TYPE_AAAA
            || rrtype == DNS_TYPE_SOA
            || rrtype == DNS_TYPE_CNAME
            || rrtype == DNS_TYPE_NS
            || rrtype == DNS_TYPE_PTR
            || rrtype == DNS_TYPE_MX
            || rrtype == DNS_TYPE_SRV
            || rrtype == DNS_TYPE_NAPTR
            || rrtype == DNS_TYPE_RRSIG)
        parse_error("Name '%s': type %s not allowed, please use the explicit support built in for this RR type",
                    logf_dname(z->lhs_dname), logf_rrtype(rrtype));

    // These are just not allowed in any form
    if (rrtype == DNS_TYPE_HINFO
            || (rrtype == DNS_TYPE_OPT)
            || (rrtype == DNS_TYPE_NSEC)
            || (rrtype > 127 && rrtype < 256)
            || rrtype == 0)
        parse_error("Name '%s': %s not allowed",
                    logf_dname(z->lhs_dname), logf_rrtype(rrtype));

    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    if (ltree_add_rec(z->zroot, z->lhs_dname, z->rfc3597_data, rrtype, z->ttl))
        siglongjmp(z->jbuf, 1);
    z->rfc3597_data = NULL;
}

F_NONNULL
static void rec_ds(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_DS;

    // The parser already constrained the max values for these:
    const unsigned key_tag = z->uv_1;
    const unsigned dnssec_alg = z->uv_2;
    const unsigned digest_type = z->uv_3;

    if (!dnssec_alg)
        parse_error_noargs("DS record has reserved algorithm type zero");
    if (!digest_type)
        parse_error_noargs("DS record has reserved digest type zero");

    // The parse definition gives us 0-64 bytes of digest in an attempt to be
    // maximally compatible with future digests.  For the types we already know
    // about we should check for the right length here though:
    unsigned check_len = 0;
    switch (digest_type) {
    case 1: // SHA-1
        check_len = 20U;
        break;
    case 2: // SHA-256
        check_len = 32U;
        break;
    case 3: // GOST R 34.11-94
        check_len = 32U;
        break;
    case 4: // SHA-384
        check_len = 48U;
        break;
    default:
        check_len = 0;
    }
    if (check_len && z->rfc3597_data_written != check_len)
        parse_error("DS record digest length %u incorrect for algorithm %u",
                    z->rfc3597_data_len, digest_type);

    // Re-form rfc3597_data to be actual rdata by moving the digest up
    const unsigned rdlen = 2U + 1U + 1U + z->rfc3597_data_written;
    z->rfc3597_data = xrealloc(z->rfc3597_data, 2U + rdlen);
    memmove(&z->rfc3597_data[6], z->rfc3597_data, z->rfc3597_data_written);
    gdnsd_put_una16(htons(rdlen), z->rfc3597_data);
    gdnsd_put_una16(htons(key_tag), &z->rfc3597_data[2]);
    z->rfc3597_data[4] = dnssec_alg;
    z->rfc3597_data[5] = digest_type;

    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    if (ltree_add_rec(z->zroot, z->lhs_dname, z->rfc3597_data, rrtype, z->ttl))
        siglongjmp(z->jbuf, 1);
    z->rfc3597_data = NULL;
}

F_NONNULL
static void rec_caa(struct zscan* z)
{
    const unsigned rrtype = DNS_TYPE_CAA;
    z->ttl = clamp_ttl(z, z->lhs_dname, rrtype, z->ttl);
    if (z->uval > 255)
        parse_error("CAA flags byte value %u is >255", z->uval);

    const unsigned prop_len = strlen(z->caa_prop);
    gdnsd_assume(prop_len < 256); // parser-enforced
    const unsigned value_len = z->text_len;
    const unsigned total_len = 2 + prop_len + value_len;

    gdnsd_assume(total_len < 65536U);
    uint8_t* caa_rdata = xmalloc(2U + total_len);
    uint8_t* caa_write = caa_rdata;
    gdnsd_put_una16(htons(total_len), caa_write);
    caa_write += 2U;
    *caa_write++ = z->uval;
    *caa_write++ = prop_len;
    memcpy(caa_write, z->caa_prop, prop_len);
    caa_write += prop_len;
    memcpy(caa_write, z->text, value_len);

    if (ltree_add_rec(z->zroot, z->lhs_dname, caa_rdata, rrtype, z->ttl)) {
        free(caa_rdata);
        siglongjmp(z->jbuf, 1);
    }
    text_cleanup(z);
}

F_NONNULL
static void rfc3597_data_setup(struct zscan* z)
{
    z->rfc3597_data_len = z->uval + 2U;
    z->rfc3597_data = xmalloc(z->rfc3597_data_len);
    gdnsd_put_una16(htons(z->uval), z->rfc3597_data);
    z->rfc3597_data_written = 2U;
}

F_NONNULL
static void rfc3597_octet(struct zscan* z)
{
    if (z->rfc3597_data_written == z->rfc3597_data_len)
        parse_error_noargs("RFC3597 generic RR: more rdata is present than the indicated length");
    z->rfc3597_data[z->rfc3597_data_written++] = hexbyte(z->tstart);
}

F_NONNULL
static void dsdig_setup(struct zscan* z)
{
    z->rfc3597_data_len = 64U; // all known digests are <= 48U to date
    z->rfc3597_data = xmalloc(z->rfc3597_data_len);
    z->rfc3597_data_written = 0U;
}

F_NONNULL
static void dsdig_octet(struct zscan* z)
{
    if (z->rfc3597_data_written == z->rfc3597_data_len)
        parse_error_noargs("DS Digest field too long");
    z->rfc3597_data[z->rfc3597_data_written++] = hexbyte(z->tstart);
}

F_NONNULL
static void zone_add_zsk(struct zscan* z, unsigned algid)
{
    if (dnssec_add_ephemeral_zsk(z->zroot, algid))
        siglongjmp(z->jbuf, 1);
}

// The external entrypoint to the parser
bool zscan_rfc1035(struct ltree_node_zroot* zroot, const char* fn)
{
    gdnsd_assume(zroot->c.dname);
    return zscan_do(zroot, zroot->c.dname, fn, gcfg->zones_default_ttl);
}

// This pre-processor does two important things that vastly simplify the real
// ragel parser:
// 1) Gets rid of all comments, replacing their characters with spaces so that
//    they're just seen as excessive whitespace.  Technically we only needed
//    to strip comments for the () case below, which is the complicated one
//    for ragel, but since we're doing it anyways it seemed simpler to do
//    universally and take comment-handling out of ragel as well.
// 2) Gets rid of all awful rfc1035 () line continuation, replacing the
//    parentheses themselves with spaces, and replacing any embedded newlines
//    with the formfeed character \f (which the ragel parser treats as
//    whitespace, but also knows to increment linecount on these so that error
//    reporting still shows the correct line number).

#define preproc_err(_msg) \
    do {\
        log_err("rfc1035: Zone %s: Zonefile preprocessing error at file %s line %zu: " _msg, logf_dname(z->zroot->c.dname), z->curfn, line_num);\
        siglongjmp(z->jbuf, 1);\
    } while (0)

F_NONNULL
static void preprocess_buf(struct zscan* z, char* buf, const size_t buflen)
{
    // This is validated with a user-facing error before calling this function!
    gdnsd_assume(buf[buflen - 1] == '\n');

    bool in_quotes = false;
    bool in_parens = false;
    size_t line_num = 1;
    for (size_t i = 0; i < buflen; i++) {
        switch (buf[i]) {
        case '\n':
            line_num++;
            // In parens, replace \n with \f.  The ragel parser treats \f as
            // whitespace but knows to increment the line count so that error
            // reports are sane, while true unescaped \n terminates records.
            if (in_parens && !in_quotes)
                buf[i] = '\f';
            break;
        case ';':
            if (!in_quotes) {
                // Note we don't check i < buflen while advancing here, because
                // there's a check that the final character of the buffer must
                // be '\n' before the preprocessor is even invoked, which is
                // re-asserted at the top of this function.
                do {
                    buf[i++] = ' ';
                } while (buf[i] != '\n');
                line_num++;
                if (in_parens)
                    buf[i] = '\f';
            }
            break;
        case '"':
            in_quotes = !in_quotes;
            break;
        case '(':
            if (!in_quotes) {
                if (in_parens)
                    preproc_err("Parentheses double-opened");
                in_parens = true;
                buf[i] = ' ';
            }
            break;
        case ')':
            if (!in_quotes) {
                if (!in_parens)
                    preproc_err("Parentheses double-closed");
                in_parens = false;
                buf[i] = ' ';
            }
            break;
        case '\\':
            // Skip one escaped char.  Note 3-digit escapes exist as well, but
            // we're only concerned here with escaping of metachars, so it
            // turns out we don't have to track for the 3-digit escapes here.
            // We do have to keep the line count accurate in the case of an
            // escaped newline, though.
            if (buf[++i] == '\n')
                line_num++;
            break;
        case '\f':
            // Because \f is a special metachar for our ()-handling
            if (!in_quotes)
                preproc_err("Literal formfeed character not allowed in unquoted text: please escape it!");
            break;
        default:
            break;
        }
    }

    if (in_quotes)
        preproc_err("Unterminated open double-quote at EOF");
    if (in_parens)
        preproc_err("Unterminated open parentheses at EOF");
}

// *INDENT-OFF*
// start-sonar-exclude

#line 1041 "src/zscan_rfc1035.c"
static const int zone_start = 496;
static const int zone_first_final = 496;
static const int zone_error = 0;

static const int zone_en_main = 496;


#line 1275 "./src/zscan_rfc1035.rl"

// end-sonar-exclude

F_NONNULL
static void scanner(struct zscan* z, char* buf, const size_t bufsize)
{
    gdnsd_assume(bufsize);

    // This avoids the unfortunately common case of files with final lines
    //   that are unterminated by bailing out early.  This also incidentally
    //   but importantly protects from set_uval()'s strtoul running off the
    //   end of the buffer if we were parsing an integer at that point.
    if (buf[bufsize - 1] != '\n') {
        parse_error_noargs("No newline at end of file");
        return;
    }

    // Undo parentheses braindamage before real parsing
    preprocess_buf(z, buf, bufsize);

    (void)zone_en_main; // silence unused var warning from generated code

    int cs = zone_start;

    GDNSD_DIAG_PUSH_IGNORED("-Wswitch-default")
    GDNSD_DIAG_PUSH_IGNORED("-Wimplicit-fallthrough")
// start-sonar-exclude
#ifndef __clang_analyzer__
    // ^ ... because the ragel-generated code for the zonefile parser is
    //   so huge that it makes analyzer runs take forever.
    const char* p = buf;
    const char* pe = buf + bufsize;
    const char* eof = pe;
    
#line 1080 "src/zscan_rfc1035.c"
	{
	if ( p == pe )
		goto _test_eof;
	switch ( cs )
	{
case 496:
	switch( (*p) ) {
		case 9: goto st12;
		case 10: goto st497;
		case 12: goto st13;
		case 32: goto st12;
		case 34: goto tr1173;
		case 36: goto st391;
		case 59: goto st0;
		case 92: goto tr1175;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1172;
tr1172:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st1;
tr1137:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st1;
tr1176:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st1;
st1:
	if ( ++p == pe )
		goto _test_eof1;
case 1:
#line 1113 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1;
		case 10: goto st0;
		case 12: goto tr3;
		case 32: goto tr1;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st476;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st1;
tr1:
#line 1046 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->lhs_dname, p - z->tstart, true); }
	goto st2;
tr17:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st2;
tr960:
#line 1047 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->lhs_dname, p - z->tstart - 1, true); }
	goto st2;
tr1138:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1046 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->lhs_dname, p - z->tstart, true); }
	goto st2;
st2:
	if ( ++p == pe )
		goto _test_eof2;
case 2:
#line 1142 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st2;
		case 12: goto st3;
		case 32: goto st2;
		case 65: goto tr8;
		case 67: goto tr9;
		case 68: goto tr10;
		case 73: goto tr11;
		case 77: goto tr12;
		case 78: goto tr13;
		case 80: goto tr14;
		case 83: goto tr15;
		case 84: goto tr16;
		case 97: goto tr8;
		case 99: goto tr9;
		case 100: goto tr10;
		case 105: goto tr11;
		case 109: goto tr12;
		case 110: goto tr13;
		case 112: goto tr14;
		case 115: goto tr15;
		case 116: goto tr16;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr7;
	goto st0;
st0:
cs = 0;
	goto _out;
tr3:
#line 1046 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->lhs_dname, p - z->tstart, true); }
	goto st3;
tr18:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st3;
tr961:
#line 1047 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->lhs_dname, p - z->tstart - 1, true); }
	goto st3;
tr1139:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1046 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->lhs_dname, p - z->tstart, true); }
	goto st3;
st3:
	if ( ++p == pe )
		goto _test_eof3;
case 3:
#line 1188 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr17;
		case 12: goto tr18;
		case 32: goto tr17;
		case 65: goto tr20;
		case 67: goto tr21;
		case 68: goto tr22;
		case 73: goto tr23;
		case 77: goto tr24;
		case 78: goto tr25;
		case 80: goto tr26;
		case 83: goto tr27;
		case 84: goto tr28;
		case 97: goto tr20;
		case 99: goto tr21;
		case 100: goto tr22;
		case 105: goto tr23;
		case 109: goto tr24;
		case 110: goto tr25;
		case 112: goto tr26;
		case 115: goto tr27;
		case 116: goto tr28;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr19;
	goto st0;
tr7:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st4;
tr19:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st4;
st4:
	if ( ++p == pe )
		goto _test_eof4;
case 4:
#line 1229 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr29;
		case 12: goto tr30;
		case 32: goto tr29;
		case 47: goto tr31;
		case 68: goto tr33;
		case 72: goto tr33;
		case 77: goto tr33;
		case 87: goto tr33;
		case 100: goto tr33;
		case 104: goto tr33;
		case 109: goto tr33;
		case 119: goto tr33;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st4;
	goto st0;
tr45:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st5;
tr29:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st5;
tr1169:
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st5;
st5:
	if ( ++p == pe )
		goto _test_eof5;
case 5:
#line 1264 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st5;
		case 12: goto st6;
		case 32: goto st5;
		case 65: goto st7;
		case 67: goto st14;
		case 68: goto st54;
		case 73: goto st486;
		case 77: goto st150;
		case 78: goto st168;
		case 80: goto st252;
		case 83: goto st268;
		case 84: goto st341;
		case 97: goto st7;
		case 99: goto st14;
		case 100: goto st54;
		case 105: goto st486;
		case 109: goto st150;
		case 110: goto st168;
		case 112: goto st252;
		case 115: goto st268;
		case 116: goto st341;
	}
	goto st0;
tr46:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st6;
tr30:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st6;
tr1170:
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st6;
st6:
	if ( ++p == pe )
		goto _test_eof6;
case 6:
#line 1306 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr45;
		case 12: goto tr46;
		case 32: goto tr45;
		case 65: goto tr47;
		case 67: goto tr48;
		case 68: goto tr49;
		case 73: goto tr50;
		case 77: goto tr51;
		case 78: goto tr52;
		case 80: goto tr53;
		case 83: goto tr54;
		case 84: goto tr55;
		case 97: goto tr47;
		case 99: goto tr48;
		case 100: goto tr49;
		case 105: goto tr50;
		case 109: goto tr51;
		case 110: goto tr52;
		case 112: goto tr53;
		case 115: goto tr54;
		case 116: goto tr55;
	}
	goto st0;
tr8:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st7;
tr47:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st7;
tr20:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st7;
st7:
	if ( ++p == pe )
		goto _test_eof7;
case 7:
#line 1344 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st8;
		case 12: goto st9;
		case 32: goto st8;
		case 65: goto st480;
		case 97: goto st480;
	}
	goto st0;
tr60:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st8;
st8:
	if ( ++p == pe )
		goto _test_eof8;
case 8:
#line 1359 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st8;
		case 12: goto st9;
		case 32: goto st8;
		case 46: goto tr59;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr59;
	goto st0;
tr61:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st9;
st9:
	if ( ++p == pe )
		goto _test_eof9;
case 9:
#line 1375 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr60;
		case 12: goto tr61;
		case 32: goto tr60;
		case 46: goto tr62;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr62;
	goto st0;
tr59:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st10;
tr62:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st10;
st10:
	if ( ++p == pe )
		goto _test_eof10;
case 10:
#line 1395 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr63;
		case 10: goto tr64;
		case 12: goto tr65;
		case 32: goto tr63;
		case 46: goto st10;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st10;
	goto st0;
tr106:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st11;
tr63:
#line 1072 "./src/zscan_rfc1035.rl"
	{ set_ipv4(z, p); }
#line 1094 "./src/zscan_rfc1035.rl"
	{ rec_a(z); }
	goto st11;
tr102:
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st11;
tr111:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st11;
tr129:
#line 1070 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok_huge(z, p - z->tstart - 1); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st11;
tr143:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st11;
tr150:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st11;
tr168:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st11;
tr285:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st11;
tr295:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st11;
tr315:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st11;
tr325:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st11;
tr372:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st11;
tr379:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st11;
tr397:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st11;
tr459:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st11;
tr466:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st11;
tr484:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st11;
tr567:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st11;
tr574:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st11;
tr592:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st11;
tr603:
#line 1108 "./src/zscan_rfc1035.rl"
	{ rec_nxd(z); }
	goto st11;
tr614:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st11;
tr621:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st11;
tr639:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st11;
tr711:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st11;
tr716:
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st11;
tr809:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st11;
tr816:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st11;
tr834:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st11;
tr989:
#line 1115 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ED25519); }
	goto st11;
tr995:
#line 1116 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ECDSAP256SHA256); }
	goto st11;
tr1022:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st11;
tr1029:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st11;
tr1047:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st11;
tr1089:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st11;
tr1096:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st11;
tr1114:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st11;
tr1127:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st11;
tr1132:
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st11;
tr1150:
#line 1073 "./src/zscan_rfc1035.rl"
	{ set_ipv6(z, p); }
#line 1095 "./src/zscan_rfc1035.rl"
	{ rec_aaaa(z); }
	goto st11;
st11:
	if ( ++p == pe )
		goto _test_eof11;
case 11:
#line 1589 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st11;
		case 10: goto st497;
		case 12: goto st25;
		case 32: goto st11;
	}
	goto st0;
tr73:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st497;
tr64:
#line 1072 "./src/zscan_rfc1035.rl"
	{ set_ipv4(z, p); }
#line 1094 "./src/zscan_rfc1035.rl"
	{ rec_a(z); }
	goto st497;
tr103:
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st497;
tr1178:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st497;
tr112:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st497;
tr130:
#line 1070 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok_huge(z, p - z->tstart - 1); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st497;
tr144:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st497;
tr151:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st497;
tr169:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st497;
tr207:
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st497;
tr211:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st497;
tr216:
#line 1113 "./src/zscan_rfc1035.rl"
	{ dsdig_octet(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st497;
tr220:
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st497;
tr224:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st497;
tr286:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st497;
tr296:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st497;
tr316:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st497;
tr326:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st497;
tr373:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st497;
tr380:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st497;
tr398:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st497;
tr460:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st497;
tr467:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st497;
tr485:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st497;
tr568:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st497;
tr575:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st497;
tr593:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st497;
tr604:
#line 1108 "./src/zscan_rfc1035.rl"
	{ rec_nxd(z); }
	goto st497;
tr615:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st497;
tr622:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st497;
tr640:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st497;
tr712:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st497;
tr717:
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st497;
tr810:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st497;
tr817:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st497;
tr835:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st497;
tr851:
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
#line 1102 "./src/zscan_rfc1035.rl"
	{ rec_txt(z); }
	goto st497;
tr857:
#line 1102 "./src/zscan_rfc1035.rl"
	{ rec_txt(z); }
	goto st497;
tr863:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1102 "./src/zscan_rfc1035.rl"
	{ rec_txt(z); }
	goto st497;
tr877:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
#line 1102 "./src/zscan_rfc1035.rl"
	{ rec_txt(z); }
	goto st497;
tr885:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
#line 1102 "./src/zscan_rfc1035.rl"
	{ rec_txt(z); }
	goto st497;
tr920:
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st497;
tr924:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st497;
tr929:
#line 1111 "./src/zscan_rfc1035.rl"
	{ rfc3597_octet(z); }
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st497;
tr990:
#line 1115 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ED25519); }
	goto st497;
tr996:
#line 1116 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ECDSAP256SHA256); }
	goto st497;
tr1013:
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1023:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1030:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1048:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1056:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1074:
#line 1061 "./src/zscan_rfc1035.rl"
	{ z->tstart++; set_filename(z, p - z->tstart - 1); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st497;
tr1090:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st497;
tr1097:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st497;
tr1115:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st497;
tr1128:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st497;
tr1133:
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st497;
tr1151:
#line 1073 "./src/zscan_rfc1035.rl"
	{ set_ipv6(z, p); }
#line 1095 "./src/zscan_rfc1035.rl"
	{ rec_aaaa(z); }
	goto st497;
st497:
	if ( ++p == pe )
		goto _test_eof497;
case 497:
#line 1847 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1177;
		case 10: goto tr1178;
		case 12: goto tr1179;
		case 32: goto tr1177;
		case 34: goto tr1180;
		case 36: goto tr1181;
		case 59: goto st0;
		case 92: goto tr1182;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1176;
tr72:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st12;
tr1177:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st12;
st12:
	if ( ++p == pe )
		goto _test_eof12;
case 12:
#line 1870 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st12;
		case 10: goto st497;
		case 12: goto st13;
		case 32: goto st12;
		case 65: goto tr8;
		case 67: goto tr9;
		case 68: goto tr10;
		case 73: goto tr11;
		case 77: goto tr12;
		case 78: goto tr13;
		case 80: goto tr14;
		case 83: goto tr15;
		case 84: goto tr16;
		case 97: goto tr8;
		case 99: goto tr9;
		case 100: goto tr10;
		case 105: goto tr11;
		case 109: goto tr12;
		case 110: goto tr13;
		case 112: goto tr14;
		case 115: goto tr15;
		case 116: goto tr16;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr7;
	goto st0;
tr74:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st13;
tr1179:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st13;
st13:
	if ( ++p == pe )
		goto _test_eof13;
case 13:
#line 1907 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr72;
		case 10: goto tr73;
		case 12: goto tr74;
		case 32: goto tr72;
		case 65: goto tr20;
		case 67: goto tr21;
		case 68: goto tr22;
		case 73: goto tr23;
		case 77: goto tr24;
		case 78: goto tr25;
		case 80: goto tr26;
		case 83: goto tr27;
		case 84: goto tr28;
		case 97: goto tr20;
		case 99: goto tr21;
		case 100: goto tr22;
		case 105: goto tr23;
		case 109: goto tr24;
		case 110: goto tr25;
		case 112: goto tr26;
		case 115: goto tr27;
		case 116: goto tr28;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr19;
	goto st0;
tr9:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st14;
tr48:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st14;
tr21:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st14;
st14:
	if ( ++p == pe )
		goto _test_eof14;
case 14:
#line 1948 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 65: goto st15;
		case 78: goto st37;
		case 97: goto st15;
		case 110: goto st37;
	}
	goto st0;
st15:
	if ( ++p == pe )
		goto _test_eof15;
case 15:
	switch( (*p) ) {
		case 65: goto st16;
		case 97: goto st16;
	}
	goto st0;
st16:
	if ( ++p == pe )
		goto _test_eof16;
case 16:
	switch( (*p) ) {
		case 9: goto st17;
		case 12: goto st18;
		case 32: goto st17;
	}
	goto st0;
tr81:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st17;
st17:
	if ( ++p == pe )
		goto _test_eof17;
case 17:
#line 1981 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st17;
		case 12: goto st18;
		case 32: goto st17;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr80;
	goto st0;
tr82:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st18;
st18:
	if ( ++p == pe )
		goto _test_eof18;
case 18:
#line 1996 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr81;
		case 12: goto tr82;
		case 32: goto tr81;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr83;
	goto st0;
tr80:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st19;
tr83:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st19;
st19:
	if ( ++p == pe )
		goto _test_eof19;
case 19:
#line 2015 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr84;
		case 12: goto tr85;
		case 32: goto tr84;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st19;
	goto st0;
tr90:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st20;
tr84:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
	goto st20;
st20:
	if ( ++p == pe )
		goto _test_eof20;
case 20:
#line 2033 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st20;
		case 12: goto st21;
		case 32: goto st20;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr89;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr89;
	} else
		goto tr89;
	goto st0;
tr91:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st21;
tr85:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
	goto st21;
st21:
	if ( ++p == pe )
		goto _test_eof21;
case 21:
#line 2057 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr90;
		case 12: goto tr91;
		case 32: goto tr90;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr92;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto tr92;
	} else
		goto tr92;
	goto st0;
tr89:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st22;
tr92:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st22;
st22:
	if ( ++p == pe )
		goto _test_eof22;
case 22:
#line 2082 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr93;
		case 12: goto tr94;
		case 32: goto tr93;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st22;
	} else if ( (*p) > 90 ) {
		if ( 97 <= (*p) && (*p) <= 122 )
			goto st22;
	} else
		goto st22;
	goto st0;
tr117:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st23;
tr93:
#line 1091 "./src/zscan_rfc1035.rl"
	{ set_caa_prop(z, p); }
	goto st23;
st23:
	if ( ++p == pe )
		goto _test_eof23;
case 23:
#line 2106 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st23;
		case 10: goto st0;
		case 12: goto st30;
		case 32: goto st23;
		case 34: goto tr99;
		case 59: goto st0;
		case 92: goto tr100;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr96;
tr96:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st24;
tr110:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st24;
tr116:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st24;
st24:
	if ( ++p == pe )
		goto _test_eof24;
case 24:
#line 2134 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr102;
		case 10: goto tr103;
		case 12: goto tr104;
		case 32: goto tr102;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st26;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st24;
tr107:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st25;
tr65:
#line 1072 "./src/zscan_rfc1035.rl"
	{ set_ipv4(z, p); }
#line 1094 "./src/zscan_rfc1035.rl"
	{ rec_a(z); }
	goto st25;
tr104:
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st25;
tr113:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1069 "./src/zscan_rfc1035.rl"
	{ text_add_tok_huge(z, p - z->tstart); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st25;
tr131:
#line 1070 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok_huge(z, p - z->tstart - 1); }
#line 1106 "./src/zscan_rfc1035.rl"
	{ rec_caa(z); }
	goto st25;
tr145:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st25;
tr152:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st25;
tr170:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1097 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_CNAME); }
	goto st25;
tr287:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st25;
tr297:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1103 "./src/zscan_rfc1035.rl"
	{ rec_dyna(z); }
	goto st25;
tr317:
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st25;
tr327:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1090 "./src/zscan_rfc1035.rl"
	{ set_dyna(z, p); }
#line 1104 "./src/zscan_rfc1035.rl"
	{ rec_dync(z); }
	goto st25;
tr374:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st25;
tr381:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st25;
tr399:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1099 "./src/zscan_rfc1035.rl"
	{ rec_mx(z); }
	goto st25;
tr461:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st25;
tr468:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st25;
tr486:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1101 "./src/zscan_rfc1035.rl"
	{ rec_naptr(z); }
	goto st25;
tr569:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st25;
tr576:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st25;
tr594:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1096 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_NS); }
	goto st25;
tr605:
#line 1108 "./src/zscan_rfc1035.rl"
	{ rec_nxd(z); }
	goto st25;
tr616:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st25;
tr623:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st25;
tr641:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1098 "./src/zscan_rfc1035.rl"
	{ rec_1name(z, DNS_TYPE_PTR); }
	goto st25;
tr713:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st25;
tr718:
#line 1088 "./src/zscan_rfc1035.rl"
	{ z->uv_5 = z->uval; }
#line 1093 "./src/zscan_rfc1035.rl"
	{ rec_soa(z); }
	goto st25;
tr811:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st25;
tr818:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st25;
tr836:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1100 "./src/zscan_rfc1035.rl"
	{ rec_srv(z); }
	goto st25;
tr991:
#line 1115 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ED25519); }
	goto st25;
tr997:
#line 1116 "./src/zscan_rfc1035.rl"
	{ zone_add_zsk(z, DNSSEC_ALG_ECDSAP256SHA256); }
	goto st25;
tr1024:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st25;
tr1031:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st25;
tr1049:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1062 "./src/zscan_rfc1035.rl"
	{ process_include(z); }
	goto st25;
tr1091:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st25;
tr1098:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st25;
tr1116:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
#line 1055 "./src/zscan_rfc1035.rl"
	{
        validate_origin_in_zone(z, z->rhs_dname);
        dname_copy(z->origin, z->rhs_dname);
    }
	goto st25;
tr1129:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st25;
tr1134:
#line 1081 "./src/zscan_rfc1035.rl"
	{ z->def_ttl = z->uval; }
	goto st25;
tr1152:
#line 1073 "./src/zscan_rfc1035.rl"
	{ set_ipv6(z, p); }
#line 1095 "./src/zscan_rfc1035.rl"
	{ rec_aaaa(z); }
	goto st25;
st25:
	if ( ++p == pe )
		goto _test_eof25;
case 25:
#line 2330 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr106;
		case 10: goto tr73;
		case 12: goto tr107;
		case 32: goto tr106;
	}
	goto st0;
tr100:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st26;
tr114:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st26;
tr120:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st26;
st26:
	if ( ++p == pe )
		goto _test_eof26;
case 26:
#line 2353 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st27;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st28;
	goto st24;
st27:
	if ( ++p == pe )
		goto _test_eof27;
case 27:
	switch( (*p) ) {
		case 9: goto tr111;
		case 10: goto tr112;
		case 12: goto tr113;
		case 32: goto tr111;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr114;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr110;
st28:
	if ( ++p == pe )
		goto _test_eof28;
case 28:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st29;
	goto st0;
st29:
	if ( ++p == pe )
		goto _test_eof29;
case 29:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st24;
	goto st0;
tr118:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st30;
tr94:
#line 1091 "./src/zscan_rfc1035.rl"
	{ set_caa_prop(z, p); }
	goto st30;
st30:
	if ( ++p == pe )
		goto _test_eof30;
case 30:
#line 2398 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr117;
		case 10: goto st0;
		case 12: goto tr118;
		case 32: goto tr117;
		case 34: goto tr119;
		case 59: goto st0;
		case 92: goto tr120;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr116;
tr99:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st31;
tr125:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st31;
tr119:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st31;
st31:
	if ( ++p == pe )
		goto _test_eof31;
case 31:
#line 2426 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st32;
		case 34: goto st33;
		case 92: goto st34;
	}
	goto st31;
tr126:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st32;
st32:
	if ( ++p == pe )
		goto _test_eof32;
case 32:
#line 2439 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr126;
		case 34: goto tr127;
		case 92: goto tr128;
	}
	goto tr125;
tr127:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st33;
st33:
	if ( ++p == pe )
		goto _test_eof33;
case 33:
#line 2452 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr129;
		case 10: goto tr130;
		case 12: goto tr131;
		case 32: goto tr129;
	}
	goto st0;
tr128:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st34;
st34:
	if ( ++p == pe )
		goto _test_eof34;
case 34:
#line 2466 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st32;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st35;
	goto st31;
st35:
	if ( ++p == pe )
		goto _test_eof35;
case 35:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st36;
	goto st0;
st36:
	if ( ++p == pe )
		goto _test_eof36;
case 36:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st31;
	goto st0;
st37:
	if ( ++p == pe )
		goto _test_eof37;
case 37:
	switch( (*p) ) {
		case 65: goto st38;
		case 97: goto st38;
	}
	goto st0;
st38:
	if ( ++p == pe )
		goto _test_eof38;
case 38:
	switch( (*p) ) {
		case 77: goto st39;
		case 109: goto st39;
	}
	goto st0;
st39:
	if ( ++p == pe )
		goto _test_eof39;
case 39:
	switch( (*p) ) {
		case 69: goto st40;
		case 101: goto st40;
	}
	goto st0;
st40:
	if ( ++p == pe )
		goto _test_eof40;
case 40:
	switch( (*p) ) {
		case 9: goto st41;
		case 12: goto st47;
		case 32: goto st41;
	}
	goto st0;
tr156:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st41;
st41:
	if ( ++p == pe )
		goto _test_eof41;
case 41:
#line 2529 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st41;
		case 10: goto st0;
		case 12: goto st47;
		case 32: goto st41;
		case 34: goto tr140;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr141;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr139;
tr139:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st42;
tr155:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st42;
tr149:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st42;
st42:
	if ( ++p == pe )
		goto _test_eof42;
case 42:
#line 2556 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr143;
		case 10: goto tr144;
		case 12: goto tr145;
		case 32: goto tr143;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st43;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st42;
tr141:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st43;
tr159:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st43;
tr153:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st43;
st43:
	if ( ++p == pe )
		goto _test_eof43;
case 43:
#line 2582 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st44;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st45;
	goto st42;
st44:
	if ( ++p == pe )
		goto _test_eof44;
case 44:
	switch( (*p) ) {
		case 9: goto tr150;
		case 10: goto tr151;
		case 12: goto tr152;
		case 32: goto tr150;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr153;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr149;
st45:
	if ( ++p == pe )
		goto _test_eof45;
case 45:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st46;
	goto st0;
st46:
	if ( ++p == pe )
		goto _test_eof46;
case 46:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st42;
	goto st0;
tr157:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st47;
st47:
	if ( ++p == pe )
		goto _test_eof47;
case 47:
#line 2624 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr156;
		case 10: goto st0;
		case 12: goto tr157;
		case 32: goto tr156;
		case 34: goto tr158;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr159;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr155;
tr140:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st48;
tr158:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st48;
tr164:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st48;
st48:
	if ( ++p == pe )
		goto _test_eof48;
case 48:
#line 2651 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st49;
		case 34: goto st50;
		case 92: goto st51;
	}
	goto st48;
tr165:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st49;
st49:
	if ( ++p == pe )
		goto _test_eof49;
case 49:
#line 2664 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr165;
		case 34: goto tr166;
		case 92: goto tr167;
	}
	goto tr164;
tr166:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st50;
st50:
	if ( ++p == pe )
		goto _test_eof50;
case 50:
#line 2677 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr168;
		case 10: goto tr169;
		case 12: goto tr170;
		case 32: goto tr168;
	}
	goto st0;
tr167:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st51;
st51:
	if ( ++p == pe )
		goto _test_eof51;
case 51:
#line 2691 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st49;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st52;
	goto st48;
st52:
	if ( ++p == pe )
		goto _test_eof52;
case 52:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st53;
	goto st0;
st53:
	if ( ++p == pe )
		goto _test_eof53;
case 53:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st48;
	goto st0;
tr10:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st54;
tr49:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st54;
tr22:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st54;
st54:
	if ( ++p == pe )
		goto _test_eof54;
case 54:
#line 2726 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 83: goto st55;
		case 89: goto st113;
		case 115: goto st55;
		case 121: goto st113;
	}
	goto st0;
st55:
	if ( ++p == pe )
		goto _test_eof55;
case 55:
	switch( (*p) ) {
		case 9: goto st56;
		case 12: goto st57;
		case 32: goto st56;
	}
	goto st0;
tr178:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st56;
st56:
	if ( ++p == pe )
		goto _test_eof56;
case 56:
#line 2750 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st56;
		case 12: goto st57;
		case 32: goto st56;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr177;
	goto st0;
tr179:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st57;
st57:
	if ( ++p == pe )
		goto _test_eof57;
case 57:
#line 2765 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr178;
		case 12: goto tr179;
		case 32: goto tr178;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr180;
	goto st0;
tr177:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st58;
tr180:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st58;
st58:
	if ( ++p == pe )
		goto _test_eof58;
case 58:
#line 2784 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr181;
		case 12: goto tr182;
		case 32: goto tr181;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st58;
	goto st0;
tr189:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st59;
tr181:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st59;
st59:
	if ( ++p == pe )
		goto _test_eof59;
case 59:
#line 2803 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st59;
		case 12: goto st60;
		case 32: goto st59;
		case 69: goto st71;
		case 82: goto st104;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr186;
	goto st0;
tr190:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st60;
tr182:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st60;
st60:
	if ( ++p == pe )
		goto _test_eof60;
case 60:
#line 2824 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr189;
		case 12: goto tr190;
		case 32: goto tr189;
		case 69: goto tr192;
		case 82: goto tr193;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr191;
	goto st0;
tr186:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st61;
tr191:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st61;
st61:
	if ( ++p == pe )
		goto _test_eof61;
case 61:
#line 2845 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr194;
		case 12: goto tr195;
		case 32: goto tr194;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st61;
	goto st0;
tr200:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st62;
tr194:
#line 1075 "./src/zscan_rfc1035.rl"
	{ set_uval8(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
tr243:
#line 1203 "./src/zscan_rfc1035.rl"
	{ z->uval = 13U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
tr253:
#line 1204 "./src/zscan_rfc1035.rl"
	{ z->uval = 14U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
tr261:
#line 1205 "./src/zscan_rfc1035.rl"
	{ z->uval = 15U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
tr265:
#line 1206 "./src/zscan_rfc1035.rl"
	{ z->uval = 16U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
tr275:
#line 1202 "./src/zscan_rfc1035.rl"
	{ z->uval = 8U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st62;
st62:
	if ( ++p == pe )
		goto _test_eof62;
case 62:
#line 2884 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st62;
		case 12: goto st63;
		case 32: goto st62;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr199;
	goto st0;
tr201:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st63;
tr195:
#line 1075 "./src/zscan_rfc1035.rl"
	{ set_uval8(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
tr244:
#line 1203 "./src/zscan_rfc1035.rl"
	{ z->uval = 13U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
tr254:
#line 1204 "./src/zscan_rfc1035.rl"
	{ z->uval = 14U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
tr262:
#line 1205 "./src/zscan_rfc1035.rl"
	{ z->uval = 15U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
tr266:
#line 1206 "./src/zscan_rfc1035.rl"
	{ z->uval = 16U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
tr276:
#line 1202 "./src/zscan_rfc1035.rl"
	{ z->uval = 8U; }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st63;
st63:
	if ( ++p == pe )
		goto _test_eof63;
case 63:
#line 2923 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr200;
		case 12: goto tr201;
		case 32: goto tr200;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr202;
	goto st0;
tr199:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st64;
tr202:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st64;
st64:
	if ( ++p == pe )
		goto _test_eof64;
case 64:
#line 2942 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr203;
		case 12: goto tr204;
		case 32: goto tr203;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st64;
	goto st0;
tr203:
#line 1075 "./src/zscan_rfc1035.rl"
	{ set_uval8(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st65;
tr206:
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st65;
tr210:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st65;
st65:
	if ( ++p == pe )
		goto _test_eof65;
case 65:
#line 2967 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr206;
		case 10: goto tr207;
		case 12: goto tr208;
		case 32: goto tr206;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr209;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr209;
	} else
		goto tr209;
	goto st0;
tr204:
#line 1075 "./src/zscan_rfc1035.rl"
	{ set_uval8(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st66;
tr208:
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st66;
tr212:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1107 "./src/zscan_rfc1035.rl"
	{ rec_ds(z); }
	goto st66;
st66:
	if ( ++p == pe )
		goto _test_eof66;
case 66:
#line 2999 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr210;
		case 10: goto tr211;
		case 12: goto tr212;
		case 32: goto tr210;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr213;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr213;
	} else
		goto tr213;
	goto st0;
tr222:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st67;
tr226:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st67;
tr209:
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st67;
tr213:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1112 "./src/zscan_rfc1035.rl"
	{ dsdig_setup(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st67;
tr218:
#line 1113 "./src/zscan_rfc1035.rl"
	{ dsdig_octet(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st67;
st67:
	if ( ++p == pe )
		goto _test_eof67;
case 67:
#line 3038 "src/zscan_rfc1035.c"
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st68;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto st68;
	} else
		goto st68;
	goto st0;
st68:
	if ( ++p == pe )
		goto _test_eof68;
case 68:
	switch( (*p) ) {
		case 9: goto tr215;
		case 10: goto tr216;
		case 12: goto tr217;
		case 32: goto tr215;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr218;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr218;
	} else
		goto tr218;
	goto st0;
tr223:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st69;
tr215:
#line 1113 "./src/zscan_rfc1035.rl"
	{ dsdig_octet(z); }
	goto st69;
st69:
	if ( ++p == pe )
		goto _test_eof69;
case 69:
#line 3076 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st69;
		case 10: goto tr220;
		case 12: goto st70;
		case 32: goto st69;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr222;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr222;
	} else
		goto tr222;
	goto st0;
tr225:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st70;
tr217:
#line 1113 "./src/zscan_rfc1035.rl"
	{ dsdig_octet(z); }
	goto st70;
st70:
	if ( ++p == pe )
		goto _test_eof70;
case 70:
#line 3101 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr223;
		case 10: goto tr224;
		case 12: goto tr225;
		case 32: goto tr223;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr226;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr226;
	} else
		goto tr226;
	goto st0;
tr192:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st71;
st71:
	if ( ++p == pe )
		goto _test_eof71;
case 71:
#line 3123 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 67: goto st72;
		case 68: goto st95;
	}
	goto st0;
st72:
	if ( ++p == pe )
		goto _test_eof72;
case 72:
	if ( (*p) == 68 )
		goto st73;
	goto st0;
st73:
	if ( ++p == pe )
		goto _test_eof73;
case 73:
	if ( (*p) == 83 )
		goto st74;
	goto st0;
st74:
	if ( ++p == pe )
		goto _test_eof74;
case 74:
	if ( (*p) == 65 )
		goto st75;
	goto st0;
st75:
	if ( ++p == pe )
		goto _test_eof75;
case 75:
	if ( (*p) == 80 )
		goto st76;
	goto st0;
st76:
	if ( ++p == pe )
		goto _test_eof76;
case 76:
	switch( (*p) ) {
		case 50: goto st77;
		case 51: goto st86;
	}
	goto st0;
st77:
	if ( ++p == pe )
		goto _test_eof77;
case 77:
	if ( (*p) == 53 )
		goto st78;
	goto st0;
st78:
	if ( ++p == pe )
		goto _test_eof78;
case 78:
	if ( (*p) == 54 )
		goto st79;
	goto st0;
st79:
	if ( ++p == pe )
		goto _test_eof79;
case 79:
	if ( (*p) == 83 )
		goto st80;
	goto st0;
st80:
	if ( ++p == pe )
		goto _test_eof80;
case 80:
	if ( (*p) == 72 )
		goto st81;
	goto st0;
st81:
	if ( ++p == pe )
		goto _test_eof81;
case 81:
	if ( (*p) == 65 )
		goto st82;
	goto st0;
st82:
	if ( ++p == pe )
		goto _test_eof82;
case 82:
	if ( (*p) == 50 )
		goto st83;
	goto st0;
st83:
	if ( ++p == pe )
		goto _test_eof83;
case 83:
	if ( (*p) == 53 )
		goto st84;
	goto st0;
st84:
	if ( ++p == pe )
		goto _test_eof84;
case 84:
	if ( (*p) == 54 )
		goto st85;
	goto st0;
st85:
	if ( ++p == pe )
		goto _test_eof85;
case 85:
	switch( (*p) ) {
		case 9: goto tr243;
		case 12: goto tr244;
		case 32: goto tr243;
	}
	goto st0;
st86:
	if ( ++p == pe )
		goto _test_eof86;
case 86:
	if ( (*p) == 56 )
		goto st87;
	goto st0;
st87:
	if ( ++p == pe )
		goto _test_eof87;
case 87:
	if ( (*p) == 52 )
		goto st88;
	goto st0;
st88:
	if ( ++p == pe )
		goto _test_eof88;
case 88:
	if ( (*p) == 83 )
		goto st89;
	goto st0;
st89:
	if ( ++p == pe )
		goto _test_eof89;
case 89:
	if ( (*p) == 72 )
		goto st90;
	goto st0;
st90:
	if ( ++p == pe )
		goto _test_eof90;
case 90:
	if ( (*p) == 65 )
		goto st91;
	goto st0;
st91:
	if ( ++p == pe )
		goto _test_eof91;
case 91:
	if ( (*p) == 51 )
		goto st92;
	goto st0;
st92:
	if ( ++p == pe )
		goto _test_eof92;
case 92:
	if ( (*p) == 56 )
		goto st93;
	goto st0;
st93:
	if ( ++p == pe )
		goto _test_eof93;
case 93:
	if ( (*p) == 52 )
		goto st94;
	goto st0;
st94:
	if ( ++p == pe )
		goto _test_eof94;
case 94:
	switch( (*p) ) {
		case 9: goto tr253;
		case 12: goto tr254;
		case 32: goto tr253;
	}
	goto st0;
st95:
	if ( ++p == pe )
		goto _test_eof95;
case 95:
	switch( (*p) ) {
		case 50: goto st96;
		case 52: goto st101;
	}
	goto st0;
st96:
	if ( ++p == pe )
		goto _test_eof96;
case 96:
	if ( (*p) == 53 )
		goto st97;
	goto st0;
st97:
	if ( ++p == pe )
		goto _test_eof97;
case 97:
	if ( (*p) == 53 )
		goto st98;
	goto st0;
st98:
	if ( ++p == pe )
		goto _test_eof98;
case 98:
	if ( (*p) == 49 )
		goto st99;
	goto st0;
st99:
	if ( ++p == pe )
		goto _test_eof99;
case 99:
	if ( (*p) == 57 )
		goto st100;
	goto st0;
st100:
	if ( ++p == pe )
		goto _test_eof100;
case 100:
	switch( (*p) ) {
		case 9: goto tr261;
		case 12: goto tr262;
		case 32: goto tr261;
	}
	goto st0;
st101:
	if ( ++p == pe )
		goto _test_eof101;
case 101:
	if ( (*p) == 52 )
		goto st102;
	goto st0;
st102:
	if ( ++p == pe )
		goto _test_eof102;
case 102:
	if ( (*p) == 56 )
		goto st103;
	goto st0;
st103:
	if ( ++p == pe )
		goto _test_eof103;
case 103:
	switch( (*p) ) {
		case 9: goto tr265;
		case 12: goto tr266;
		case 32: goto tr265;
	}
	goto st0;
tr193:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st104;
st104:
	if ( ++p == pe )
		goto _test_eof104;
case 104:
#line 3375 "src/zscan_rfc1035.c"
	if ( (*p) == 83 )
		goto st105;
	goto st0;
st105:
	if ( ++p == pe )
		goto _test_eof105;
case 105:
	if ( (*p) == 65 )
		goto st106;
	goto st0;
st106:
	if ( ++p == pe )
		goto _test_eof106;
case 106:
	if ( (*p) == 83 )
		goto st107;
	goto st0;
st107:
	if ( ++p == pe )
		goto _test_eof107;
case 107:
	if ( (*p) == 72 )
		goto st108;
	goto st0;
st108:
	if ( ++p == pe )
		goto _test_eof108;
case 108:
	if ( (*p) == 65 )
		goto st109;
	goto st0;
st109:
	if ( ++p == pe )
		goto _test_eof109;
case 109:
	if ( (*p) == 50 )
		goto st110;
	goto st0;
st110:
	if ( ++p == pe )
		goto _test_eof110;
case 110:
	if ( (*p) == 53 )
		goto st111;
	goto st0;
st111:
	if ( ++p == pe )
		goto _test_eof111;
case 111:
	if ( (*p) == 54 )
		goto st112;
	goto st0;
st112:
	if ( ++p == pe )
		goto _test_eof112;
case 112:
	switch( (*p) ) {
		case 9: goto tr275;
		case 12: goto tr276;
		case 32: goto tr275;
	}
	goto st0;
st113:
	if ( ++p == pe )
		goto _test_eof113;
case 113:
	switch( (*p) ) {
		case 78: goto st114;
		case 110: goto st114;
	}
	goto st0;
st114:
	if ( ++p == pe )
		goto _test_eof114;
case 114:
	switch( (*p) ) {
		case 65: goto st115;
		case 67: goto st129;
		case 97: goto st115;
		case 99: goto st129;
	}
	goto st0;
st115:
	if ( ++p == pe )
		goto _test_eof115;
case 115:
	switch( (*p) ) {
		case 9: goto st116;
		case 12: goto st128;
		case 32: goto st116;
	}
	goto st0;
tr307:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st116;
st116:
	if ( ++p == pe )
		goto _test_eof116;
case 116:
#line 3474 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st116;
		case 10: goto st0;
		case 12: goto st128;
		case 32: goto st116;
		case 59: goto st0;
		case 92: goto tr283;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr282;
tr282:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st117;
tr306:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st117;
tr302:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st117;
st117:
	if ( ++p == pe )
		goto _test_eof117;
case 117:
#line 3502 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr285;
		case 10: goto tr286;
		case 12: goto tr287;
		case 32: goto tr285;
		case 33: goto st118;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st124;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st117;
tr303:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st118;
st118:
	if ( ++p == pe )
		goto _test_eof118;
case 118:
#line 3522 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 12: goto st0;
		case 59: goto st0;
		case 92: goto st120;
	}
	if ( (*p) < 32 ) {
		if ( 9 <= (*p) && (*p) <= 10 )
			goto st0;
	} else if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else
		goto st0;
	goto st119;
tr294:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st119;
st119:
	if ( ++p == pe )
		goto _test_eof119;
case 119:
#line 3543 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr285;
		case 10: goto tr286;
		case 12: goto tr287;
		case 32: goto tr285;
		case 59: goto st0;
		case 92: goto st120;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto st119;
tr298:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st120;
st120:
	if ( ++p == pe )
		goto _test_eof120;
case 120:
#line 3564 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st121;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st122;
	goto st119;
st121:
	if ( ++p == pe )
		goto _test_eof121;
case 121:
	switch( (*p) ) {
		case 9: goto tr295;
		case 10: goto tr296;
		case 12: goto tr297;
		case 32: goto tr295;
		case 59: goto st0;
		case 92: goto tr298;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr294;
st122:
	if ( ++p == pe )
		goto _test_eof122;
case 122:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st123;
	goto st0;
st123:
	if ( ++p == pe )
		goto _test_eof123;
case 123:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st119;
	goto st0;
tr283:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st124;
tr309:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st124;
tr304:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st124;
st124:
	if ( ++p == pe )
		goto _test_eof124;
case 124:
#line 3615 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st125;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st126;
	goto st117;
st125:
	if ( ++p == pe )
		goto _test_eof125;
case 125:
	switch( (*p) ) {
		case 9: goto tr295;
		case 10: goto tr296;
		case 12: goto tr297;
		case 32: goto tr295;
		case 33: goto tr303;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr304;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr302;
st126:
	if ( ++p == pe )
		goto _test_eof126;
case 126:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st127;
	goto st0;
st127:
	if ( ++p == pe )
		goto _test_eof127;
case 127:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st117;
	goto st0;
tr308:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st128;
st128:
	if ( ++p == pe )
		goto _test_eof128;
case 128:
#line 3658 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr307;
		case 10: goto st0;
		case 12: goto tr308;
		case 32: goto tr307;
		case 59: goto st0;
		case 92: goto tr309;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr306;
st129:
	if ( ++p == pe )
		goto _test_eof129;
case 129:
	switch( (*p) ) {
		case 9: goto st130;
		case 12: goto st142;
		case 32: goto st130;
	}
	goto st0;
tr337:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st130;
st130:
	if ( ++p == pe )
		goto _test_eof130;
case 130:
#line 3689 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st130;
		case 10: goto st0;
		case 12: goto st142;
		case 32: goto st130;
		case 59: goto st0;
		case 92: goto tr313;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr312;
tr312:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st131;
tr336:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st131;
tr332:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st131;
st131:
	if ( ++p == pe )
		goto _test_eof131;
case 131:
#line 3717 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr315;
		case 10: goto tr316;
		case 12: goto tr317;
		case 32: goto tr315;
		case 33: goto st132;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st138;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st131;
tr333:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st132;
st132:
	if ( ++p == pe )
		goto _test_eof132;
case 132:
#line 3737 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 12: goto st0;
		case 59: goto st0;
		case 92: goto st134;
	}
	if ( (*p) < 32 ) {
		if ( 9 <= (*p) && (*p) <= 10 )
			goto st0;
	} else if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else
		goto st0;
	goto st133;
tr324:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st133;
st133:
	if ( ++p == pe )
		goto _test_eof133;
case 133:
#line 3758 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr315;
		case 10: goto tr316;
		case 12: goto tr317;
		case 32: goto tr315;
		case 59: goto st0;
		case 92: goto st134;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto st133;
tr328:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st134;
st134:
	if ( ++p == pe )
		goto _test_eof134;
case 134:
#line 3779 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st135;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st136;
	goto st133;
st135:
	if ( ++p == pe )
		goto _test_eof135;
case 135:
	switch( (*p) ) {
		case 9: goto tr325;
		case 10: goto tr326;
		case 12: goto tr327;
		case 32: goto tr325;
		case 59: goto st0;
		case 92: goto tr328;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr324;
st136:
	if ( ++p == pe )
		goto _test_eof136;
case 136:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st137;
	goto st0;
st137:
	if ( ++p == pe )
		goto _test_eof137;
case 137:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st133;
	goto st0;
tr313:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st138;
tr339:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st138;
tr334:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st138;
st138:
	if ( ++p == pe )
		goto _test_eof138;
case 138:
#line 3830 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st139;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st140;
	goto st131;
st139:
	if ( ++p == pe )
		goto _test_eof139;
case 139:
	switch( (*p) ) {
		case 9: goto tr325;
		case 10: goto tr326;
		case 12: goto tr327;
		case 32: goto tr325;
		case 33: goto tr333;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr334;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr332;
st140:
	if ( ++p == pe )
		goto _test_eof140;
case 140:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st141;
	goto st0;
st141:
	if ( ++p == pe )
		goto _test_eof141;
case 141:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st131;
	goto st0;
tr338:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st142;
st142:
	if ( ++p == pe )
		goto _test_eof142;
case 142:
#line 3873 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr337;
		case 10: goto st0;
		case 12: goto tr338;
		case 32: goto tr337;
		case 59: goto st0;
		case 92: goto tr339;
	}
	if ( (*p) > 34 ) {
		if ( 40 <= (*p) && (*p) <= 41 )
			goto st0;
	} else if ( (*p) >= 33 )
		goto st0;
	goto tr336;
tr11:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st143;
tr23:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
#line 1083 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; z->ttl_min = z->def_ttl >> 1; z->uv_2 = 0; }
	goto st143;
st143:
	if ( ++p == pe )
		goto _test_eof143;
case 143:
#line 3900 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 78: goto st144;
		case 110: goto st144;
	}
	goto st0;
st144:
	if ( ++p == pe )
		goto _test_eof144;
case 144:
	switch( (*p) ) {
		case 9: goto st145;
		case 12: goto st146;
		case 32: goto st145;
	}
	goto st0;
tr344:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st145;
st145:
	if ( ++p == pe )
		goto _test_eof145;
case 145:
#line 3922 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st145;
		case 12: goto st146;
		case 32: goto st145;
		case 65: goto st7;
		case 67: goto st14;
		case 68: goto st54;
		case 77: goto st150;
		case 78: goto st168;
		case 80: goto st252;
		case 83: goto st268;
		case 84: goto st341;
		case 97: goto st7;
		case 99: goto st14;
		case 100: goto st54;
		case 109: goto st150;
		case 110: goto st168;
		case 112: goto st252;
		case 115: goto st268;
		case 116: goto st341;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr343;
	goto st0;
tr345:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st146;
st146:
	if ( ++p == pe )
		goto _test_eof146;
case 146:
#line 3953 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr344;
		case 12: goto tr345;
		case 32: goto tr344;
		case 65: goto tr47;
		case 67: goto tr48;
		case 68: goto tr49;
		case 77: goto tr51;
		case 78: goto tr52;
		case 80: goto tr53;
		case 83: goto tr54;
		case 84: goto tr55;
		case 97: goto tr47;
		case 99: goto tr48;
		case 100: goto tr49;
		case 109: goto tr51;
		case 110: goto tr52;
		case 112: goto tr53;
		case 115: goto tr54;
		case 116: goto tr55;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr346;
	goto st0;
tr343:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st147;
tr346:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st147;
st147:
	if ( ++p == pe )
		goto _test_eof147;
case 147:
#line 3988 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr347;
		case 12: goto tr348;
		case 32: goto tr347;
		case 47: goto tr349;
		case 68: goto tr351;
		case 72: goto tr351;
		case 77: goto tr351;
		case 87: goto tr351;
		case 100: goto tr351;
		case 104: goto tr351;
		case 109: goto tr351;
		case 119: goto tr351;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st147;
	goto st0;
tr354:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st148;
tr347:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st148;
tr949:
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st148;
st148:
	if ( ++p == pe )
		goto _test_eof148;
case 148:
#line 4023 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st148;
		case 12: goto st149;
		case 32: goto st148;
		case 65: goto st7;
		case 67: goto st14;
		case 68: goto st54;
		case 77: goto st150;
		case 78: goto st168;
		case 80: goto st252;
		case 83: goto st268;
		case 84: goto st341;
		case 97: goto st7;
		case 99: goto st14;
		case 100: goto st54;
		case 109: goto st150;
		case 110: goto st168;
		case 112: goto st252;
		case 115: goto st268;
		case 116: goto st341;
	}
	goto st0;
tr355:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st149;
tr348:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st149;
tr950:
#line 1079 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uval; }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st149;
st149:
	if ( ++p == pe )
		goto _test_eof149;
case 149:
#line 4063 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr354;
		case 12: goto tr355;
		case 32: goto tr354;
		case 65: goto tr47;
		case 67: goto tr48;
		case 68: goto tr49;
		case 77: goto tr51;
		case 78: goto tr52;
		case 80: goto tr53;
		case 83: goto tr54;
		case 84: goto tr55;
		case 97: goto tr47;
		case 99: goto tr48;
		case 100: goto tr49;
		case 109: goto tr51;
		case 110: goto tr52;
		case 112: goto tr53;
		case 115: goto tr54;
		case 116: goto tr55;
	}
	goto st0;
tr12:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st150;
tr51:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st150;
tr24:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st150;
st150:
	if ( ++p == pe )
		goto _test_eof150;
case 150:
#line 4099 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 88: goto st151;
		case 120: goto st151;
	}
	goto st0;
st151:
	if ( ++p == pe )
		goto _test_eof151;
case 151:
	switch( (*p) ) {
		case 9: goto st152;
		case 12: goto st153;
		case 32: goto st152;
	}
	goto st0;
tr360:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st152;
st152:
	if ( ++p == pe )
		goto _test_eof152;
case 152:
#line 4121 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st152;
		case 12: goto st153;
		case 32: goto st152;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr359;
	goto st0;
tr361:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st153;
st153:
	if ( ++p == pe )
		goto _test_eof153;
case 153:
#line 4136 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr360;
		case 12: goto tr361;
		case 32: goto tr360;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr362;
	goto st0;
tr359:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st154;
tr362:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st154;
st154:
	if ( ++p == pe )
		goto _test_eof154;
case 154:
#line 4155 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr363;
		case 12: goto tr364;
		case 32: goto tr363;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st154;
	goto st0;
tr385:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st155;
tr363:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
	goto st155;
st155:
	if ( ++p == pe )
		goto _test_eof155;
case 155:
#line 4173 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st155;
		case 10: goto st0;
		case 12: goto st161;
		case 32: goto st155;
		case 34: goto tr369;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr370;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr366;
tr366:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st156;
tr384:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st156;
tr378:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st156;
st156:
	if ( ++p == pe )
		goto _test_eof156;
case 156:
#line 4200 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr372;
		case 10: goto tr373;
		case 12: goto tr374;
		case 32: goto tr372;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st157;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st156;
tr370:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st157;
tr388:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st157;
tr382:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st157;
st157:
	if ( ++p == pe )
		goto _test_eof157;
case 157:
#line 4226 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st158;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st159;
	goto st156;
st158:
	if ( ++p == pe )
		goto _test_eof158;
case 158:
	switch( (*p) ) {
		case 9: goto tr379;
		case 10: goto tr380;
		case 12: goto tr381;
		case 32: goto tr379;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr382;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr378;
st159:
	if ( ++p == pe )
		goto _test_eof159;
case 159:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st160;
	goto st0;
st160:
	if ( ++p == pe )
		goto _test_eof160;
case 160:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st156;
	goto st0;
tr386:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st161;
tr364:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
	goto st161;
st161:
	if ( ++p == pe )
		goto _test_eof161;
case 161:
#line 4271 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr385;
		case 10: goto st0;
		case 12: goto tr386;
		case 32: goto tr385;
		case 34: goto tr387;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr388;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr384;
tr369:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st162;
tr387:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st162;
tr393:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st162;
st162:
	if ( ++p == pe )
		goto _test_eof162;
case 162:
#line 4298 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st163;
		case 34: goto st164;
		case 92: goto st165;
	}
	goto st162;
tr394:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st163;
st163:
	if ( ++p == pe )
		goto _test_eof163;
case 163:
#line 4311 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr394;
		case 34: goto tr395;
		case 92: goto tr396;
	}
	goto tr393;
tr395:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st164;
st164:
	if ( ++p == pe )
		goto _test_eof164;
case 164:
#line 4324 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr397;
		case 10: goto tr398;
		case 12: goto tr399;
		case 32: goto tr397;
	}
	goto st0;
tr396:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st165;
st165:
	if ( ++p == pe )
		goto _test_eof165;
case 165:
#line 4338 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st163;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st166;
	goto st162;
st166:
	if ( ++p == pe )
		goto _test_eof166;
case 166:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st167;
	goto st0;
st167:
	if ( ++p == pe )
		goto _test_eof167;
case 167:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st162;
	goto st0;
tr13:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st168;
tr52:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st168;
tr25:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st168;
st168:
	if ( ++p == pe )
		goto _test_eof168;
case 168:
#line 4371 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 65: goto st169;
		case 83: goto st231;
		case 88: goto st245;
		case 97: goto st169;
		case 115: goto st231;
		case 120: goto st245;
	}
	goto st0;
st169:
	if ( ++p == pe )
		goto _test_eof169;
case 169:
	switch( (*p) ) {
		case 80: goto st170;
		case 112: goto st170;
	}
	goto st0;
st170:
	if ( ++p == pe )
		goto _test_eof170;
case 170:
	switch( (*p) ) {
		case 84: goto st171;
		case 116: goto st171;
	}
	goto st0;
st171:
	if ( ++p == pe )
		goto _test_eof171;
case 171:
	switch( (*p) ) {
		case 82: goto st172;
		case 114: goto st172;
	}
	goto st0;
st172:
	if ( ++p == pe )
		goto _test_eof172;
case 172:
	switch( (*p) ) {
		case 9: goto st173;
		case 12: goto st174;
		case 32: goto st173;
	}
	goto st0;
tr411:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st173;
st173:
	if ( ++p == pe )
		goto _test_eof173;
case 173:
#line 4424 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st173;
		case 12: goto st174;
		case 32: goto st173;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr410;
	goto st0;
tr412:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st174;
st174:
	if ( ++p == pe )
		goto _test_eof174;
case 174:
#line 4439 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr411;
		case 12: goto tr412;
		case 32: goto tr411;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr413;
	goto st0;
tr410:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st175;
tr413:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st175;
st175:
	if ( ++p == pe )
		goto _test_eof175;
case 175:
#line 4458 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr414;
		case 12: goto tr415;
		case 32: goto tr414;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st175;
	goto st0;
tr420:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st176;
tr414:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st176;
st176:
	if ( ++p == pe )
		goto _test_eof176;
case 176:
#line 4477 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st176;
		case 12: goto st177;
		case 32: goto st176;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr419;
	goto st0;
tr421:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st177;
tr415:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st177;
st177:
	if ( ++p == pe )
		goto _test_eof177;
case 177:
#line 4496 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr420;
		case 12: goto tr421;
		case 32: goto tr420;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr422;
	goto st0;
tr419:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st178;
tr422:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st178;
st178:
	if ( ++p == pe )
		goto _test_eof178;
case 178:
#line 4515 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr423;
		case 12: goto tr424;
		case 32: goto tr423;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st178;
	goto st0;
tr545:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st179;
tr423:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st179;
st179:
	if ( ++p == pe )
		goto _test_eof179;
case 179:
#line 4534 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st179;
		case 10: goto st0;
		case 12: goto st224;
		case 32: goto st179;
		case 34: goto tr429;
		case 59: goto st0;
		case 92: goto tr430;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr426;
tr426:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st180;
tr539:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st180;
tr544:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st180;
st180:
	if ( ++p == pe )
		goto _test_eof180;
case 180:
#line 4562 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr432;
		case 10: goto st0;
		case 12: goto tr433;
		case 32: goto tr432;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st220;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st180;
tr521:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st181;
tr432:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st181;
tr540:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st181;
tr557:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st181;
st181:
	if ( ++p == pe )
		goto _test_eof181;
case 181:
#line 4591 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st181;
		case 10: goto st0;
		case 12: goto st213;
		case 32: goto st181;
		case 34: goto tr438;
		case 59: goto st0;
		case 92: goto tr439;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr435;
tr435:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st182;
tr520:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st182;
tr515:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st182;
st182:
	if ( ++p == pe )
		goto _test_eof182;
case 182:
#line 4617 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr441;
		case 10: goto st0;
		case 12: goto tr442;
		case 32: goto tr441;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st209;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st182;
tr497:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st183;
tr441:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st183;
tr516:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st183;
tr533:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st183;
st183:
	if ( ++p == pe )
		goto _test_eof183;
case 183:
#line 4646 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st183;
		case 10: goto st0;
		case 12: goto st202;
		case 32: goto st183;
		case 34: goto tr447;
		case 59: goto st0;
		case 92: goto tr448;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr444;
tr444:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st184;
tr496:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st184;
tr491:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st184;
st184:
	if ( ++p == pe )
		goto _test_eof184;
case 184:
#line 4672 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr450;
		case 10: goto st0;
		case 12: goto tr451;
		case 32: goto tr450;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st198;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st184;
tr472:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st185;
tr450:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st185;
tr492:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st185;
tr509:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st185;
st185:
	if ( ++p == pe )
		goto _test_eof185;
case 185:
#line 4701 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st185;
		case 10: goto st0;
		case 12: goto st191;
		case 32: goto st185;
		case 34: goto tr456;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr457;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr453;
tr453:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st186;
tr471:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st186;
tr465:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st186;
st186:
	if ( ++p == pe )
		goto _test_eof186;
case 186:
#line 4728 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr459;
		case 10: goto tr460;
		case 12: goto tr461;
		case 32: goto tr459;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st187;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st186;
tr457:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st187;
tr475:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st187;
tr469:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st187;
st187:
	if ( ++p == pe )
		goto _test_eof187;
case 187:
#line 4754 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st188;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st189;
	goto st186;
st188:
	if ( ++p == pe )
		goto _test_eof188;
case 188:
	switch( (*p) ) {
		case 9: goto tr466;
		case 10: goto tr467;
		case 12: goto tr468;
		case 32: goto tr466;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr469;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr465;
st189:
	if ( ++p == pe )
		goto _test_eof189;
case 189:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st190;
	goto st0;
st190:
	if ( ++p == pe )
		goto _test_eof190;
case 190:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st186;
	goto st0;
tr473:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st191;
tr451:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st191;
tr493:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st191;
tr510:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st191;
st191:
	if ( ++p == pe )
		goto _test_eof191;
case 191:
#line 4806 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr472;
		case 10: goto st0;
		case 12: goto tr473;
		case 32: goto tr472;
		case 34: goto tr474;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr475;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr471;
tr456:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st192;
tr474:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st192;
tr480:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st192;
st192:
	if ( ++p == pe )
		goto _test_eof192;
case 192:
#line 4833 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st193;
		case 34: goto st194;
		case 92: goto st195;
	}
	goto st192;
tr481:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st193;
st193:
	if ( ++p == pe )
		goto _test_eof193;
case 193:
#line 4846 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr481;
		case 34: goto tr482;
		case 92: goto tr483;
	}
	goto tr480;
tr482:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st194;
st194:
	if ( ++p == pe )
		goto _test_eof194;
case 194:
#line 4859 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr484;
		case 10: goto tr485;
		case 12: goto tr486;
		case 32: goto tr484;
	}
	goto st0;
tr483:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st195;
st195:
	if ( ++p == pe )
		goto _test_eof195;
case 195:
#line 4873 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st193;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st196;
	goto st192;
st196:
	if ( ++p == pe )
		goto _test_eof196;
case 196:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st197;
	goto st0;
st197:
	if ( ++p == pe )
		goto _test_eof197;
case 197:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st192;
	goto st0;
tr448:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st198;
tr500:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st198;
tr494:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st198;
st198:
	if ( ++p == pe )
		goto _test_eof198;
case 198:
#line 4906 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st199;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st200;
	goto st184;
st199:
	if ( ++p == pe )
		goto _test_eof199;
case 199:
	switch( (*p) ) {
		case 9: goto tr492;
		case 10: goto st0;
		case 12: goto tr493;
		case 32: goto tr492;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr494;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr491;
st200:
	if ( ++p == pe )
		goto _test_eof200;
case 200:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st201;
	goto st0;
st201:
	if ( ++p == pe )
		goto _test_eof201;
case 201:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st184;
	goto st0;
tr498:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st202;
tr442:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st202;
tr517:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st202;
tr534:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st202;
st202:
	if ( ++p == pe )
		goto _test_eof202;
case 202:
#line 4958 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr497;
		case 10: goto st0;
		case 12: goto tr498;
		case 32: goto tr497;
		case 34: goto tr499;
		case 59: goto st0;
		case 92: goto tr500;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr496;
tr447:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st203;
tr499:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st203;
tr505:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st203;
st203:
	if ( ++p == pe )
		goto _test_eof203;
case 203:
#line 4984 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st204;
		case 34: goto st205;
		case 92: goto st206;
	}
	goto st203;
tr506:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st204;
st204:
	if ( ++p == pe )
		goto _test_eof204;
case 204:
#line 4997 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr506;
		case 34: goto tr507;
		case 92: goto tr508;
	}
	goto tr505;
tr507:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st205;
st205:
	if ( ++p == pe )
		goto _test_eof205;
case 205:
#line 5010 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr509;
		case 12: goto tr510;
		case 32: goto tr509;
	}
	goto st0;
tr508:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st206;
st206:
	if ( ++p == pe )
		goto _test_eof206;
case 206:
#line 5023 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st204;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st207;
	goto st203;
st207:
	if ( ++p == pe )
		goto _test_eof207;
case 207:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st208;
	goto st0;
st208:
	if ( ++p == pe )
		goto _test_eof208;
case 208:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st203;
	goto st0;
tr439:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st209;
tr524:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st209;
tr518:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st209;
st209:
	if ( ++p == pe )
		goto _test_eof209;
case 209:
#line 5056 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st210;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st211;
	goto st182;
st210:
	if ( ++p == pe )
		goto _test_eof210;
case 210:
	switch( (*p) ) {
		case 9: goto tr516;
		case 10: goto st0;
		case 12: goto tr517;
		case 32: goto tr516;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr518;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr515;
st211:
	if ( ++p == pe )
		goto _test_eof211;
case 211:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st212;
	goto st0;
st212:
	if ( ++p == pe )
		goto _test_eof212;
case 212:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st182;
	goto st0;
tr522:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st213;
tr433:
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st213;
tr541:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1067 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, false); }
	goto st213;
tr558:
#line 1068 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, false); }
	goto st213;
st213:
	if ( ++p == pe )
		goto _test_eof213;
case 213:
#line 5108 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr521;
		case 10: goto st0;
		case 12: goto tr522;
		case 32: goto tr521;
		case 34: goto tr523;
		case 59: goto st0;
		case 92: goto tr524;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr520;
tr438:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st214;
tr523:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st214;
tr529:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st214;
st214:
	if ( ++p == pe )
		goto _test_eof214;
case 214:
#line 5134 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st215;
		case 34: goto st216;
		case 92: goto st217;
	}
	goto st214;
tr530:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st215;
st215:
	if ( ++p == pe )
		goto _test_eof215;
case 215:
#line 5147 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr530;
		case 34: goto tr531;
		case 92: goto tr532;
	}
	goto tr529;
tr531:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st216;
st216:
	if ( ++p == pe )
		goto _test_eof216;
case 216:
#line 5160 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr533;
		case 12: goto tr534;
		case 32: goto tr533;
	}
	goto st0;
tr532:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st217;
st217:
	if ( ++p == pe )
		goto _test_eof217;
case 217:
#line 5173 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st215;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st218;
	goto st214;
st218:
	if ( ++p == pe )
		goto _test_eof218;
case 218:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st219;
	goto st0;
st219:
	if ( ++p == pe )
		goto _test_eof219;
case 219:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st214;
	goto st0;
tr430:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st220;
tr542:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st220;
tr548:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st220;
st220:
	if ( ++p == pe )
		goto _test_eof220;
case 220:
#line 5208 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st221;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st222;
	goto st180;
st221:
	if ( ++p == pe )
		goto _test_eof221;
case 221:
	switch( (*p) ) {
		case 9: goto tr540;
		case 10: goto st0;
		case 12: goto tr541;
		case 32: goto tr540;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr542;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr539;
st222:
	if ( ++p == pe )
		goto _test_eof222;
case 222:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st223;
	goto st0;
st223:
	if ( ++p == pe )
		goto _test_eof223;
case 223:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st180;
	goto st0;
tr546:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st224;
tr424:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st224;
st224:
	if ( ++p == pe )
		goto _test_eof224;
case 224:
#line 5254 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr545;
		case 10: goto st0;
		case 12: goto tr546;
		case 32: goto tr545;
		case 34: goto tr547;
		case 59: goto st0;
		case 92: goto tr548;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr544;
tr429:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st225;
tr553:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st225;
tr547:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st225;
st225:
	if ( ++p == pe )
		goto _test_eof225;
case 225:
#line 5282 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st226;
		case 34: goto st227;
		case 92: goto st228;
	}
	goto st225;
tr554:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st226;
st226:
	if ( ++p == pe )
		goto _test_eof226;
case 226:
#line 5295 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr554;
		case 34: goto tr555;
		case 92: goto tr556;
	}
	goto tr553;
tr555:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st227;
st227:
	if ( ++p == pe )
		goto _test_eof227;
case 227:
#line 5308 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr557;
		case 12: goto tr558;
		case 32: goto tr557;
	}
	goto st0;
tr556:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st228;
st228:
	if ( ++p == pe )
		goto _test_eof228;
case 228:
#line 5321 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st226;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st229;
	goto st225;
st229:
	if ( ++p == pe )
		goto _test_eof229;
case 229:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st230;
	goto st0;
st230:
	if ( ++p == pe )
		goto _test_eof230;
case 230:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st225;
	goto st0;
st231:
	if ( ++p == pe )
		goto _test_eof231;
case 231:
	switch( (*p) ) {
		case 9: goto st232;
		case 12: goto st238;
		case 32: goto st232;
	}
	goto st0;
tr580:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st232;
st232:
	if ( ++p == pe )
		goto _test_eof232;
case 232:
#line 5357 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st232;
		case 10: goto st0;
		case 12: goto st238;
		case 32: goto st232;
		case 34: goto tr564;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr565;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr563;
tr563:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st233;
tr579:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st233;
tr573:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st233;
st233:
	if ( ++p == pe )
		goto _test_eof233;
case 233:
#line 5384 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr567;
		case 10: goto tr568;
		case 12: goto tr569;
		case 32: goto tr567;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st234;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st233;
tr565:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st234;
tr583:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st234;
tr577:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st234;
st234:
	if ( ++p == pe )
		goto _test_eof234;
case 234:
#line 5410 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st235;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st236;
	goto st233;
st235:
	if ( ++p == pe )
		goto _test_eof235;
case 235:
	switch( (*p) ) {
		case 9: goto tr574;
		case 10: goto tr575;
		case 12: goto tr576;
		case 32: goto tr574;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr577;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr573;
st236:
	if ( ++p == pe )
		goto _test_eof236;
case 236:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st237;
	goto st0;
st237:
	if ( ++p == pe )
		goto _test_eof237;
case 237:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st233;
	goto st0;
tr581:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st238;
st238:
	if ( ++p == pe )
		goto _test_eof238;
case 238:
#line 5452 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr580;
		case 10: goto st0;
		case 12: goto tr581;
		case 32: goto tr580;
		case 34: goto tr582;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr583;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr579;
tr564:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st239;
tr582:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st239;
tr588:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st239;
st239:
	if ( ++p == pe )
		goto _test_eof239;
case 239:
#line 5479 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st240;
		case 34: goto st241;
		case 92: goto st242;
	}
	goto st239;
tr589:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st240;
st240:
	if ( ++p == pe )
		goto _test_eof240;
case 240:
#line 5492 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr589;
		case 34: goto tr590;
		case 92: goto tr591;
	}
	goto tr588;
tr590:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st241;
st241:
	if ( ++p == pe )
		goto _test_eof241;
case 241:
#line 5505 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr592;
		case 10: goto tr593;
		case 12: goto tr594;
		case 32: goto tr592;
	}
	goto st0;
tr591:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st242;
st242:
	if ( ++p == pe )
		goto _test_eof242;
case 242:
#line 5519 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st240;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st243;
	goto st239;
st243:
	if ( ++p == pe )
		goto _test_eof243;
case 243:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st244;
	goto st0;
st244:
	if ( ++p == pe )
		goto _test_eof244;
case 244:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st239;
	goto st0;
st245:
	if ( ++p == pe )
		goto _test_eof245;
case 245:
	switch( (*p) ) {
		case 68: goto st246;
		case 100: goto st246;
	}
	goto st0;
st246:
	if ( ++p == pe )
		goto _test_eof246;
case 246:
	switch( (*p) ) {
		case 79: goto st247;
		case 111: goto st247;
	}
	goto st0;
st247:
	if ( ++p == pe )
		goto _test_eof247;
case 247:
	switch( (*p) ) {
		case 77: goto st248;
		case 109: goto st248;
	}
	goto st0;
st248:
	if ( ++p == pe )
		goto _test_eof248;
case 248:
	switch( (*p) ) {
		case 65: goto st249;
		case 97: goto st249;
	}
	goto st0;
st249:
	if ( ++p == pe )
		goto _test_eof249;
case 249:
	switch( (*p) ) {
		case 73: goto st250;
		case 105: goto st250;
	}
	goto st0;
st250:
	if ( ++p == pe )
		goto _test_eof250;
case 250:
	switch( (*p) ) {
		case 78: goto st251;
		case 110: goto st251;
	}
	goto st0;
st251:
	if ( ++p == pe )
		goto _test_eof251;
case 251:
	switch( (*p) ) {
		case 9: goto tr603;
		case 10: goto tr604;
		case 12: goto tr605;
		case 32: goto tr603;
	}
	goto st0;
tr14:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st252;
tr53:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st252;
tr26:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st252;
st252:
	if ( ++p == pe )
		goto _test_eof252;
case 252:
#line 5617 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 84: goto st253;
		case 116: goto st253;
	}
	goto st0;
st253:
	if ( ++p == pe )
		goto _test_eof253;
case 253:
	switch( (*p) ) {
		case 82: goto st254;
		case 114: goto st254;
	}
	goto st0;
st254:
	if ( ++p == pe )
		goto _test_eof254;
case 254:
	switch( (*p) ) {
		case 9: goto st255;
		case 12: goto st261;
		case 32: goto st255;
	}
	goto st0;
tr627:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st255;
st255:
	if ( ++p == pe )
		goto _test_eof255;
case 255:
#line 5648 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st255;
		case 10: goto st0;
		case 12: goto st261;
		case 32: goto st255;
		case 34: goto tr611;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr612;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr610;
tr610:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st256;
tr626:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st256;
tr620:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st256;
st256:
	if ( ++p == pe )
		goto _test_eof256;
case 256:
#line 5675 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr614;
		case 10: goto tr615;
		case 12: goto tr616;
		case 32: goto tr614;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st257;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st256;
tr612:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st257;
tr630:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st257;
tr624:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st257;
st257:
	if ( ++p == pe )
		goto _test_eof257;
case 257:
#line 5701 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st258;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st259;
	goto st256;
st258:
	if ( ++p == pe )
		goto _test_eof258;
case 258:
	switch( (*p) ) {
		case 9: goto tr621;
		case 10: goto tr622;
		case 12: goto tr623;
		case 32: goto tr621;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr624;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr620;
st259:
	if ( ++p == pe )
		goto _test_eof259;
case 259:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st260;
	goto st0;
st260:
	if ( ++p == pe )
		goto _test_eof260;
case 260:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st256;
	goto st0;
tr628:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st261;
st261:
	if ( ++p == pe )
		goto _test_eof261;
case 261:
#line 5743 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr627;
		case 10: goto st0;
		case 12: goto tr628;
		case 32: goto tr627;
		case 34: goto tr629;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr630;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr626;
tr611:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st262;
tr629:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st262;
tr635:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st262;
st262:
	if ( ++p == pe )
		goto _test_eof262;
case 262:
#line 5770 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st263;
		case 34: goto st264;
		case 92: goto st265;
	}
	goto st262;
tr636:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st263;
st263:
	if ( ++p == pe )
		goto _test_eof263;
case 263:
#line 5783 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr636;
		case 34: goto tr637;
		case 92: goto tr638;
	}
	goto tr635;
tr637:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st264;
st264:
	if ( ++p == pe )
		goto _test_eof264;
case 264:
#line 5796 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr639;
		case 10: goto tr640;
		case 12: goto tr641;
		case 32: goto tr639;
	}
	goto st0;
tr638:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st265;
st265:
	if ( ++p == pe )
		goto _test_eof265;
case 265:
#line 5810 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st263;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st266;
	goto st262;
st266:
	if ( ++p == pe )
		goto _test_eof266;
case 266:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st267;
	goto st0;
st267:
	if ( ++p == pe )
		goto _test_eof267;
case 267:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st262;
	goto st0;
tr15:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st268;
tr54:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st268;
tr27:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st268;
st268:
	if ( ++p == pe )
		goto _test_eof268;
case 268:
#line 5843 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 79: goto st269;
		case 82: goto st317;
		case 111: goto st269;
		case 114: goto st317;
	}
	goto st0;
st269:
	if ( ++p == pe )
		goto _test_eof269;
case 269:
	switch( (*p) ) {
		case 65: goto st270;
		case 97: goto st270;
	}
	goto st0;
st270:
	if ( ++p == pe )
		goto _test_eof270;
case 270:
	switch( (*p) ) {
		case 9: goto st271;
		case 12: goto st310;
		case 32: goto st271;
	}
	goto st0;
tr759:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st271;
st271:
	if ( ++p == pe )
		goto _test_eof271;
case 271:
#line 5876 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st271;
		case 10: goto st0;
		case 12: goto st310;
		case 32: goto st271;
		case 34: goto tr650;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr651;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr649;
tr649:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st272;
tr758:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st272;
tr753:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st272;
st272:
	if ( ++p == pe )
		goto _test_eof272;
case 272:
#line 5903 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr653;
		case 10: goto st0;
		case 12: goto tr654;
		case 32: goto tr653;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st306;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st272;
tr735:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st273;
tr653:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
	goto st273;
tr754:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
	goto st273;
tr771:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
	goto st273;
st273:
	if ( ++p == pe )
		goto _test_eof273;
case 273:
#line 5932 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st273;
		case 10: goto st0;
		case 12: goto st299;
		case 32: goto st273;
		case 34: goto tr659;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr660;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr656;
tr656:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st274;
tr734:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st274;
tr729:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st274;
st274:
	if ( ++p == pe )
		goto _test_eof274;
case 274:
#line 5959 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr662;
		case 10: goto st0;
		case 12: goto tr663;
		case 32: goto tr662;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st295;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st274;
tr668:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st275;
tr662:
#line 1050 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->eml_dname, p - z->tstart, false); }
	goto st275;
tr730:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1050 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->eml_dname, p - z->tstart, false); }
	goto st275;
tr747:
#line 1051 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->eml_dname, p - z->tstart - 1, false); }
	goto st275;
st275:
	if ( ++p == pe )
		goto _test_eof275;
case 275:
#line 5988 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st275;
		case 12: goto st276;
		case 32: goto st275;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr667;
	goto st0;
tr669:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st276;
tr663:
#line 1050 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->eml_dname, p - z->tstart, false); }
	goto st276;
tr731:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1050 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->eml_dname, p - z->tstart, false); }
	goto st276;
tr748:
#line 1051 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->eml_dname, p - z->tstart - 1, false); }
	goto st276;
st276:
	if ( ++p == pe )
		goto _test_eof276;
case 276:
#line 6013 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr668;
		case 12: goto tr669;
		case 32: goto tr668;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr670;
	goto st0;
tr667:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st277;
tr670:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st277;
st277:
	if ( ++p == pe )
		goto _test_eof277;
case 277:
#line 6032 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr671;
		case 12: goto tr672;
		case 32: goto tr671;
		case 68: goto tr674;
		case 72: goto tr674;
		case 77: goto tr674;
		case 87: goto tr674;
		case 100: goto tr674;
		case 104: goto tr674;
		case 109: goto tr674;
		case 119: goto tr674;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st277;
	goto st0;
tr678:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st278;
tr671:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st278;
tr725:
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st278;
st278:
	if ( ++p == pe )
		goto _test_eof278;
case 278:
#line 6062 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st278;
		case 12: goto st279;
		case 32: goto st278;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr677;
	goto st0;
tr679:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st279;
tr672:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st279;
tr726:
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st279;
st279:
	if ( ++p == pe )
		goto _test_eof279;
case 279:
#line 6084 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr678;
		case 12: goto tr679;
		case 32: goto tr678;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr680;
	goto st0;
tr677:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st280;
tr680:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st280;
st280:
	if ( ++p == pe )
		goto _test_eof280;
case 280:
#line 6103 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr681;
		case 12: goto tr682;
		case 32: goto tr681;
		case 68: goto tr684;
		case 72: goto tr684;
		case 77: goto tr684;
		case 87: goto tr684;
		case 100: goto tr684;
		case 104: goto tr684;
		case 109: goto tr684;
		case 119: goto tr684;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st280;
	goto st0;
tr688:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st281;
tr681:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st281;
tr723:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st281;
st281:
	if ( ++p == pe )
		goto _test_eof281;
case 281:
#line 6133 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st281;
		case 12: goto st282;
		case 32: goto st281;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr687;
	goto st0;
tr689:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st282;
tr682:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st282;
tr724:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st282;
st282:
	if ( ++p == pe )
		goto _test_eof282;
case 282:
#line 6155 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr688;
		case 12: goto tr689;
		case 32: goto tr688;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr690;
	goto st0;
tr687:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st283;
tr690:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st283;
st283:
	if ( ++p == pe )
		goto _test_eof283;
case 283:
#line 6174 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr691;
		case 12: goto tr692;
		case 32: goto tr691;
		case 68: goto tr694;
		case 72: goto tr694;
		case 77: goto tr694;
		case 87: goto tr694;
		case 100: goto tr694;
		case 104: goto tr694;
		case 109: goto tr694;
		case 119: goto tr694;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st283;
	goto st0;
tr698:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st284;
tr691:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st284;
tr721:
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st284;
st284:
	if ( ++p == pe )
		goto _test_eof284;
case 284:
#line 6204 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st284;
		case 12: goto st285;
		case 32: goto st284;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr697;
	goto st0;
tr699:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st285;
tr692:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st285;
tr722:
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st285;
st285:
	if ( ++p == pe )
		goto _test_eof285;
case 285:
#line 6226 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr698;
		case 12: goto tr699;
		case 32: goto tr698;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr700;
	goto st0;
tr697:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st286;
tr700:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st286;
st286:
	if ( ++p == pe )
		goto _test_eof286;
case 286:
#line 6245 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr701;
		case 12: goto tr702;
		case 32: goto tr701;
		case 68: goto tr704;
		case 72: goto tr704;
		case 77: goto tr704;
		case 87: goto tr704;
		case 100: goto tr704;
		case 104: goto tr704;
		case 109: goto tr704;
		case 119: goto tr704;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st286;
	goto st0;
tr708:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st287;
tr701:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1087 "./src/zscan_rfc1035.rl"
	{ z->uv_4 = z->uval; }
	goto st287;
tr719:
#line 1087 "./src/zscan_rfc1035.rl"
	{ z->uv_4 = z->uval; }
	goto st287;
st287:
	if ( ++p == pe )
		goto _test_eof287;
case 287:
#line 6275 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st287;
		case 12: goto st288;
		case 32: goto st287;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr707;
	goto st0;
tr709:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st288;
tr702:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1087 "./src/zscan_rfc1035.rl"
	{ z->uv_4 = z->uval; }
	goto st288;
tr720:
#line 1087 "./src/zscan_rfc1035.rl"
	{ z->uv_4 = z->uval; }
	goto st288;
st288:
	if ( ++p == pe )
		goto _test_eof288;
case 288:
#line 6297 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr708;
		case 12: goto tr709;
		case 32: goto tr708;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr710;
	goto st0;
tr707:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st289;
tr710:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st289;
st289:
	if ( ++p == pe )
		goto _test_eof289;
case 289:
#line 6316 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr711;
		case 10: goto tr712;
		case 12: goto tr713;
		case 32: goto tr711;
		case 68: goto tr715;
		case 72: goto tr715;
		case 77: goto tr715;
		case 87: goto tr715;
		case 100: goto tr715;
		case 104: goto tr715;
		case 109: goto tr715;
		case 119: goto tr715;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st289;
	goto st0;
tr715:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st290;
st290:
	if ( ++p == pe )
		goto _test_eof290;
case 290:
#line 6341 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr716;
		case 10: goto tr717;
		case 12: goto tr718;
		case 32: goto tr716;
	}
	goto st0;
tr704:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st291;
st291:
	if ( ++p == pe )
		goto _test_eof291;
case 291:
#line 6356 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr719;
		case 12: goto tr720;
		case 32: goto tr719;
	}
	goto st0;
tr694:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st292;
st292:
	if ( ++p == pe )
		goto _test_eof292;
case 292:
#line 6370 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr721;
		case 12: goto tr722;
		case 32: goto tr721;
	}
	goto st0;
tr684:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st293;
st293:
	if ( ++p == pe )
		goto _test_eof293;
case 293:
#line 6384 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr723;
		case 12: goto tr724;
		case 32: goto tr723;
	}
	goto st0;
tr674:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st294;
st294:
	if ( ++p == pe )
		goto _test_eof294;
case 294:
#line 6398 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr725;
		case 12: goto tr726;
		case 32: goto tr725;
	}
	goto st0;
tr660:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st295;
tr738:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st295;
tr732:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st295;
st295:
	if ( ++p == pe )
		goto _test_eof295;
case 295:
#line 6418 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st296;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st297;
	goto st274;
st296:
	if ( ++p == pe )
		goto _test_eof296;
case 296:
	switch( (*p) ) {
		case 9: goto tr730;
		case 10: goto st0;
		case 12: goto tr731;
		case 32: goto tr730;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr732;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr729;
st297:
	if ( ++p == pe )
		goto _test_eof297;
case 297:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st298;
	goto st0;
st298:
	if ( ++p == pe )
		goto _test_eof298;
case 298:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st274;
	goto st0;
tr736:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st299;
tr654:
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
	goto st299;
tr755:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1048 "./src/zscan_rfc1035.rl"
	{ dname_set(z, z->rhs_dname, p - z->tstart, false); }
	goto st299;
tr772:
#line 1049 "./src/zscan_rfc1035.rl"
	{ z->tstart++; dname_set(z, z->rhs_dname, p - z->tstart - 1, false); }
	goto st299;
st299:
	if ( ++p == pe )
		goto _test_eof299;
case 299:
#line 6470 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr735;
		case 10: goto st0;
		case 12: goto tr736;
		case 32: goto tr735;
		case 34: goto tr737;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr738;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr734;
tr659:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st300;
tr737:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st300;
tr743:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st300;
st300:
	if ( ++p == pe )
		goto _test_eof300;
case 300:
#line 6497 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st301;
		case 34: goto st302;
		case 92: goto st303;
	}
	goto st300;
tr744:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st301;
st301:
	if ( ++p == pe )
		goto _test_eof301;
case 301:
#line 6510 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr744;
		case 34: goto tr745;
		case 92: goto tr746;
	}
	goto tr743;
tr745:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st302;
st302:
	if ( ++p == pe )
		goto _test_eof302;
case 302:
#line 6523 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr747;
		case 12: goto tr748;
		case 32: goto tr747;
	}
	goto st0;
tr746:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st303;
st303:
	if ( ++p == pe )
		goto _test_eof303;
case 303:
#line 6536 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st301;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st304;
	goto st300;
st304:
	if ( ++p == pe )
		goto _test_eof304;
case 304:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st305;
	goto st0;
st305:
	if ( ++p == pe )
		goto _test_eof305;
case 305:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st300;
	goto st0;
tr651:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st306;
tr762:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st306;
tr756:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st306;
st306:
	if ( ++p == pe )
		goto _test_eof306;
case 306:
#line 6569 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st307;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st308;
	goto st272;
st307:
	if ( ++p == pe )
		goto _test_eof307;
case 307:
	switch( (*p) ) {
		case 9: goto tr754;
		case 10: goto st0;
		case 12: goto tr755;
		case 32: goto tr754;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr756;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr753;
st308:
	if ( ++p == pe )
		goto _test_eof308;
case 308:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st309;
	goto st0;
st309:
	if ( ++p == pe )
		goto _test_eof309;
case 309:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st272;
	goto st0;
tr760:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st310;
st310:
	if ( ++p == pe )
		goto _test_eof310;
case 310:
#line 6611 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr759;
		case 10: goto st0;
		case 12: goto tr760;
		case 32: goto tr759;
		case 34: goto tr761;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr762;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr758;
tr650:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st311;
tr761:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st311;
tr767:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st311;
st311:
	if ( ++p == pe )
		goto _test_eof311;
case 311:
#line 6638 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st312;
		case 34: goto st313;
		case 92: goto st314;
	}
	goto st311;
tr768:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st312;
st312:
	if ( ++p == pe )
		goto _test_eof312;
case 312:
#line 6651 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr768;
		case 34: goto tr769;
		case 92: goto tr770;
	}
	goto tr767;
tr769:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st313;
st313:
	if ( ++p == pe )
		goto _test_eof313;
case 313:
#line 6664 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr771;
		case 12: goto tr772;
		case 32: goto tr771;
	}
	goto st0;
tr770:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st314;
st314:
	if ( ++p == pe )
		goto _test_eof314;
case 314:
#line 6677 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st312;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st315;
	goto st311;
st315:
	if ( ++p == pe )
		goto _test_eof315;
case 315:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st316;
	goto st0;
st316:
	if ( ++p == pe )
		goto _test_eof316;
case 316:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st311;
	goto st0;
st317:
	if ( ++p == pe )
		goto _test_eof317;
case 317:
	switch( (*p) ) {
		case 86: goto st318;
		case 118: goto st318;
	}
	goto st0;
st318:
	if ( ++p == pe )
		goto _test_eof318;
case 318:
	switch( (*p) ) {
		case 9: goto st319;
		case 12: goto st320;
		case 32: goto st319;
	}
	goto st0;
tr779:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st319;
st319:
	if ( ++p == pe )
		goto _test_eof319;
case 319:
#line 6722 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st319;
		case 12: goto st320;
		case 32: goto st319;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr778;
	goto st0;
tr780:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st320;
st320:
	if ( ++p == pe )
		goto _test_eof320;
case 320:
#line 6737 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr779;
		case 12: goto tr780;
		case 32: goto tr779;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr781;
	goto st0;
tr778:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st321;
tr781:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st321;
st321:
	if ( ++p == pe )
		goto _test_eof321;
case 321:
#line 6756 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr782;
		case 12: goto tr783;
		case 32: goto tr782;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st321;
	goto st0;
tr788:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st322;
tr782:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st322;
st322:
	if ( ++p == pe )
		goto _test_eof322;
case 322:
#line 6775 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st322;
		case 12: goto st323;
		case 32: goto st322;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr787;
	goto st0;
tr789:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st323;
tr783:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st323;
st323:
	if ( ++p == pe )
		goto _test_eof323;
case 323:
#line 6794 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr788;
		case 12: goto tr789;
		case 32: goto tr788;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr790;
	goto st0;
tr787:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st324;
tr790:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st324;
st324:
	if ( ++p == pe )
		goto _test_eof324;
case 324:
#line 6813 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr791;
		case 12: goto tr792;
		case 32: goto tr791;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st324;
	goto st0;
tr797:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st325;
tr791:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st325;
st325:
	if ( ++p == pe )
		goto _test_eof325;
case 325:
#line 6832 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st325;
		case 12: goto st326;
		case 32: goto st325;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr796;
	goto st0;
tr798:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st326;
tr792:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
	goto st326;
st326:
	if ( ++p == pe )
		goto _test_eof326;
case 326:
#line 6851 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr797;
		case 12: goto tr798;
		case 32: goto tr797;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr799;
	goto st0;
tr796:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st327;
tr799:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st327;
st327:
	if ( ++p == pe )
		goto _test_eof327;
case 327:
#line 6870 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr800;
		case 12: goto tr801;
		case 32: goto tr800;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st327;
	goto st0;
tr822:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st328;
tr800:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st328;
st328:
	if ( ++p == pe )
		goto _test_eof328;
case 328:
#line 6889 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st328;
		case 10: goto st0;
		case 12: goto st334;
		case 32: goto st328;
		case 34: goto tr806;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr807;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr803;
tr803:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st329;
tr821:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st329;
tr815:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st329;
st329:
	if ( ++p == pe )
		goto _test_eof329;
case 329:
#line 6916 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr809;
		case 10: goto tr810;
		case 12: goto tr811;
		case 32: goto tr809;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st330;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st329;
tr807:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st330;
tr825:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st330;
tr819:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st330;
st330:
	if ( ++p == pe )
		goto _test_eof330;
case 330:
#line 6942 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st331;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st332;
	goto st329;
st331:
	if ( ++p == pe )
		goto _test_eof331;
case 331:
	switch( (*p) ) {
		case 9: goto tr816;
		case 10: goto tr817;
		case 12: goto tr818;
		case 32: goto tr816;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr819;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr815;
st332:
	if ( ++p == pe )
		goto _test_eof332;
case 332:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st333;
	goto st0;
st333:
	if ( ++p == pe )
		goto _test_eof333;
case 333:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st329;
	goto st0;
tr823:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st334;
tr801:
#line 1076 "./src/zscan_rfc1035.rl"
	{ set_uval16(z); }
#line 1086 "./src/zscan_rfc1035.rl"
	{ z->uv_3 = z->uval; }
	goto st334;
st334:
	if ( ++p == pe )
		goto _test_eof334;
case 334:
#line 6988 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr822;
		case 10: goto st0;
		case 12: goto tr823;
		case 32: goto tr822;
		case 34: goto tr824;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr825;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr821;
tr806:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st335;
tr824:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st335;
tr830:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st335;
st335:
	if ( ++p == pe )
		goto _test_eof335;
case 335:
#line 7015 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st336;
		case 34: goto st337;
		case 92: goto st338;
	}
	goto st335;
tr831:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st336;
st336:
	if ( ++p == pe )
		goto _test_eof336;
case 336:
#line 7028 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr831;
		case 34: goto tr832;
		case 92: goto tr833;
	}
	goto tr830;
tr832:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st337;
st337:
	if ( ++p == pe )
		goto _test_eof337;
case 337:
#line 7041 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr834;
		case 10: goto tr835;
		case 12: goto tr836;
		case 32: goto tr834;
	}
	goto st0;
tr833:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st338;
st338:
	if ( ++p == pe )
		goto _test_eof338;
case 338:
#line 7055 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st336;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st339;
	goto st335;
st339:
	if ( ++p == pe )
		goto _test_eof339;
case 339:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st340;
	goto st0;
st340:
	if ( ++p == pe )
		goto _test_eof340;
case 340:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st335;
	goto st0;
tr16:
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st341;
tr55:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st341;
tr28:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1082 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->def_ttl; }
	goto st341;
st341:
	if ( ++p == pe )
		goto _test_eof341;
case 341:
#line 7088 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 88: goto st342;
		case 89: goto st361;
		case 120: goto st342;
		case 121: goto st361;
	}
	goto st0;
st342:
	if ( ++p == pe )
		goto _test_eof342;
case 342:
	switch( (*p) ) {
		case 84: goto st343;
		case 116: goto st343;
	}
	goto st0;
st343:
	if ( ++p == pe )
		goto _test_eof343;
case 343:
	switch( (*p) ) {
		case 9: goto st344;
		case 12: goto st360;
		case 32: goto st344;
	}
	goto st0;
st344:
	if ( ++p == pe )
		goto _test_eof344;
case 344:
	switch( (*p) ) {
		case 9: goto tr845;
		case 10: goto st0;
		case 12: goto tr846;
		case 32: goto tr845;
		case 34: goto tr847;
		case 59: goto st0;
		case 92: goto tr848;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr844;
tr855:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st345;
tr861:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st345;
tr844:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st345;
tr883:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st345;
tr892:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st345;
tr875:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st345;
st345:
	if ( ++p == pe )
		goto _test_eof345;
case 345:
#line 7157 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr850;
		case 10: goto tr851;
		case 12: goto tr852;
		case 32: goto tr850;
		case 34: goto tr853;
		case 59: goto st0;
		case 92: goto st351;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st345;
tr862:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st346;
tr850:
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
	goto st346;
tr876:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
	goto st346;
tr884:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
	goto st346;
st346:
	if ( ++p == pe )
		goto _test_eof346;
case 346:
#line 7186 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st346;
		case 10: goto tr857;
		case 12: goto st347;
		case 32: goto st346;
		case 34: goto tr859;
		case 59: goto st0;
		case 92: goto tr860;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr855;
tr864:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st347;
tr852:
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
	goto st347;
tr878:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
	goto st347;
tr886:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
	goto st347;
st347:
	if ( ++p == pe )
		goto _test_eof347;
case 347:
#line 7215 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr862;
		case 10: goto tr863;
		case 12: goto tr864;
		case 32: goto tr862;
		case 34: goto tr865;
		case 59: goto st0;
		case 92: goto tr866;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr861;
tr859:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr865:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr847:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr871:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st348;
tr895:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr853:
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr879:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
tr887:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1065 "./src/zscan_rfc1035.rl"
	{ text_add_tok(z, p - z->tstart, true); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st348;
st348:
	if ( ++p == pe )
		goto _test_eof348;
case 348:
#line 7263 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st349;
		case 34: goto st350;
		case 92: goto st355;
	}
	goto st348;
tr872:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st349;
st349:
	if ( ++p == pe )
		goto _test_eof349;
case 349:
#line 7276 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr872;
		case 34: goto tr873;
		case 92: goto tr874;
	}
	goto tr871;
tr873:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st350;
st350:
	if ( ++p == pe )
		goto _test_eof350;
case 350:
#line 7289 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr876;
		case 10: goto tr877;
		case 12: goto tr878;
		case 32: goto tr876;
		case 34: goto tr879;
		case 59: goto st0;
		case 92: goto tr880;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr875;
tr860:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st351;
tr866:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st351;
tr848:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st351;
tr888:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st351;
tr896:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st351;
tr880:
#line 1066 "./src/zscan_rfc1035.rl"
	{ z->tstart++; text_add_tok(z, p - z->tstart - 1, true); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st351;
st351:
	if ( ++p == pe )
		goto _test_eof351;
case 351:
#line 7328 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st352;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st353;
	goto st345;
st352:
	if ( ++p == pe )
		goto _test_eof352;
case 352:
	switch( (*p) ) {
		case 9: goto tr884;
		case 10: goto tr885;
		case 12: goto tr886;
		case 32: goto tr884;
		case 34: goto tr887;
		case 59: goto st0;
		case 92: goto tr888;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr883;
st353:
	if ( ++p == pe )
		goto _test_eof353;
case 353:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st354;
	goto st0;
st354:
	if ( ++p == pe )
		goto _test_eof354;
case 354:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st345;
	goto st0;
tr874:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st355;
st355:
	if ( ++p == pe )
		goto _test_eof355;
case 355:
#line 7370 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st349;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st356;
	goto st348;
st356:
	if ( ++p == pe )
		goto _test_eof356;
case 356:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st357;
	goto st0;
st357:
	if ( ++p == pe )
		goto _test_eof357;
case 357:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st348;
	goto st0;
tr845:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
	goto st358;
tr893:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
	goto st358;
st358:
	if ( ++p == pe )
		goto _test_eof358;
case 358:
#line 7400 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr845;
		case 10: goto tr857;
		case 12: goto tr846;
		case 32: goto tr845;
		case 34: goto tr847;
		case 59: goto st0;
		case 92: goto tr848;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr844;
tr846:
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
	goto st359;
tr894:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1064 "./src/zscan_rfc1035.rl"
	{ text_start(z); }
	goto st359;
st359:
	if ( ++p == pe )
		goto _test_eof359;
case 359:
#line 7423 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr893;
		case 10: goto tr863;
		case 12: goto tr894;
		case 32: goto tr893;
		case 34: goto tr895;
		case 59: goto st0;
		case 92: goto tr896;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr892;
st360:
	if ( ++p == pe )
		goto _test_eof360;
case 360:
	switch( (*p) ) {
		case 9: goto tr893;
		case 10: goto st0;
		case 12: goto tr894;
		case 32: goto tr893;
		case 34: goto tr895;
		case 59: goto st0;
		case 92: goto tr896;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr892;
st361:
	if ( ++p == pe )
		goto _test_eof361;
case 361:
	switch( (*p) ) {
		case 80: goto st362;
		case 112: goto st362;
	}
	goto st0;
st362:
	if ( ++p == pe )
		goto _test_eof362;
case 362:
	switch( (*p) ) {
		case 69: goto st363;
		case 101: goto st363;
	}
	goto st0;
st363:
	if ( ++p == pe )
		goto _test_eof363;
case 363:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr899;
	goto st0;
tr899:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st364;
st364:
	if ( ++p == pe )
		goto _test_eof364;
case 364:
#line 7483 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr900;
		case 12: goto tr901;
		case 32: goto tr900;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st364;
	goto st0;
tr906:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st365;
tr900:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st365;
st365:
	if ( ++p == pe )
		goto _test_eof365;
case 365:
#line 7502 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st365;
		case 12: goto st366;
		case 32: goto st365;
		case 92: goto st367;
	}
	goto st0;
tr907:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st366;
tr901:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st366;
st366:
	if ( ++p == pe )
		goto _test_eof366;
case 366:
#line 7520 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr906;
		case 12: goto tr907;
		case 32: goto tr906;
		case 92: goto tr908;
	}
	goto st0;
tr908:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st367;
st367:
	if ( ++p == pe )
		goto _test_eof367;
case 367:
#line 7534 "src/zscan_rfc1035.c"
	if ( (*p) == 35 )
		goto st368;
	goto st0;
st368:
	if ( ++p == pe )
		goto _test_eof368;
case 368:
	switch( (*p) ) {
		case 9: goto st369;
		case 12: goto st370;
		case 32: goto st369;
	}
	goto st0;
tr913:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st369;
st369:
	if ( ++p == pe )
		goto _test_eof369;
case 369:
#line 7554 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st369;
		case 12: goto st370;
		case 32: goto st369;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr912;
	goto st0;
tr914:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st370;
st370:
	if ( ++p == pe )
		goto _test_eof370;
case 370:
#line 7569 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr913;
		case 12: goto tr914;
		case 32: goto tr913;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr915;
	goto st0;
tr912:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st371;
tr915:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st371;
st371:
	if ( ++p == pe )
		goto _test_eof371;
case 371:
#line 7588 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr916;
		case 12: goto tr917;
		case 32: goto tr916;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st371;
	goto st0;
tr916:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1110 "./src/zscan_rfc1035.rl"
	{ rfc3597_data_setup(z); }
	goto st372;
tr919:
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st372;
tr923:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st372;
st372:
	if ( ++p == pe )
		goto _test_eof372;
case 372:
#line 7611 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr919;
		case 10: goto tr920;
		case 12: goto tr921;
		case 32: goto tr919;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr922;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr922;
	} else
		goto tr922;
	goto st0;
tr917:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1110 "./src/zscan_rfc1035.rl"
	{ rfc3597_data_setup(z); }
	goto st373;
tr921:
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st373;
tr925:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1105 "./src/zscan_rfc1035.rl"
	{ rec_rfc3597(z); }
	goto st373;
st373:
	if ( ++p == pe )
		goto _test_eof373;
case 373:
#line 7641 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr923;
		case 10: goto tr924;
		case 12: goto tr925;
		case 32: goto tr923;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr926;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr926;
	} else
		goto tr926;
	goto st0;
tr922:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st374;
tr926:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st374;
tr931:
#line 1111 "./src/zscan_rfc1035.rl"
	{ rfc3597_octet(z); }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st374;
st374:
	if ( ++p == pe )
		goto _test_eof374;
case 374:
#line 7671 "src/zscan_rfc1035.c"
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto st375;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto st375;
	} else
		goto st375;
	goto st0;
st375:
	if ( ++p == pe )
		goto _test_eof375;
case 375:
	switch( (*p) ) {
		case 9: goto tr928;
		case 10: goto tr929;
		case 12: goto tr930;
		case 32: goto tr928;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr931;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr931;
	} else
		goto tr931;
	goto st0;
tr934:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st376;
tr928:
#line 1111 "./src/zscan_rfc1035.rl"
	{ rfc3597_octet(z); }
	goto st376;
st376:
	if ( ++p == pe )
		goto _test_eof376;
case 376:
#line 7709 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st376;
		case 10: goto tr920;
		case 12: goto st377;
		case 32: goto st376;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr922;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr922;
	} else
		goto tr922;
	goto st0;
tr935:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st377;
tr930:
#line 1111 "./src/zscan_rfc1035.rl"
	{ rfc3597_octet(z); }
	goto st377;
st377:
	if ( ++p == pe )
		goto _test_eof377;
case 377:
#line 7734 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr934;
		case 10: goto tr924;
		case 12: goto tr935;
		case 32: goto tr934;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 57 )
			goto tr926;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr926;
	} else
		goto tr926;
	goto st0;
tr349:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st378;
tr951:
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st378;
st378:
	if ( ++p == pe )
		goto _test_eof378;
case 378:
#line 7760 "src/zscan_rfc1035.c"
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr936;
	goto st0;
tr936:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st379;
st379:
	if ( ++p == pe )
		goto _test_eof379;
case 379:
#line 7770 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr937;
		case 12: goto tr938;
		case 32: goto tr937;
		case 68: goto tr940;
		case 72: goto tr940;
		case 77: goto tr940;
		case 87: goto tr940;
		case 100: goto tr940;
		case 104: goto tr940;
		case 109: goto tr940;
		case 119: goto tr940;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st379;
	goto st0;
tr944:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st380;
tr937:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st380;
tr947:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st380;
st380:
	if ( ++p == pe )
		goto _test_eof380;
case 380:
#line 7802 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st380;
		case 12: goto st381;
		case 32: goto st380;
		case 68: goto st382;
		case 100: goto st382;
	}
	goto st0;
tr945:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st381;
tr938:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st381;
tr948:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st381;
st381:
	if ( ++p == pe )
		goto _test_eof381;
case 381:
#line 7826 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr944;
		case 12: goto tr945;
		case 32: goto tr944;
		case 68: goto tr946;
		case 100: goto tr946;
	}
	goto st0;
tr946:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st382;
st382:
	if ( ++p == pe )
		goto _test_eof382;
case 382:
#line 7841 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 89: goto st113;
		case 121: goto st113;
	}
	goto st0;
tr940:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st383;
st383:
	if ( ++p == pe )
		goto _test_eof383;
case 383:
#line 7854 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr947;
		case 12: goto tr948;
		case 32: goto tr947;
	}
	goto st0;
tr351:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st384;
st384:
	if ( ++p == pe )
		goto _test_eof384;
case 384:
#line 7868 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr949;
		case 12: goto tr950;
		case 32: goto tr949;
		case 47: goto tr951;
	}
	goto st0;
tr1173:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st385;
tr956:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st385;
tr1180:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st385;
st385:
	if ( ++p == pe )
		goto _test_eof385;
case 385:
#line 7889 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st386;
		case 34: goto st387;
		case 92: goto st388;
	}
	goto st385;
tr957:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st386;
st386:
	if ( ++p == pe )
		goto _test_eof386;
case 386:
#line 7902 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr957;
		case 34: goto tr958;
		case 92: goto tr959;
	}
	goto tr956;
tr958:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st387;
st387:
	if ( ++p == pe )
		goto _test_eof387;
case 387:
#line 7915 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr960;
		case 12: goto tr961;
		case 32: goto tr960;
	}
	goto st0;
tr959:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st388;
st388:
	if ( ++p == pe )
		goto _test_eof388;
case 388:
#line 7928 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st386;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st389;
	goto st385;
st389:
	if ( ++p == pe )
		goto _test_eof389;
case 389:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st390;
	goto st0;
st390:
	if ( ++p == pe )
		goto _test_eof390;
case 390:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st385;
	goto st0;
tr1181:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st391;
st391:
	if ( ++p == pe )
		goto _test_eof391;
case 391:
#line 7954 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 66: goto st392;
		case 73: goto st417;
		case 79: goto st450;
		case 84: goto st469;
		case 105: goto st417;
		case 111: goto st450;
		case 116: goto st469;
	}
	goto st0;
st392:
	if ( ++p == pe )
		goto _test_eof392;
case 392:
	if ( (*p) == 82 )
		goto st393;
	goto st0;
st393:
	if ( ++p == pe )
		goto _test_eof393;
case 393:
	if ( (*p) == 69 )
		goto st394;
	goto st0;
st394:
	if ( ++p == pe )
		goto _test_eof394;
case 394:
	if ( (*p) == 65 )
		goto st395;
	goto st0;
st395:
	if ( ++p == pe )
		goto _test_eof395;
case 395:
	if ( (*p) == 75 )
		goto st396;
	goto st0;
st396:
	if ( ++p == pe )
		goto _test_eof396;
case 396:
	if ( (*p) == 95 )
		goto st397;
	goto st0;
st397:
	if ( ++p == pe )
		goto _test_eof397;
case 397:
	if ( (*p) == 77 )
		goto st398;
	goto st0;
st398:
	if ( ++p == pe )
		goto _test_eof398;
case 398:
	if ( (*p) == 89 )
		goto st399;
	goto st0;
st399:
	if ( ++p == pe )
		goto _test_eof399;
case 399:
	if ( (*p) == 95 )
		goto st400;
	goto st0;
st400:
	if ( ++p == pe )
		goto _test_eof400;
case 400:
	if ( (*p) == 90 )
		goto st401;
	goto st0;
st401:
	if ( ++p == pe )
		goto _test_eof401;
case 401:
	if ( (*p) == 79 )
		goto st402;
	goto st0;
st402:
	if ( ++p == pe )
		goto _test_eof402;
case 402:
	if ( (*p) == 78 )
		goto st403;
	goto st0;
st403:
	if ( ++p == pe )
		goto _test_eof403;
case 403:
	if ( (*p) == 69 )
		goto st404;
	goto st0;
st404:
	if ( ++p == pe )
		goto _test_eof404;
case 404:
	if ( (*p) == 95 )
		goto st405;
	goto st0;
st405:
	if ( ++p == pe )
		goto _test_eof405;
case 405:
	switch( (*p) ) {
		case 69: goto st406;
		case 80: goto st413;
	}
	goto st0;
st406:
	if ( ++p == pe )
		goto _test_eof406;
case 406:
	if ( (*p) == 68 )
		goto st407;
	goto st0;
st407:
	if ( ++p == pe )
		goto _test_eof407;
case 407:
	if ( (*p) == 50 )
		goto st408;
	goto st0;
st408:
	if ( ++p == pe )
		goto _test_eof408;
case 408:
	if ( (*p) == 53 )
		goto st409;
	goto st0;
st409:
	if ( ++p == pe )
		goto _test_eof409;
case 409:
	if ( (*p) == 53 )
		goto st410;
	goto st0;
st410:
	if ( ++p == pe )
		goto _test_eof410;
case 410:
	if ( (*p) == 49 )
		goto st411;
	goto st0;
st411:
	if ( ++p == pe )
		goto _test_eof411;
case 411:
	if ( (*p) == 57 )
		goto st412;
	goto st0;
st412:
	if ( ++p == pe )
		goto _test_eof412;
case 412:
	switch( (*p) ) {
		case 9: goto tr989;
		case 10: goto tr990;
		case 12: goto tr991;
		case 32: goto tr989;
	}
	goto st0;
st413:
	if ( ++p == pe )
		goto _test_eof413;
case 413:
	if ( (*p) == 50 )
		goto st414;
	goto st0;
st414:
	if ( ++p == pe )
		goto _test_eof414;
case 414:
	if ( (*p) == 53 )
		goto st415;
	goto st0;
st415:
	if ( ++p == pe )
		goto _test_eof415;
case 415:
	if ( (*p) == 54 )
		goto st416;
	goto st0;
st416:
	if ( ++p == pe )
		goto _test_eof416;
case 416:
	switch( (*p) ) {
		case 9: goto tr995;
		case 10: goto tr996;
		case 12: goto tr997;
		case 32: goto tr995;
	}
	goto st0;
st417:
	if ( ++p == pe )
		goto _test_eof417;
case 417:
	switch( (*p) ) {
		case 78: goto st418;
		case 110: goto st418;
	}
	goto st0;
st418:
	if ( ++p == pe )
		goto _test_eof418;
case 418:
	switch( (*p) ) {
		case 67: goto st419;
		case 99: goto st419;
	}
	goto st0;
st419:
	if ( ++p == pe )
		goto _test_eof419;
case 419:
	switch( (*p) ) {
		case 76: goto st420;
		case 108: goto st420;
	}
	goto st0;
st420:
	if ( ++p == pe )
		goto _test_eof420;
case 420:
	switch( (*p) ) {
		case 85: goto st421;
		case 117: goto st421;
	}
	goto st0;
st421:
	if ( ++p == pe )
		goto _test_eof421;
case 421:
	switch( (*p) ) {
		case 68: goto st422;
		case 100: goto st422;
	}
	goto st0;
st422:
	if ( ++p == pe )
		goto _test_eof422;
case 422:
	switch( (*p) ) {
		case 69: goto st423;
		case 101: goto st423;
	}
	goto st0;
st423:
	if ( ++p == pe )
		goto _test_eof423;
case 423:
	switch( (*p) ) {
		case 9: goto tr1004;
		case 12: goto tr1005;
		case 32: goto tr1004;
	}
	goto st0;
tr1061:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st424;
tr1004:
#line 1053 "./src/zscan_rfc1035.rl"
	{ dname_copy(z->rhs_dname, z->origin); }
	goto st424;
st424:
	if ( ++p == pe )
		goto _test_eof424;
case 424:
#line 8223 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st424;
		case 10: goto st0;
		case 12: goto st443;
		case 32: goto st424;
		case 34: goto tr1009;
		case 59: goto st0;
		case 92: goto tr1010;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1006;
tr1006:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st425;
tr1060:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st425;
tr1054:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st425;
st425:
	if ( ++p == pe )
		goto _test_eof425;
case 425:
#line 8249 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1012;
		case 10: goto tr1013;
		case 12: goto tr1014;
		case 32: goto tr1012;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st439;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st425;
tr1035:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st426;
tr1012:
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
	goto st426;
tr1055:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
	goto st426;
tr1073:
#line 1061 "./src/zscan_rfc1035.rl"
	{ z->tstart++; set_filename(z, p - z->tstart - 1); }
	goto st426;
st426:
	if ( ++p == pe )
		goto _test_eof426;
case 426:
#line 8278 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st426;
		case 10: goto st0;
		case 12: goto st432;
		case 32: goto st426;
		case 34: goto tr1019;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr1020;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1016;
tr1016:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st427;
tr1034:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st427;
tr1028:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st427;
st427:
	if ( ++p == pe )
		goto _test_eof427;
case 427:
#line 8305 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1022;
		case 10: goto tr1023;
		case 12: goto tr1024;
		case 32: goto tr1022;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st428;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st427;
tr1020:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st428;
tr1038:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st428;
tr1032:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st428;
st428:
	if ( ++p == pe )
		goto _test_eof428;
case 428:
#line 8331 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st429;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st430;
	goto st427;
st429:
	if ( ++p == pe )
		goto _test_eof429;
case 429:
	switch( (*p) ) {
		case 9: goto tr1029;
		case 10: goto tr1030;
		case 12: goto tr1031;
		case 32: goto tr1029;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr1032;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1028;
st430:
	if ( ++p == pe )
		goto _test_eof430;
case 430:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st431;
	goto st0;
st431:
	if ( ++p == pe )
		goto _test_eof431;
case 431:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st427;
	goto st0;
tr1036:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st432;
tr1014:
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
	goto st432;
tr1057:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1060 "./src/zscan_rfc1035.rl"
	{ set_filename(z, p - z->tstart); }
	goto st432;
tr1075:
#line 1061 "./src/zscan_rfc1035.rl"
	{ z->tstart++; set_filename(z, p - z->tstart - 1); }
	goto st432;
st432:
	if ( ++p == pe )
		goto _test_eof432;
case 432:
#line 8383 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1035;
		case 10: goto st0;
		case 12: goto tr1036;
		case 32: goto tr1035;
		case 34: goto tr1037;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr1038;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1034;
tr1019:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st433;
tr1037:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st433;
tr1043:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st433;
st433:
	if ( ++p == pe )
		goto _test_eof433;
case 433:
#line 8410 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st434;
		case 34: goto st435;
		case 92: goto st436;
	}
	goto st433;
tr1044:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st434;
st434:
	if ( ++p == pe )
		goto _test_eof434;
case 434:
#line 8423 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr1044;
		case 34: goto tr1045;
		case 92: goto tr1046;
	}
	goto tr1043;
tr1045:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st435;
st435:
	if ( ++p == pe )
		goto _test_eof435;
case 435:
#line 8436 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1047;
		case 10: goto tr1048;
		case 12: goto tr1049;
		case 32: goto tr1047;
	}
	goto st0;
tr1046:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st436;
st436:
	if ( ++p == pe )
		goto _test_eof436;
case 436:
#line 8450 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st434;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st437;
	goto st433;
st437:
	if ( ++p == pe )
		goto _test_eof437;
case 437:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st438;
	goto st0;
st438:
	if ( ++p == pe )
		goto _test_eof438;
case 438:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st433;
	goto st0;
tr1010:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st439;
tr1064:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st439;
tr1058:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st439;
st439:
	if ( ++p == pe )
		goto _test_eof439;
case 439:
#line 8483 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st440;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st441;
	goto st425;
st440:
	if ( ++p == pe )
		goto _test_eof440;
case 440:
	switch( (*p) ) {
		case 9: goto tr1055;
		case 10: goto tr1056;
		case 12: goto tr1057;
		case 32: goto tr1055;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr1058;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1054;
st441:
	if ( ++p == pe )
		goto _test_eof441;
case 441:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st442;
	goto st0;
st442:
	if ( ++p == pe )
		goto _test_eof442;
case 442:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st425;
	goto st0;
tr1062:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st443;
tr1005:
#line 1053 "./src/zscan_rfc1035.rl"
	{ dname_copy(z->rhs_dname, z->origin); }
	goto st443;
st443:
	if ( ++p == pe )
		goto _test_eof443;
case 443:
#line 8528 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1061;
		case 10: goto st0;
		case 12: goto tr1062;
		case 32: goto tr1061;
		case 34: goto tr1063;
		case 59: goto st0;
		case 92: goto tr1064;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1060;
tr1009:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st444;
tr1063:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st444;
tr1069:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st444;
st444:
	if ( ++p == pe )
		goto _test_eof444;
case 444:
#line 8554 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st445;
		case 34: goto st446;
		case 92: goto st447;
	}
	goto st444;
tr1070:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st445;
st445:
	if ( ++p == pe )
		goto _test_eof445;
case 445:
#line 8567 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr1070;
		case 34: goto tr1071;
		case 92: goto tr1072;
	}
	goto tr1069;
tr1071:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st446;
st446:
	if ( ++p == pe )
		goto _test_eof446;
case 446:
#line 8580 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1073;
		case 10: goto tr1074;
		case 12: goto tr1075;
		case 32: goto tr1073;
	}
	goto st0;
tr1072:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st447;
st447:
	if ( ++p == pe )
		goto _test_eof447;
case 447:
#line 8594 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st445;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st448;
	goto st444;
st448:
	if ( ++p == pe )
		goto _test_eof448;
case 448:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st449;
	goto st0;
st449:
	if ( ++p == pe )
		goto _test_eof449;
case 449:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st444;
	goto st0;
st450:
	if ( ++p == pe )
		goto _test_eof450;
case 450:
	switch( (*p) ) {
		case 82: goto st451;
		case 114: goto st451;
	}
	goto st0;
st451:
	if ( ++p == pe )
		goto _test_eof451;
case 451:
	switch( (*p) ) {
		case 73: goto st452;
		case 105: goto st452;
	}
	goto st0;
st452:
	if ( ++p == pe )
		goto _test_eof452;
case 452:
	switch( (*p) ) {
		case 71: goto st453;
		case 103: goto st453;
	}
	goto st0;
st453:
	if ( ++p == pe )
		goto _test_eof453;
case 453:
	switch( (*p) ) {
		case 73: goto st454;
		case 105: goto st454;
	}
	goto st0;
st454:
	if ( ++p == pe )
		goto _test_eof454;
case 454:
	switch( (*p) ) {
		case 78: goto st455;
		case 110: goto st455;
	}
	goto st0;
st455:
	if ( ++p == pe )
		goto _test_eof455;
case 455:
	switch( (*p) ) {
		case 9: goto st456;
		case 12: goto st462;
		case 32: goto st456;
	}
	goto st0;
tr1102:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st456;
st456:
	if ( ++p == pe )
		goto _test_eof456;
case 456:
#line 8675 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st456;
		case 10: goto st0;
		case 12: goto st462;
		case 32: goto st456;
		case 34: goto tr1086;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr1087;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1085;
tr1085:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st457;
tr1101:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st457;
tr1095:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st457;
st457:
	if ( ++p == pe )
		goto _test_eof457;
case 457:
#line 8702 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1089;
		case 10: goto tr1090;
		case 12: goto tr1091;
		case 32: goto tr1089;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto st458;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto st457;
tr1087:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st458;
tr1105:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st458;
tr1099:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st458;
st458:
	if ( ++p == pe )
		goto _test_eof458;
case 458:
#line 8728 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st459;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st460;
	goto st457;
st459:
	if ( ++p == pe )
		goto _test_eof459;
case 459:
	switch( (*p) ) {
		case 9: goto tr1096;
		case 10: goto tr1097;
		case 12: goto tr1098;
		case 32: goto tr1096;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr1099;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1095;
st460:
	if ( ++p == pe )
		goto _test_eof460;
case 460:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st461;
	goto st0;
st461:
	if ( ++p == pe )
		goto _test_eof461;
case 461:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st457;
	goto st0;
tr1103:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st462;
st462:
	if ( ++p == pe )
		goto _test_eof462;
case 462:
#line 8770 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1102;
		case 10: goto st0;
		case 12: goto tr1103;
		case 32: goto tr1102;
		case 34: goto tr1104;
		case 36: goto st0;
		case 59: goto st0;
		case 92: goto tr1105;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1101;
tr1086:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st463;
tr1104:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st463;
tr1110:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st463;
st463:
	if ( ++p == pe )
		goto _test_eof463;
case 463:
#line 8797 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto st464;
		case 34: goto st465;
		case 92: goto st466;
	}
	goto st463;
tr1111:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st464;
st464:
	if ( ++p == pe )
		goto _test_eof464;
case 464:
#line 8810 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 10: goto tr1111;
		case 34: goto tr1112;
		case 92: goto tr1113;
	}
	goto tr1110;
tr1112:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st465;
st465:
	if ( ++p == pe )
		goto _test_eof465;
case 465:
#line 8823 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1114;
		case 10: goto tr1115;
		case 12: goto tr1116;
		case 32: goto tr1114;
	}
	goto st0;
tr1113:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st466;
st466:
	if ( ++p == pe )
		goto _test_eof466;
case 466:
#line 8837 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st464;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st467;
	goto st463;
st467:
	if ( ++p == pe )
		goto _test_eof467;
case 467:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st468;
	goto st0;
st468:
	if ( ++p == pe )
		goto _test_eof468;
case 468:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st463;
	goto st0;
st469:
	if ( ++p == pe )
		goto _test_eof469;
case 469:
	switch( (*p) ) {
		case 84: goto st470;
		case 116: goto st470;
	}
	goto st0;
st470:
	if ( ++p == pe )
		goto _test_eof470;
case 470:
	switch( (*p) ) {
		case 76: goto st471;
		case 108: goto st471;
	}
	goto st0;
st471:
	if ( ++p == pe )
		goto _test_eof471;
case 471:
	switch( (*p) ) {
		case 9: goto st472;
		case 12: goto st473;
		case 32: goto st472;
	}
	goto st0;
tr1124:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st472;
st472:
	if ( ++p == pe )
		goto _test_eof472;
case 472:
#line 8891 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st472;
		case 12: goto st473;
		case 32: goto st472;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr1123;
	goto st0;
tr1125:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st473;
st473:
	if ( ++p == pe )
		goto _test_eof473;
case 473:
#line 8906 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1124;
		case 12: goto tr1125;
		case 32: goto tr1124;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr1126;
	goto st0;
tr1123:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st474;
tr1126:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st474;
st474:
	if ( ++p == pe )
		goto _test_eof474;
case 474:
#line 8925 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1127;
		case 10: goto tr1128;
		case 12: goto tr1129;
		case 32: goto tr1127;
		case 68: goto tr1131;
		case 72: goto tr1131;
		case 77: goto tr1131;
		case 87: goto tr1131;
		case 100: goto tr1131;
		case 104: goto tr1131;
		case 109: goto tr1131;
		case 119: goto tr1131;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st474;
	goto st0;
tr1131:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st475;
st475:
	if ( ++p == pe )
		goto _test_eof475;
case 475:
#line 8950 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1132;
		case 10: goto tr1133;
		case 12: goto tr1134;
		case 32: goto tr1132;
	}
	goto st0;
tr1175:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st476;
tr1140:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st476;
tr1182:
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st476;
st476:
	if ( ++p == pe )
		goto _test_eof476;
case 476:
#line 8971 "src/zscan_rfc1035.c"
	if ( (*p) == 10 )
		goto st477;
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st478;
	goto st1;
st477:
	if ( ++p == pe )
		goto _test_eof477;
case 477:
	switch( (*p) ) {
		case 9: goto tr1138;
		case 10: goto st0;
		case 12: goto tr1139;
		case 32: goto tr1138;
		case 34: goto st0;
		case 59: goto st0;
		case 92: goto tr1140;
	}
	if ( 40 <= (*p) && (*p) <= 41 )
		goto st0;
	goto tr1137;
st478:
	if ( ++p == pe )
		goto _test_eof478;
case 478:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st479;
	goto st0;
st479:
	if ( ++p == pe )
		goto _test_eof479;
case 479:
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st1;
	goto st0;
st480:
	if ( ++p == pe )
		goto _test_eof480;
case 480:
	switch( (*p) ) {
		case 65: goto st481;
		case 97: goto st481;
	}
	goto st0;
st481:
	if ( ++p == pe )
		goto _test_eof481;
case 481:
	switch( (*p) ) {
		case 65: goto st482;
		case 97: goto st482;
	}
	goto st0;
st482:
	if ( ++p == pe )
		goto _test_eof482;
case 482:
	switch( (*p) ) {
		case 9: goto st483;
		case 12: goto st484;
		case 32: goto st483;
	}
	goto st0;
tr1147:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st483;
st483:
	if ( ++p == pe )
		goto _test_eof483;
case 483:
#line 9041 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st483;
		case 12: goto st484;
		case 32: goto st483;
		case 46: goto tr1146;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 58 )
			goto tr1146;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr1146;
	} else
		goto tr1146;
	goto st0;
tr1148:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st484;
st484:
	if ( ++p == pe )
		goto _test_eof484;
case 484:
#line 9063 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1147;
		case 12: goto tr1148;
		case 32: goto tr1147;
		case 46: goto tr1149;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 58 )
			goto tr1149;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto tr1149;
	} else
		goto tr1149;
	goto st0;
tr1146:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st485;
tr1149:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st485;
st485:
	if ( ++p == pe )
		goto _test_eof485;
case 485:
#line 9089 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1150;
		case 10: goto tr1151;
		case 12: goto tr1152;
		case 32: goto tr1150;
		case 46: goto st485;
	}
	if ( (*p) < 65 ) {
		if ( 48 <= (*p) && (*p) <= 58 )
			goto st485;
	} else if ( (*p) > 70 ) {
		if ( 97 <= (*p) && (*p) <= 102 )
			goto st485;
	} else
		goto st485;
	goto st0;
tr50:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st486;
st486:
	if ( ++p == pe )
		goto _test_eof486;
case 486:
#line 9112 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 78: goto st487;
		case 110: goto st487;
	}
	goto st0;
st487:
	if ( ++p == pe )
		goto _test_eof487;
case 487:
	switch( (*p) ) {
		case 9: goto st148;
		case 12: goto st149;
		case 32: goto st148;
	}
	goto st0;
tr31:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st488;
tr1171:
#line 1084 "./src/zscan_rfc1035.rl"
	{ z->uv_1 = z->uval; }
	goto st488;
st488:
	if ( ++p == pe )
		goto _test_eof488;
case 488:
#line 9138 "src/zscan_rfc1035.c"
	if ( 48 <= (*p) && (*p) <= 57 )
		goto tr1155;
	goto st0;
tr1155:
#line 1043 "./src/zscan_rfc1035.rl"
	{ z->tstart = p; }
	goto st489;
st489:
	if ( ++p == pe )
		goto _test_eof489;
case 489:
#line 9148 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1156;
		case 12: goto tr1157;
		case 32: goto tr1156;
		case 68: goto tr1159;
		case 72: goto tr1159;
		case 77: goto tr1159;
		case 87: goto tr1159;
		case 100: goto tr1159;
		case 104: goto tr1159;
		case 109: goto tr1159;
		case 119: goto tr1159;
	}
	if ( 48 <= (*p) && (*p) <= 57 )
		goto st489;
	goto st0;
tr1163:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st490;
tr1156:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st490;
tr1167:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st490;
st490:
	if ( ++p == pe )
		goto _test_eof490;
case 490:
#line 9180 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto st490;
		case 12: goto st491;
		case 32: goto st490;
		case 68: goto st382;
		case 73: goto st492;
		case 100: goto st382;
		case 105: goto st492;
	}
	goto st0;
tr1164:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st491;
tr1157:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st491;
tr1168:
#line 1085 "./src/zscan_rfc1035.rl"
	{ z->uv_2 = z->uval; }
#line 1080 "./src/zscan_rfc1035.rl"
	{ z->ttl  = z->uv_1; z->ttl_min = z->uv_2 ? z->uv_2 : z->uv_1 >> 1; }
	goto st491;
st491:
	if ( ++p == pe )
		goto _test_eof491;
case 491:
#line 9206 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1163;
		case 12: goto tr1164;
		case 32: goto tr1163;
		case 68: goto tr946;
		case 73: goto tr1165;
		case 100: goto tr946;
		case 105: goto tr1165;
	}
	goto st0;
tr1165:
#line 1124 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	goto st492;
st492:
	if ( ++p == pe )
		goto _test_eof492;
case 492:
#line 9223 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 78: goto st493;
		case 110: goto st493;
	}
	goto st0;
st493:
	if ( ++p == pe )
		goto _test_eof493;
case 493:
	switch( (*p) ) {
		case 9: goto st380;
		case 12: goto st381;
		case 32: goto st380;
	}
	goto st0;
tr1159:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st494;
st494:
	if ( ++p == pe )
		goto _test_eof494;
case 494:
#line 9246 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1167;
		case 12: goto tr1168;
		case 32: goto tr1167;
	}
	goto st0;
tr33:
#line 1074 "./src/zscan_rfc1035.rl"
	{ set_uval(z); }
#line 1077 "./src/zscan_rfc1035.rl"
	{ mult_uval(z, (*p)); }
	goto st495;
st495:
	if ( ++p == pe )
		goto _test_eof495;
case 495:
#line 9260 "src/zscan_rfc1035.c"
	switch( (*p) ) {
		case 9: goto tr1169;
		case 12: goto tr1170;
		case 32: goto tr1169;
		case 47: goto tr1171;
	}
	goto st0;
	}
	_test_eof1: cs = 1; goto _test_eof; 
	_test_eof2: cs = 2; goto _test_eof; 
	_test_eof3: cs = 3; goto _test_eof; 
	_test_eof4: cs = 4; goto _test_eof; 
	_test_eof5: cs = 5; goto _test_eof; 
	_test_eof6: cs = 6; goto _test_eof; 
	_test_eof7: cs = 7; goto _test_eof; 
	_test_eof8: cs = 8; goto _test_eof; 
	_test_eof9: cs = 9; goto _test_eof; 
	_test_eof10: cs = 10; goto _test_eof; 
	_test_eof11: cs = 11; goto _test_eof; 
	_test_eof497: cs = 497; goto _test_eof; 
	_test_eof12: cs = 12; goto _test_eof; 
	_test_eof13: cs = 13; goto _test_eof; 
	_test_eof14: cs = 14; goto _test_eof; 
	_test_eof15: cs = 15; goto _test_eof; 
	_test_eof16: cs = 16; goto _test_eof; 
	_test_eof17: cs = 17; goto _test_eof; 
	_test_eof18: cs = 18; goto _test_eof; 
	_test_eof19: cs = 19; goto _test_eof; 
	_test_eof20: cs = 20; goto _test_eof; 
	_test_eof21: cs = 21; goto _test_eof; 
	_test_eof22: cs = 22; goto _test_eof; 
	_test_eof23: cs = 23; goto _test_eof; 
	_test_eof24: cs = 24; goto _test_eof; 
	_test_eof25: cs = 25; goto _test_eof; 
	_test_eof26: cs = 26; goto _test_eof; 
	_test_eof27: cs = 27; goto _test_eof; 
	_test_eof28: cs = 28; goto _test_eof; 
	_test_eof29: cs = 29; goto _test_eof; 
	_test_eof30: cs = 30; goto _test_eof; 
	_test_eof31: cs = 31; goto _test_eof; 
	_test_eof32: cs = 32; goto _test_eof; 
	_test_eof33: cs = 33; goto _test_eof; 
	_test_eof34: cs = 34; goto _test_eof; 
	_test_eof35: cs = 35; goto _test_eof; 
	_test_eof36: cs = 36; goto _test_eof; 
	_test_eof37: cs = 37; goto _test_eof; 
	_test_eof38: cs = 38; goto _test_eof; 
	_test_eof39: cs = 39; goto _test_eof; 
	_test_eof40: cs = 40; goto _test_eof; 
	_test_eof41: cs = 41; goto _test_eof; 
	_test_eof42: cs = 42; goto _test_eof; 
	_test_eof43: cs = 43; goto _test_eof; 
	_test_eof44: cs = 44; goto _test_eof; 
	_test_eof45: cs = 45; goto _test_eof; 
	_test_eof46: cs = 46; goto _test_eof; 
	_test_eof47: cs = 47; goto _test_eof; 
	_test_eof48: cs = 48; goto _test_eof; 
	_test_eof49: cs = 49; goto _test_eof; 
	_test_eof50: cs = 50; goto _test_eof; 
	_test_eof51: cs = 51; goto _test_eof; 
	_test_eof52: cs = 52; goto _test_eof; 
	_test_eof53: cs = 53; goto _test_eof; 
	_test_eof54: cs = 54; goto _test_eof; 
	_test_eof55: cs = 55; goto _test_eof; 
	_test_eof56: cs = 56; goto _test_eof; 
	_test_eof57: cs = 57; goto _test_eof; 
	_test_eof58: cs = 58; goto _test_eof; 
	_test_eof59: cs = 59; goto _test_eof; 
	_test_eof60: cs = 60; goto _test_eof; 
	_test_eof61: cs = 61; goto _test_eof; 
	_test_eof62: cs = 62; goto _test_eof; 
	_test_eof63: cs = 63; goto _test_eof; 
	_test_eof64: cs = 64; goto _test_eof; 
	_test_eof65: cs = 65; goto _test_eof; 
	_test_eof66: cs = 66; goto _test_eof; 
	_test_eof67: cs = 67; goto _test_eof; 
	_test_eof68: cs = 68; goto _test_eof; 
	_test_eof69: cs = 69; goto _test_eof; 
	_test_eof70: cs = 70; goto _test_eof; 
	_test_eof71: cs = 71; goto _test_eof; 
	_test_eof72: cs = 72; goto _test_eof; 
	_test_eof73: cs = 73; goto _test_eof; 
	_test_eof74: cs = 74; goto _test_eof; 
	_test_eof75: cs = 75; goto _test_eof; 
	_test_eof76: cs = 76; goto _test_eof; 
	_test_eof77: cs = 77; goto _test_eof; 
	_test_eof78: cs = 78; goto _test_eof; 
	_test_eof79: cs = 79; goto _test_eof; 
	_test_eof80: cs = 80; goto _test_eof; 
	_test_eof81: cs = 81; goto _test_eof; 
	_test_eof82: cs = 82; goto _test_eof; 
	_test_eof83: cs = 83; goto _test_eof; 
	_test_eof84: cs = 84; goto _test_eof; 
	_test_eof85: cs = 85; goto _test_eof; 
	_test_eof86: cs = 86; goto _test_eof; 
	_test_eof87: cs = 87; goto _test_eof; 
	_test_eof88: cs = 88; goto _test_eof; 
	_test_eof89: cs = 89; goto _test_eof; 
	_test_eof90: cs = 90; goto _test_eof; 
	_test_eof91: cs = 91; goto _test_eof; 
	_test_eof92: cs = 92; goto _test_eof; 
	_test_eof93: cs = 93; goto _test_eof; 
	_test_eof94: cs = 94; goto _test_eof; 
	_test_eof95: cs = 95; goto _test_eof; 
	_test_eof96: cs = 96; goto _test_eof; 
	_test_eof97: cs = 97; goto _test_eof; 
	_test_eof98: cs = 98; goto _test_eof; 
	_test_eof99: cs = 99; goto _test_eof; 
	_test_eof100: cs = 100; goto _test_eof; 
	_test_eof101: cs = 101; goto _test_eof; 
	_test_eof102: cs = 102; goto _test_eof; 
	_test_eof103: cs = 103; goto _test_eof; 
	_test_eof104: cs = 104; goto _test_eof; 
	_test_eof105: cs = 105; goto _test_eof; 
	_test_eof106: cs = 106; goto _test_eof; 
	_test_eof107: cs = 107; goto _test_eof; 
	_test_eof108: cs = 108; goto _test_eof; 
	_test_eof109: cs = 109; goto _test_eof; 
	_test_eof110: cs = 110; goto _test_eof; 
	_test_eof111: cs = 111; goto _test_eof; 
	_test_eof112: cs = 112; goto _test_eof; 
	_test_eof113: cs = 113; goto _test_eof; 
	_test_eof114: cs = 114; goto _test_eof; 
	_test_eof115: cs = 115; goto _test_eof; 
	_test_eof116: cs = 116; goto _test_eof; 
	_test_eof117: cs = 117; goto _test_eof; 
	_test_eof118: cs = 118; goto _test_eof; 
	_test_eof119: cs = 119; goto _test_eof; 
	_test_eof120: cs = 120; goto _test_eof; 
	_test_eof121: cs = 121; goto _test_eof; 
	_test_eof122: cs = 122; goto _test_eof; 
	_test_eof123: cs = 123; goto _test_eof; 
	_test_eof124: cs = 124; goto _test_eof; 
	_test_eof125: cs = 125; goto _test_eof; 
	_test_eof126: cs = 126; goto _test_eof; 
	_test_eof127: cs = 127; goto _test_eof; 
	_test_eof128: cs = 128; goto _test_eof; 
	_test_eof129: cs = 129; goto _test_eof; 
	_test_eof130: cs = 130; goto _test_eof; 
	_test_eof131: cs = 131; goto _test_eof; 
	_test_eof132: cs = 132; goto _test_eof; 
	_test_eof133: cs = 133; goto _test_eof; 
	_test_eof134: cs = 134; goto _test_eof; 
	_test_eof135: cs = 135; goto _test_eof; 
	_test_eof136: cs = 136; goto _test_eof; 
	_test_eof137: cs = 137; goto _test_eof; 
	_test_eof138: cs = 138; goto _test_eof; 
	_test_eof139: cs = 139; goto _test_eof; 
	_test_eof140: cs = 140; goto _test_eof; 
	_test_eof141: cs = 141; goto _test_eof; 
	_test_eof142: cs = 142; goto _test_eof; 
	_test_eof143: cs = 143; goto _test_eof; 
	_test_eof144: cs = 144; goto _test_eof; 
	_test_eof145: cs = 145; goto _test_eof; 
	_test_eof146: cs = 146; goto _test_eof; 
	_test_eof147: cs = 147; goto _test_eof; 
	_test_eof148: cs = 148; goto _test_eof; 
	_test_eof149: cs = 149; goto _test_eof; 
	_test_eof150: cs = 150; goto _test_eof; 
	_test_eof151: cs = 151; goto _test_eof; 
	_test_eof152: cs = 152; goto _test_eof; 
	_test_eof153: cs = 153; goto _test_eof; 
	_test_eof154: cs = 154; goto _test_eof; 
	_test_eof155: cs = 155; goto _test_eof; 
	_test_eof156: cs = 156; goto _test_eof; 
	_test_eof157: cs = 157; goto _test_eof; 
	_test_eof158: cs = 158; goto _test_eof; 
	_test_eof159: cs = 159; goto _test_eof; 
	_test_eof160: cs = 160; goto _test_eof; 
	_test_eof161: cs = 161; goto _test_eof; 
	_test_eof162: cs = 162; goto _test_eof; 
	_test_eof163: cs = 163; goto _test_eof; 
	_test_eof164: cs = 164; goto _test_eof; 
	_test_eof165: cs = 165; goto _test_eof; 
	_test_eof166: cs = 166; goto _test_eof; 
	_test_eof167: cs = 167; goto _test_eof; 
	_test_eof168: cs = 168; goto _test_eof; 
	_test_eof169: cs = 169; goto _test_eof; 
	_test_eof170: cs = 170; goto _test_eof; 
	_test_eof171: cs = 171; goto _test_eof; 
	_test_eof172: cs = 172; goto _test_eof; 
	_test_eof173: cs = 173; goto _test_eof; 
	_test_eof174: cs = 174; goto _test_eof; 
	_test_eof175: cs = 175; goto _test_eof; 
	_test_eof176: cs = 176; goto _test_eof; 
	_test_eof177: cs = 177; goto _test_eof; 
	_test_eof178: cs = 178; goto _test_eof; 
	_test_eof179: cs = 179; goto _test_eof; 
	_test_eof180: cs = 180; goto _test_eof; 
	_test_eof181: cs = 181; goto _test_eof; 
	_test_eof182: cs = 182; goto _test_eof; 
	_test_eof183: cs = 183; goto _test_eof; 
	_test_eof184: cs = 184; goto _test_eof; 
	_test_eof185: cs = 185; goto _test_eof; 
	_test_eof186: cs = 186; goto _test_eof; 
	_test_eof187: cs = 187; goto _test_eof; 
	_test_eof188: cs = 188; goto _test_eof; 
	_test_eof189: cs = 189; goto _test_eof; 
	_test_eof190: cs = 190; goto _test_eof; 
	_test_eof191: cs = 191; goto _test_eof; 
	_test_eof192: cs = 192; goto _test_eof; 
	_test_eof193: cs = 193; goto _test_eof; 
	_test_eof194: cs = 194; goto _test_eof; 
	_test_eof195: cs = 195; goto _test_eof; 
	_test_eof196: cs = 196; goto _test_eof; 
	_test_eof197: cs = 197; goto _test_eof; 
	_test_eof198: cs = 198; goto _test_eof; 
	_test_eof199: cs = 199; goto _test_eof; 
	_test_eof200: cs = 200; goto _test_eof; 
	_test_eof201: cs = 201; goto _test_eof; 
	_test_eof202: cs = 202; goto _test_eof; 
	_test_eof203: cs = 203; goto _test_eof; 
	_test_eof204: cs = 204; goto _test_eof; 
	_test_eof205: cs = 205; goto _test_eof; 
	_test_eof206: cs = 206; goto _test_eof; 
	_test_eof207: cs = 207; goto _test_eof; 
	_test_eof208: cs = 208; goto _test_eof; 
	_test_eof209: cs = 209; goto _test_eof; 
	_test_eof210: cs = 210; goto _test_eof; 
	_test_eof211: cs = 211; goto _test_eof; 
	_test_eof212: cs = 212; goto _test_eof; 
	_test_eof213: cs = 213; goto _test_eof; 
	_test_eof214: cs = 214; goto _test_eof; 
	_test_eof215: cs = 215; goto _test_eof; 
	_test_eof216: cs = 216; goto _test_eof; 
	_test_eof217: cs = 217; goto _test_eof; 
	_test_eof218: cs = 218; goto _test_eof; 
	_test_eof219: cs = 219; goto _test_eof; 
	_test_eof220: cs = 220; goto _test_eof; 
	_test_eof221: cs = 221; goto _test_eof; 
	_test_eof222: cs = 222; goto _test_eof; 
	_test_eof223: cs = 223; goto _test_eof; 
	_test_eof224: cs = 224; goto _test_eof; 
	_test_eof225: cs = 225; goto _test_eof; 
	_test_eof226: cs = 226; goto _test_eof; 
	_test_eof227: cs = 227; goto _test_eof; 
	_test_eof228: cs = 228; goto _test_eof; 
	_test_eof229: cs = 229; goto _test_eof; 
	_test_eof230: cs = 230; goto _test_eof; 
	_test_eof231: cs = 231; goto _test_eof; 
	_test_eof232: cs = 232; goto _test_eof; 
	_test_eof233: cs = 233; goto _test_eof; 
	_test_eof234: cs = 234; goto _test_eof; 
	_test_eof235: cs = 235; goto _test_eof; 
	_test_eof236: cs = 236; goto _test_eof; 
	_test_eof237: cs = 237; goto _test_eof; 
	_test_eof238: cs = 238; goto _test_eof; 
	_test_eof239: cs = 239; goto _test_eof; 
	_test_eof240: cs = 240; goto _test_eof; 
	_test_eof241: cs = 241; goto _test_eof; 
	_test_eof242: cs = 242; goto _test_eof; 
	_test_eof243: cs = 243; goto _test_eof; 
	_test_eof244: cs = 244; goto _test_eof; 
	_test_eof245: cs = 245; goto _test_eof; 
	_test_eof246: cs = 246; goto _test_eof; 
	_test_eof247: cs = 247; goto _test_eof; 
	_test_eof248: cs = 248; goto _test_eof; 
	_test_eof249: cs = 249; goto _test_eof; 
	_test_eof250: cs = 250; goto _test_eof; 
	_test_eof251: cs = 251; goto _test_eof; 
	_test_eof252: cs = 252; goto _test_eof; 
	_test_eof253: cs = 253; goto _test_eof; 
	_test_eof254: cs = 254; goto _test_eof; 
	_test_eof255: cs = 255; goto _test_eof; 
	_test_eof256: cs = 256; goto _test_eof; 
	_test_eof257: cs = 257; goto _test_eof; 
	_test_eof258: cs = 258; goto _test_eof; 
	_test_eof259: cs = 259; goto _test_eof; 
	_test_eof260: cs = 260; goto _test_eof; 
	_test_eof261: cs = 261; goto _test_eof; 
	_test_eof262: cs = 262; goto _test_eof; 
	_test_eof263: cs = 263; goto _test_eof; 
	_test_eof264: cs = 264; goto _test_eof; 
	_test_eof265: cs = 265; goto _test_eof; 
	_test_eof266: cs = 266; goto _test_eof; 
	_test_eof267: cs = 267; goto _test_eof; 
	_test_eof268: cs = 268; goto _test_eof; 
	_test_eof269: cs = 269; goto _test_eof; 
	_test_eof270: cs = 270; goto _test_eof; 
	_test_eof271: cs = 271; goto _test_eof; 
	_test_eof272: cs = 272; goto _test_eof; 
	_test_eof273: cs = 273; goto _test_eof; 
	_test_eof274: cs = 274; goto _test_eof; 
	_test_eof275: cs = 275; goto _test_eof; 
	_test_eof276: cs = 276; goto _test_eof; 
	_test_eof277: cs = 277; goto _test_eof; 
	_test_eof278: cs = 278; goto _test_eof; 
	_test_eof279: cs = 279; goto _test_eof; 
	_test_eof280: cs = 280; goto _test_eof; 
	_test_eof281: cs = 281; goto _test_eof; 
	_test_eof282: cs = 282; goto _test_eof; 
	_test_eof283: cs = 283; goto _test_eof; 
	_test_eof284: cs = 284; goto _test_eof; 
	_test_eof285: cs = 285; goto _test_eof; 
	_test_eof286: cs = 286; goto _test_eof; 
	_test_eof287: cs = 287; goto _test_eof; 
	_test_eof288: cs = 288; goto _test_eof; 
	_test_eof289: cs = 289; goto _test_eof; 
	_test_eof290: cs = 290; goto _test_eof; 
	_test_eof291: cs = 291; goto _test_eof; 
	_test_eof292: cs = 292; goto _test_eof; 
	_test_eof293: cs = 293; goto _test_eof; 
	_test_eof294: cs = 294; goto _test_eof; 
	_test_eof295: cs = 295; goto _test_eof; 
	_test_eof296: cs = 296; goto _test_eof; 
	_test_eof297: cs = 297; goto _test_eof; 
	_test_eof298: cs = 298; goto _test_eof; 
	_test_eof299: cs = 299; goto _test_eof; 
	_test_eof300: cs = 300; goto _test_eof; 
	_test_eof301: cs = 301; goto _test_eof; 
	_test_eof302: cs = 302; goto _test_eof; 
	_test_eof303: cs = 303; goto _test_eof; 
	_test_eof304: cs = 304; goto _test_eof; 
	_test_eof305: cs = 305; goto _test_eof; 
	_test_eof306: cs = 306; goto _test_eof; 
	_test_eof307: cs = 307; goto _test_eof; 
	_test_eof308: cs = 308; goto _test_eof; 
	_test_eof309: cs = 309; goto _test_eof; 
	_test_eof310: cs = 310; goto _test_eof; 
	_test_eof311: cs = 311; goto _test_eof; 
	_test_eof312: cs = 312; goto _test_eof; 
	_test_eof313: cs = 313; goto _test_eof; 
	_test_eof314: cs = 314; goto _test_eof; 
	_test_eof315: cs = 315; goto _test_eof; 
	_test_eof316: cs = 316; goto _test_eof; 
	_test_eof317: cs = 317; goto _test_eof; 
	_test_eof318: cs = 318; goto _test_eof; 
	_test_eof319: cs = 319; goto _test_eof; 
	_test_eof320: cs = 320; goto _test_eof; 
	_test_eof321: cs = 321; goto _test_eof; 
	_test_eof322: cs = 322; goto _test_eof; 
	_test_eof323: cs = 323; goto _test_eof; 
	_test_eof324: cs = 324; goto _test_eof; 
	_test_eof325: cs = 325; goto _test_eof; 
	_test_eof326: cs = 326; goto _test_eof; 
	_test_eof327: cs = 327; goto _test_eof; 
	_test_eof328: cs = 328; goto _test_eof; 
	_test_eof329: cs = 329; goto _test_eof; 
	_test_eof330: cs = 330; goto _test_eof; 
	_test_eof331: cs = 331; goto _test_eof; 
	_test_eof332: cs = 332; goto _test_eof; 
	_test_eof333: cs = 333; goto _test_eof; 
	_test_eof334: cs = 334; goto _test_eof; 
	_test_eof335: cs = 335; goto _test_eof; 
	_test_eof336: cs = 336; goto _test_eof; 
	_test_eof337: cs = 337; goto _test_eof; 
	_test_eof338: cs = 338; goto _test_eof; 
	_test_eof339: cs = 339; goto _test_eof; 
	_test_eof340: cs = 340; goto _test_eof; 
	_test_eof341: cs = 341; goto _test_eof; 
	_test_eof342: cs = 342; goto _test_eof; 
	_test_eof343: cs = 343; goto _test_eof; 
	_test_eof344: cs = 344; goto _test_eof; 
	_test_eof345: cs = 345; goto _test_eof; 
	_test_eof346: cs = 346; goto _test_eof; 
	_test_eof347: cs = 347; goto _test_eof; 
	_test_eof348: cs = 348; goto _test_eof; 
	_test_eof349: cs = 349; goto _test_eof; 
	_test_eof350: cs = 350; goto _test_eof; 
	_test_eof351: cs = 351; goto _test_eof; 
	_test_eof352: cs = 352; goto _test_eof; 
	_test_eof353: cs = 353; goto _test_eof; 
	_test_eof354: cs = 354; goto _test_eof; 
	_test_eof355: cs = 355; goto _test_eof; 
	_test_eof356: cs = 356; goto _test_eof; 
	_test_eof357: cs = 357; goto _test_eof; 
	_test_eof358: cs = 358; goto _test_eof; 
	_test_eof359: cs = 359; goto _test_eof; 
	_test_eof360: cs = 360; goto _test_eof; 
	_test_eof361: cs = 361; goto _test_eof; 
	_test_eof362: cs = 362; goto _test_eof; 
	_test_eof363: cs = 363; goto _test_eof; 
	_test_eof364: cs = 364; goto _test_eof; 
	_test_eof365: cs = 365; goto _test_eof; 
	_test_eof366: cs = 366; goto _test_eof; 
	_test_eof367: cs = 367; goto _test_eof; 
	_test_eof368: cs = 368; goto _test_eof; 
	_test_eof369: cs = 369; goto _test_eof; 
	_test_eof370: cs = 370; goto _test_eof; 
	_test_eof371: cs = 371; goto _test_eof; 
	_test_eof372: cs = 372; goto _test_eof; 
	_test_eof373: cs = 373; goto _test_eof; 
	_test_eof374: cs = 374; goto _test_eof; 
	_test_eof375: cs = 375; goto _test_eof; 
	_test_eof376: cs = 376; goto _test_eof; 
	_test_eof377: cs = 377; goto _test_eof; 
	_test_eof378: cs = 378; goto _test_eof; 
	_test_eof379: cs = 379; goto _test_eof; 
	_test_eof380: cs = 380; goto _test_eof; 
	_test_eof381: cs = 381; goto _test_eof; 
	_test_eof382: cs = 382; goto _test_eof; 
	_test_eof383: cs = 383; goto _test_eof; 
	_test_eof384: cs = 384; goto _test_eof; 
	_test_eof385: cs = 385; goto _test_eof; 
	_test_eof386: cs = 386; goto _test_eof; 
	_test_eof387: cs = 387; goto _test_eof; 
	_test_eof388: cs = 388; goto _test_eof; 
	_test_eof389: cs = 389; goto _test_eof; 
	_test_eof390: cs = 390; goto _test_eof; 
	_test_eof391: cs = 391; goto _test_eof; 
	_test_eof392: cs = 392; goto _test_eof; 
	_test_eof393: cs = 393; goto _test_eof; 
	_test_eof394: cs = 394; goto _test_eof; 
	_test_eof395: cs = 395; goto _test_eof; 
	_test_eof396: cs = 396; goto _test_eof; 
	_test_eof397: cs = 397; goto _test_eof; 
	_test_eof398: cs = 398; goto _test_eof; 
	_test_eof399: cs = 399; goto _test_eof; 
	_test_eof400: cs = 400; goto _test_eof; 
	_test_eof401: cs = 401; goto _test_eof; 
	_test_eof402: cs = 402; goto _test_eof; 
	_test_eof403: cs = 403; goto _test_eof; 
	_test_eof404: cs = 404; goto _test_eof; 
	_test_eof405: cs = 405; goto _test_eof; 
	_test_eof406: cs = 406; goto _test_eof; 
	_test_eof407: cs = 407; goto _test_eof; 
	_test_eof408: cs = 408; goto _test_eof; 
	_test_eof409: cs = 409; goto _test_eof; 
	_test_eof410: cs = 410; goto _test_eof; 
	_test_eof411: cs = 411; goto _test_eof; 
	_test_eof412: cs = 412; goto _test_eof; 
	_test_eof413: cs = 413; goto _test_eof; 
	_test_eof414: cs = 414; goto _test_eof; 
	_test_eof415: cs = 415; goto _test_eof; 
	_test_eof416: cs = 416; goto _test_eof; 
	_test_eof417: cs = 417; goto _test_eof; 
	_test_eof418: cs = 418; goto _test_eof; 
	_test_eof419: cs = 419; goto _test_eof; 
	_test_eof420: cs = 420; goto _test_eof; 
	_test_eof421: cs = 421; goto _test_eof; 
	_test_eof422: cs = 422; goto _test_eof; 
	_test_eof423: cs = 423; goto _test_eof; 
	_test_eof424: cs = 424; goto _test_eof; 
	_test_eof425: cs = 425; goto _test_eof; 
	_test_eof426: cs = 426; goto _test_eof; 
	_test_eof427: cs = 427; goto _test_eof; 
	_test_eof428: cs = 428; goto _test_eof; 
	_test_eof429: cs = 429; goto _test_eof; 
	_test_eof430: cs = 430; goto _test_eof; 
	_test_eof431: cs = 431; goto _test_eof; 
	_test_eof432: cs = 432; goto _test_eof; 
	_test_eof433: cs = 433; goto _test_eof; 
	_test_eof434: cs = 434; goto _test_eof; 
	_test_eof435: cs = 435; goto _test_eof; 
	_test_eof436: cs = 436; goto _test_eof; 
	_test_eof437: cs = 437; goto _test_eof; 
	_test_eof438: cs = 438; goto _test_eof; 
	_test_eof439: cs = 439; goto _test_eof; 
	_test_eof440: cs = 440; goto _test_eof; 
	_test_eof441: cs = 441; goto _test_eof; 
	_test_eof442: cs = 442; goto _test_eof; 
	_test_eof443: cs = 443; goto _test_eof; 
	_test_eof444: cs = 444; goto _test_eof; 
	_test_eof445: cs = 445; goto _test_eof; 
	_test_eof446: cs = 446; goto _test_eof; 
	_test_eof447: cs = 447; goto _test_eof; 
	_test_eof448: cs = 448; goto _test_eof; 
	_test_eof449: cs = 449; goto _test_eof; 
	_test_eof450: cs = 450; goto _test_eof; 
	_test_eof451: cs = 451; goto _test_eof; 
	_test_eof452: cs = 452; goto _test_eof; 
	_test_eof453: cs = 453; goto _test_eof; 
	_test_eof454: cs = 454; goto _test_eof; 
	_test_eof455: cs = 455; goto _test_eof; 
	_test_eof456: cs = 456; goto _test_eof; 
	_test_eof457: cs = 457; goto _test_eof; 
	_test_eof458: cs = 458; goto _test_eof; 
	_test_eof459: cs = 459; goto _test_eof; 
	_test_eof460: cs = 460; goto _test_eof; 
	_test_eof461: cs = 461; goto _test_eof; 
	_test_eof462: cs = 462; goto _test_eof; 
	_test_eof463: cs = 463; goto _test_eof; 
	_test_eof464: cs = 464; goto _test_eof; 
	_test_eof465: cs = 465; goto _test_eof; 
	_test_eof466: cs = 466; goto _test_eof; 
	_test_eof467: cs = 467; goto _test_eof; 
	_test_eof468: cs = 468; goto _test_eof; 
	_test_eof469: cs = 469; goto _test_eof; 
	_test_eof470: cs = 470; goto _test_eof; 
	_test_eof471: cs = 471; goto _test_eof; 
	_test_eof472: cs = 472; goto _test_eof; 
	_test_eof473: cs = 473; goto _test_eof; 
	_test_eof474: cs = 474; goto _test_eof; 
	_test_eof475: cs = 475; goto _test_eof; 
	_test_eof476: cs = 476; goto _test_eof; 
	_test_eof477: cs = 477; goto _test_eof; 
	_test_eof478: cs = 478; goto _test_eof; 
	_test_eof479: cs = 479; goto _test_eof; 
	_test_eof480: cs = 480; goto _test_eof; 
	_test_eof481: cs = 481; goto _test_eof; 
	_test_eof482: cs = 482; goto _test_eof; 
	_test_eof483: cs = 483; goto _test_eof; 
	_test_eof484: cs = 484; goto _test_eof; 
	_test_eof485: cs = 485; goto _test_eof; 
	_test_eof486: cs = 486; goto _test_eof; 
	_test_eof487: cs = 487; goto _test_eof; 
	_test_eof488: cs = 488; goto _test_eof; 
	_test_eof489: cs = 489; goto _test_eof; 
	_test_eof490: cs = 490; goto _test_eof; 
	_test_eof491: cs = 491; goto _test_eof; 
	_test_eof492: cs = 492; goto _test_eof; 
	_test_eof493: cs = 493; goto _test_eof; 
	_test_eof494: cs = 494; goto _test_eof; 
	_test_eof495: cs = 495; goto _test_eof; 

	_test_eof: {}
	if ( p == eof )
	{
	switch ( cs ) {
	case 497: 
#line 1119 "./src/zscan_rfc1035.rl"
	{ z->lcount++; }
	break;
#line 9772 "src/zscan_rfc1035.c"
	}
	}

	_out: {}
	}

#line 1309 "./src/zscan_rfc1035.rl"
#endif // __clang_analyzer__
// end-sonar-exclude
    GDNSD_DIAG_POP
    GDNSD_DIAG_POP

    if (cs == zone_error)
        parse_error_noargs("General parse error");
    else if (cs < zone_first_final)
        parse_error_noargs("Trailing incomplete or unparseable record at end of file");
}
