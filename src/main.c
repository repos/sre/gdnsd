/* Copyright © 2012 Brandon L Black <blblack@gmail.com>
 *
 * This file is part of gdnsd.
 *
 * gdnsd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gdnsd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gdnsd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>
#include "main.h"

#include "conf.h"
#include "socks.h"
#include "daemon.h"
#include "dnsio_tcp.h"
#include "dnsio_udp.h"
#include "dnspacket.h"
#include "statio.h"
#include "ltree.h"
#include "css.h"
#include "csc.h"
#include "chal.h"
#include "cookie.h"
#include "dnssec.h"

#include "plugins/plugapi.h"
#include "plugins/mon.h"
#include <gdnsd/alloc.h>
#include <gdnsd/log.h>
#include <gdnsd/net.h>
#include <gdnsd/vscf.h>
#include <gdnsd/paths.h>
#include <gdnsd/misc.h>

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <pwd.h>
#include <time.h>
#include <stdatomic.h>

// Most platforms will report ATOMIC_POINTER_LOCK_FREE==2 allowing us to know
// at compile-time that our assumptions hold.  One known real-world exception
// is some compilers targeting older ARM platforms in their default mode, which
// will give a value of 1, meaning that lock free pointers are possibly
// supported at runtime, but we can't be sure at compiletime.  There may be
// other such cases today or in the future, so the only general purpose
// solution is to include an early runtime check for them here and ensure the
// daemon can't successfully run if our assumption of a lock-free atomic
// pointer doesn't hold.
static void check_atomic_assumptions(void)
{
    // Our configure script checks this before even building, but JIC:
    static_assert(ATOMIC_POINTER_LOCK_FREE, "atomic lock-free pointer possible");
#if ATOMIC_POINTER_LOCK_FREE < 2
    atomic_uintptr_t aup = 0;
    if (!atomic_is_lock_free(&aup))
        log_fatal("This build fails to support the required lock-free C11 atomic pointers.  For some targets, this might be fixable by using more specific and/or modern -march or -mcpu sorts of settings!");
#endif
}

#include <sodium.h>

// This is set by the signal handler for terminal signals, and consumed as
// the correct signal to re-raise for final termination
static int killed_by = 0;

// These are used for the libev "async" watcher mechanism for cross-thread
// notifications of zone reload completion during runtime.
static struct ev_loop* async_reloadz_loop = NULL;
static ev_async async_reloadz;

// custom atexit-like stuff for resource deallocation

static void (**exitfuncs)(void) = NULL;
static unsigned exitfuncs_pending = 0;

void gdnsd_atexit(void (*f)(void))
{
    exitfuncs = xrealloc_n(exitfuncs, exitfuncs_pending + 1, sizeof(*exitfuncs));
    exitfuncs[exitfuncs_pending++] = f;
}

static void atexit_execute(void)
{
    while (exitfuncs_pending--)
        exitfuncs[exitfuncs_pending]();
}

noreturn F_NONNULL
static void syserr_for_ev(const char* msg)
{
    log_fatal("%s: %s", msg, logf_errno());
}

static void* alloc_for_ev(void* ptr, long size)
{
    if (unlikely(size < 0))
        log_fatal("Invalid alloc_for_ev() size %li\n", size);
    if (size)
        return xrealloc(ptr, (size_t)size);
    free(ptr);
    return 0;
}

static pthread_t zones_reloader_threadid;

static bool join_zones_reloader_thread(void)
{
    void* raw_exit_status = (void*)42U;
    int pthread_err = pthread_join(zones_reloader_threadid, &raw_exit_status);
    if (pthread_err)
        log_err("pthread_join() of zone data loading thread failed: %s", logf_strerror(pthread_err));
    return !!raw_exit_status;
}

// Spawns a new thread to reload zone data.  Initial loading at startup sets
// the "initial" flag for the thread, which means it doesn't send an async
// notification back to us on completion, as we'll be waiting for it
// synchronously in this case.
static void spawn_zones_reloader_thread(const bool initial)
{
    // Block all signals using the pthreads interface while starting threads,
    //  which causes them to inherit the same mask.
    sigset_t sigmask_all;
    sigfillset(&sigmask_all);
    sigset_t sigmask_prev;
    sigemptyset(&sigmask_prev);
    if (pthread_sigmask(SIG_SETMASK, &sigmask_all, &sigmask_prev))
        log_fatal("pthread_sigmask() failed");

    pthread_attr_t attribs;
    pthread_attr_init(&attribs);
    pthread_attr_setdetachstate(&attribs, PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&attribs, PTHREAD_SCOPE_SYSTEM);

    int pthread_err = pthread_create(&zones_reloader_threadid, &attribs, &ltree_zones_reloader_thread, (void*)initial);
    if (pthread_err)
        log_fatal("pthread_create() of zone data thread failed: %s", logf_strerror(pthread_err));

    // Restore the original mask in the main thread, so
    //  we can continue handling signals like normal
    if (pthread_sigmask(SIG_SETMASK, &sigmask_prev, NULL))
        log_fatal("pthread_sigmask() failed");
    pthread_attr_destroy(&attribs);
}

static bool initialize_zones(void)
{
    spawn_zones_reloader_thread(true);
    return join_zones_reloader_thread();
}

void spawn_async_zones_reloader_thread(void)
{
    spawn_zones_reloader_thread(false);
}

F_NONNULL
static void terminal_signal(struct ev_loop* loop, struct ev_signal* w, const int revents V_UNUSED)
{
    gdnsd_assert(revents == EV_SIGNAL);
    gdnsd_assert(w->signum == SIGTERM || w->signum == SIGINT);
    const struct css* css = w->data;
    if (!css_stop_ok(css)) {
        log_err("Ignoring terminating signal %i because a replace attempt is in progress!", w->signum);
    } else {
        log_info("Exiting cleanly on receipt of terminating signal %i", w->signum);
        killed_by = w->signum;
        ev_break(loop, EVBREAK_ALL);
    }
}

F_NONNULL
static void reload_zones_done(struct ev_loop* loop V_UNUSED, struct ev_async* a V_UNUSED, const int revents V_UNUSED)
{
    gdnsd_assert(revents == EV_ASYNC);
    struct css* css = a->data;
    const bool failed = join_zones_reloader_thread();

    if (failed)
        log_err("Reloading zone data failed");
    else
        log_info("Reloading zone data successful");

    if (css_notify_zone_reloaders(css, failed))
        spawn_async_zones_reloader_thread();
}

// called by ltree reloader thread just before it exits
void notify_reload_zones_done(void)
{
    gdnsd_assume(async_reloadz_loop);
    ev_async* p_async_reloadz = &async_reloadz;
    ev_async_send(async_reloadz_loop, p_async_reloadz);
}

static void setup_reload_zones(struct css* css, struct ev_loop* loop)
{
    // Copy loop pointer to a global for access from the above helper
    // function from another thread:
    async_reloadz_loop = loop;
    ev_async* p_async_reloadz = &async_reloadz;
    ev_async_init(p_async_reloadz, reload_zones_done);
    p_async_reloadz->data = css;
    ev_async_start(loop, p_async_reloadz);
}

noreturn F_NONNULL
static void usage(const char* argv0)
{
    const char* def_cfdir = gdnsd_get_default_config_dir();
    fprintf(stderr,
            PACKAGE_NAME " version " PACKAGE_VERSION "\n"
            "Usage: %s [-c %s] [-D] [-l] [-S] [-R | -i] <action>\n"
            "  -c - Configuration directory, default '%s'\n"
            "  -D - Enable verbose debug output\n"
            "  -l - Send logs to syslog rather than stderr\n"
            "  -S - Force 'zones_strict_data = true' for this invocation\n"
            "  -R - Attempt downtimeless replace of another instance\n"
            "  -i - Idempotent mode for start/daemonize: exit 0 if already running\n"
            "       (-R and -i cannot be used together)\n"
            "Actions:\n"
            "  checkconf - Checks validity of config and zone files\n"
            "  start - Start as a regular foreground process\n"
            "  daemonize - Start as a background daemon (implies -l)\n"
            "\nFeatures: " BUILD_FEATURES
            "\nBuild Info: " BUILD_INFO
            "\nBug report URL: " PACKAGE_BUGREPORT
            "\nGeneral info URL: " PACKAGE_URL
            "\n",
            argv0, def_cfdir, def_cfdir
           );
    exit(2);
}

F_NONNULL
static void start_threads(const struct socks_cfg* socks_cfg)
{
    dnsio_udp_init(getpid());
    dnsio_tcp_init(CDL_GET_COUNT(&socks_cfg->dns_tcp_threads));

    // Block all signals using the pthreads interface while starting threads,
    //  which causes them to inherit the same mask.
    sigset_t sigmask_all;
    sigfillset(&sigmask_all);
    sigset_t sigmask_prev;
    sigemptyset(&sigmask_prev);
    if (pthread_sigmask(SIG_SETMASK, &sigmask_all, &sigmask_prev))
        log_fatal("pthread_sigmask() failed");

    // system scope scheduling, joinable threads
    pthread_attr_t attribs;
    pthread_attr_init(&attribs);
    pthread_attr_setdetachstate(&attribs, PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&attribs, PTHREAD_SCOPE_SYSTEM);

    int pthread_err;

    CDL_FOR_EACH(&socks_cfg->dns_udp_threads, struct dns_thread, dns_threads_entry, t) {
        pthread_err = pthread_create(&t->threadid, &attribs, &dnsio_udp_start, t);
        if (pthread_err)
            log_fatal("pthread_create() of DNS thread for UDP %s failed: %s",
                      logf_anysin(&t->ac->addr), logf_strerror(pthread_err));
    }
    CDL_FOR_EACH(&socks_cfg->dns_tcp_threads, struct dns_thread, dns_threads_entry, t) {
        pthread_err = pthread_create(&t->threadid, &attribs, &dnsio_tcp_start, t);
        if (pthread_err)
            log_fatal("pthread_create() of DNS thread for TCP %s failed: %s",
                      logf_anysin(&t->ac->addr), logf_strerror(pthread_err));
    }

    // Restore the original mask in the main thread, so
    //  we can continue handling signals like normal
    if (pthread_sigmask(SIG_SETMASK, &sigmask_prev, NULL))
        log_fatal("pthread_sigmask() failed");
    pthread_attr_destroy(&attribs);
}

F_NONNULL
static void request_io_threads_stop(const struct socks_cfg* socks_cfg)
{
    dnsio_tcp_request_threads_stop();
    CDL_FOR_EACH(&socks_cfg->dns_udp_threads, struct dns_thread, dns_threads_entry, t) {
        pthread_kill(t->threadid, SIGUSR2);
    }
}

F_NONNULL
static void wait_io_thread_stop(const struct dns_thread* t)
{
    void* raw_exit_status = (void*)42U;
    const int pthread_err = pthread_join(t->threadid, &raw_exit_status);
    if (pthread_err)
        log_err("pthread_join() of DNS thread failed: %s", logf_strerror(pthread_err));
    if (raw_exit_status != NULL)
        log_err("pthread_join() of DNS thread returned %p", raw_exit_status);
}

F_NONNULL
static void wait_io_threads_stop(const struct socks_cfg* socks_cfg)
{
    CDL_FOR_EACH(&socks_cfg->dns_udp_threads, struct dns_thread, dns_threads_entry, t) {
        wait_io_thread_stop(t);
    }
    CDL_FOR_EACH(&socks_cfg->dns_tcp_threads, struct dns_thread, dns_threads_entry, t) {
        wait_io_thread_stop(t);
    }
}

static void do_tak1(const struct csc* csc)
{
    union csbuf req;
    union csbuf resp;
    memset(&req, 0, sizeof(req));
    req.key = REQ_TAK1;
    req.d = (uint32_t)getpid();
    if (csc_txn(csc, &req, &resp) != CSC_TXN_OK)
        log_fatal("REPLACE[new daemon]: takeover phase 1 notification attempt failed (possibly lost race against another)");
}

static void do_tak2(struct ev_loop* loop, const struct csc* csc)
{
    uint8_t* chal_data = NULL;
    union csbuf req;
    union csbuf resp;
    memset(&req, 0, sizeof(req));
    req.key = REQ_TAK2;
    req.d = (uint32_t)getpid();
    if (csc_txn_getdata(csc, &req, &resp, (char**)&chal_data) != CSC_TXN_OK)
        log_fatal("REPLACE[new daemon]: takeover phase 2 notification attempt failed");
    const size_t chal_count = csbuf_get_v(&resp);
    const size_t chal_dlen = resp.d;
    log_debug("TAK2 challenge handoff got count %zu dlen %zu", chal_count, chal_dlen);
    size_t offset = 0;
    for (size_t i = 0; i < chal_count; i++) {
        if (offset + 5U > chal_dlen)
            log_fatal("REPLACE[new daemon]: corrupt challenge data size");
        size_t cset_dlen = gdnsd_get_una16(&chal_data[offset]);
        offset += 2U;
        size_t ttl_remain = gdnsd_get_una16(&chal_data[offset]);
        offset += 2U;
        size_t cset_count = chal_data[offset++];
        if (offset + cset_dlen > chal_dlen)
            log_fatal("REPLACE[new daemon]: corrupt challenge data size");
        if (cset_create(loop, ttl_remain, cset_count, cset_dlen, &chal_data[offset]))
            log_fatal("REPLACE[new daemon]: illegal challenge handoff data");
        offset += cset_dlen;
    }
    free(chal_data);
}

enum cli_action {
    ACT_UNDEF = 0,
    ACT_CHECKCONF,
    ACT_START,
    ACT_DAEMONIZE
};

struct cli_opts {
    const char* cfg_dir;
    bool force_zsd;
    bool replace_ok;
    bool idempotent;
    enum cli_action action;
};

F_NONNULL
static void parse_args(const int argc, char** argv, struct cli_opts* copts)
{
    int optchar;
    while ((optchar = getopt(argc, argv, "c:DlSRi"))) {
        switch (optchar) {
        case 'c':
            copts->cfg_dir = optarg;
            break;
        case 'D':
            gdnsd_log_set_debug(true);
            break;
        case 'l':
            gdnsd_log_set_syslog(true, NULL);
            break;
        case 'S':
            copts->force_zsd = true;
            break;
        case 'R':
            copts->replace_ok = true;
            break;
        case 'i':
            copts->idempotent = true;
            break;
        case -1:
            if (optind == (argc - 1)) {
                if (!strcasecmp("checkconf", argv[optind])) {
                    copts->action = ACT_CHECKCONF;
                    return;
                } else if (!strcasecmp("start", argv[optind])) {
                    copts->action = ACT_START;
                    return;
                } else if (!strcasecmp("daemonize", argv[optind])) {
                    copts->action = ACT_DAEMONIZE;
                    gdnsd_log_set_syslog(true, NULL);
                    return;
                }
            }
            usage(argv[0]);
        default:
            usage(argv[0]);
        }
    }
    usage(argv[0]);
}

static void try_raise_open_files(const struct socks_cfg* socks_cfg)
{
    // this is just a default guestimate anyways; it tries to account for all
    // the known network socket needs, and then bumps by a hundred to handle
    // most zonefile (keeping in mind we only open one file at a time, but
    // there can be parallel threads) and/or geoip database needs and other
    // miscellaneous bits.  It should at least get us in the ballpark.

    rlim_t files_desired = (rlim_t)(socks_cfg->fd_estimate + 100U);

    struct rlimit rlim;
    if (getrlimit(RLIMIT_NOFILE, &rlim)) {
        log_warn("getrlimit(RLIMIT_NOFILE) failed: %s", logf_errno());
        return;
    }

    if (rlim.rlim_cur != RLIM_INFINITY && rlim.rlim_cur < files_desired) {
        if (rlim.rlim_max != RLIM_INFINITY && rlim.rlim_max < files_desired)
            log_warn("Open files ulimit is capped at %llu, "
                     "but the daemon guesses we need about %llu",
                     (unsigned long long)rlim.rlim_max,
                     (unsigned long long)files_desired);

        if (rlim.rlim_max == RLIM_INFINITY || rlim.rlim_max > rlim.rlim_cur) {
            if (rlim.rlim_max == RLIM_INFINITY || rlim.rlim_max >= files_desired)
                rlim.rlim_cur = files_desired;
            else
                rlim.rlim_cur = rlim.rlim_max;

            if (setrlimit(RLIMIT_NOFILE, &rlim))
                log_warn("setrlimit(RLIMIT_NOFILE, cur = %llu) failed: %s",
                         (unsigned long long)rlim.rlim_cur, logf_errno());
            else
                log_info("Raised open files soft limit to %llu",
                         (unsigned long long)rlim.rlim_cur);
        }
    }
}

noreturn
static struct css* runtime_execute(const char* argv0, struct socks_cfg* socks_cfg, struct css* css, struct csc* csc)
{
    try_raise_open_files(socks_cfg);

    // init the stats output code
    statio_init();

    // Lock whole daemon into memory, including all future allocations.
    if (gcfg->lock_mem && mlockall(MCL_CURRENT | MCL_FUTURE))
        log_fatal("mlockall(MCL_CURRENT | MCL_FUTURE) failed: %s (you may need to disable the lock_mem config option if your system or your ulimits do not allow it)", logf_errno());

    // init cookie support and load key, if any
    if (!gcfg->disable_cookies)
        cookie_config(gcfg->cookie_key_file);

    // Set up libev error+allocator callbacks
    ev_set_syserr_cb(&syserr_for_ev);
    ev_set_allocator(&alloc_for_ev);

    // primary ev loop in main process to handle statio, monitors, control
    // socket, signals, etc.  Note non-default loop, meaning no implicit
    // SIGCHLD handler from libev.
    struct ev_loop* loop = ev_loop_new(EVFLAG_AUTO);
    if (!loop)
        log_fatal("Could not initialize the default libev loop");

    // set up monitoring, which expects an initially empty loop
    gdnsd_mon_start(loop);

    // import challenge data in takeover case
    if (csc)
        do_tak2(loop, csc);

    // Set up timer hook in the default loop for cookie key rotation
    if (!gcfg->disable_cookies)
        cookie_runtime_init(loop);

    // Call plugin pre-run actions
    gdnsd_plugins_action_pre_run();

    // Now that we're past potentially long-running operations like zone
    // loading, initial monitoring, plugin pre_run actions, initiate the
    // true takeover handoff sequence via css_new.
    if (!css)
        css = css_new(argv0, socks_cfg, &csc);

    // setup main thread signal handlers
    ev_signal sig_int;
    ev_signal sig_term;
    ev_signal* p_sig_int = &sig_int;
    ev_signal* p_sig_term = &sig_term;
    ev_signal_init(p_sig_int, terminal_signal, SIGINT);
    p_sig_int->data = css;
    ev_signal_start(loop, p_sig_int);
    ev_signal_init(p_sig_term, terminal_signal, SIGTERM);
    p_sig_term->data = css;
    ev_signal_start(loop, p_sig_term);

    // Initialize+bind DNS listening sockets.  For TCP this also invokes
    // listen, so all of the sockets are accepting requests when this returns,
    // but the threads are not actively executing accept/recv code to pull them
    // from the kernel's queues yet.
    socks_dns_lsocks_init(socks_cfg);

    // Start up all of the UDP and TCP i/o threads, which will begin processing
    // requests as soon as they can:
    start_threads(socks_cfg);

    // Notify 3rd parties of readiness (systemd, or fg process if daemonizing)
    gdnsd_daemon_notify_ready();

    // Notify the user that the listeners are up
    log_info("DNS listeners started");

    // Stop old daemon after establishing the new one's listeners, and import
    // the final stats from it
    if (csc) {
        if (!csc_stop_server(csc)) {
            uint64_t* stats_raw = NULL;
            const size_t dlen = csc_get_stats_handoff(csc, &stats_raw);
            if (dlen) {
                gdnsd_assume(stats_raw);
                statio_deserialize(stats_raw, dlen);
            }
            free(stats_raw);
        }
        csc_delete(csc);
    }

    // Set up zone reload mechanism and control socket handlers in the loop
    setup_reload_zones(css, loop);
    css_start(css, loop);

    // The daemon stays in this libev loop for life,
    // until there's a reason to cleanly exit
    ev_run(loop, 0);

    // request i/o threads to exit
    request_io_threads_stop(socks_cfg);

    // get rid of child procs (e.g. extmon helper)
    gdnsd_kill_registered_children();

    // wait for i/o threads to exit
    wait_io_threads_stop(socks_cfg);

    // If we were replaced, this sends a final dump of stats to the new daemon
    // for stats counter continuity
    css_send_stats_handoff(css);

    // deallocate resources
    atexit_execute();

    // We delete this last, because it will break connections with random
    // control socket clients, who may then try to reconnect, and in the case
    // of replace should quickly get connected to the new daemon from here.
    css_delete(css);

    // Stop the terminal signal handlers very late in the game.  Any terminal
    // signal received since ev_run() returned above will simply not be
    // processed because we never re-entered the eventloop since the handlers
    // saw it.  ev_signal_stop() will restore default signal behavior, which
    // will be to terminate the process, which we'll rely on in raise() below.
    // Regardless of our reason for exiting, it doesn't cause a problem if a
    // new terminal signal races us from here through exit()/raise() below.  It
    // is kinda problematic if we do this earlier (e.g. above i/o thread exit)
    // as it could abort our clean shutdown sequence.
    ev_signal_stop(loop, p_sig_term);
    ev_signal_stop(loop, p_sig_int);
    ev_loop_destroy(loop);

#ifdef GDNSD_COVERTEST_EXIT
    // We have to use exit() when testing coverage, as raise()
    //   skips over writing out gcov data
    exit(0);
#else
    // kill self with same signal, so that our exit status is correct
    //   for any parent/manager/whatever process that may be watching
    if (killed_by)
        raise(killed_by);
    else
        exit(0);
#endif

    // raise should not return
    gdnsd_assume(0);
}

int main(int argc, char** argv)
{
    umask(022);
    // Parse args, getting the config path
    //   returning the action.  Exits on cmdline errors,
    //   does not use assert/log stuff.
    struct cli_opts copts = {
        .cfg_dir = NULL,
        .force_zsd = false,
        .replace_ok = false,
        .idempotent = false,
        .action = ACT_UNDEF
    };

    parse_args(argc, argv, &copts);
    gdnsd_assume(copts.action != ACT_UNDEF);

    if (copts.replace_ok && copts.idempotent)
        usage(argv[0]);

    // Init daemon code if starting
    if (copts.action != ACT_CHECKCONF)
        gdnsd_init_daemon(copts.action == ACT_DAEMONIZE);

    log_info("gdnsd version " PACKAGE_VERSION " @ pid %li", (long)getpid());

    check_atomic_assumptions(); // ensure all our atomics are truly lock-free!

    // Initialize libsodium very early, so we don't have to worry about it again
    if (sodium_init() < 0)
        log_fatal("Could not initialize libsodium: %s", logf_errno());

    // Load and init basic pathname config (but no mkdir/chmod if checkconf)
    vscf_data_t* cfg_root = gdnsd_init_paths(copts.cfg_dir, copts.action != ACT_CHECKCONF);

    // Load (but do not act on) socket config
    struct socks_cfg* socks_cfg = socks_conf_load(cfg_root);

    // init locked control socket if starting, can fail if concurrent daemon,
    // or begin a takeover process if CLI flag allows
    struct csc* csc = NULL;
    struct css* css = NULL;
    if (copts.action != ACT_CHECKCONF) {
        css = css_new(argv[0], socks_cfg, NULL);
        if (!css) {
            if (copts.idempotent) {
                log_info("Another instance is already running, success");
                exit(0);
            }
            if (!copts.replace_ok)
                log_fatal("Another instance is running and has the control socket locked, failing");
            csc = csc_new(37U, "REPLACE[new daemon]: ", NULL);
            if (!csc)
                log_fatal("Another daemon appears to be running, but cannot establish a connection to its control socket for takeover, exiting!");
            do_tak1(csc);
            log_info("REPLACE[new daemon]: Connected to old daemon version %s at PID %li for takeover",
                     csc_get_server_version(csc), (long)csc_get_server_pid(csc));
        }
    }

    // Load full configuration and expose through the global "gcfg"
    gcfg = conf_load(cfg_root, copts.force_zsd);
    vscf_destroy(cfg_root);

    // Global early init for DNSSEC stuff
    dnssec_init_global();

    // Basic init for the acme challenge code
    chal_init();

    // initialize the zone storage and load zone data synchronously
    ltree_init();
    if (initialize_zones())
        log_fatal("Initial load of zone data failed");

    if (copts.action == ACT_CHECKCONF)
        exit(0);

    // Initalize for a real runtime daemon and enter a libev loop for the life
    // of the daemon.
    runtime_execute(argv[0], socks_cfg, css, csc);

    // Above does not return
    gdnsd_assume(0);
}
